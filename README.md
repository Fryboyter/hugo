# Fryboyter
Website released under https://fryboyter.de

## License

The [content](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/content/posts) of this project itself is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/), and the underlying source code used to format and display that content is licensed under the [GNU Affero General Public License v3.0](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/licenses/agpl).

In addition, the following projects are used which are published under a different licence.

- Isso ([isso.css](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/themes/GythaOgg/static/css/isso.css)) is published under the [MIT](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/licenses/mit) licence.

- The [Atkinson Hyperlegible](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/themes/GythaOgg/static/fonts/atkinson) font is published under the [OFL](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/licenses/ofl) licence.

## General note

Usually, I always use the latest version of Hugo and adapt the theme accordingly if necessary. It can therefore not be guaranteed that the code published here is compatible with an older version of Hugo.