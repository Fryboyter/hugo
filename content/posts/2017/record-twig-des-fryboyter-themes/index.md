---
title: Record.twig des Fryboyter-Themes
date: 2017-07-05T07:00:00+0100
categories:
- OSBN
- Allgemein
tags:
- Bolt CMS
slug: record-twig-des-fryboyter-themes
---
In meinem letzten [Artikel](/index-twig-des-fryboyter-themes/) habe ich die Datei index.twig meines Twig-Themes erklärt. Jetzt ist die Datei record.twig an der Reihe. Diese ist teilweise identisch mit der index.twig.

{{< highlight twig >}}
{% include '_header.twig' %}
<div class="header"><h1><a href="{{ paths.hosturl }}">Fryboyter</a></h1></div>
    <div class="container">
        <div class="row"> 
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <h2>{{ record.title }}</h2>
                    {% if record.taxonomy is defined %}
                        {% for type, values in record.taxonomy %}
                            <div class="article-info">Veröffentlicht am {{ record.datepublish|localdate("%d. %B %Y") }} unter:
                        {% for link, value in values %}
                            <a href="{{ link }}">{{ value }}</a>{% if not loop.last %} & {% endif %}
                        {% endfor %}
                            | <a href="{{ record.link }}#isso-thread">Comments</a>
                        {% if user.id is defined %}
                        {% if user.id == 'X' %}
                            | <a href="{{ paths.bolt }}editcontent/entries/{{ record.id }}">Bearbeiten</a>
                        {% endif %}
                        {% endif %} 
                        {% endfor %}
                    {% endif %}</div>
                    {{ record.body }}                        
                    <p>Diese Artikel könnten auch interessant sein</p>
                    <ul>{% setcontent records = "entries/random/5" allowpaging %}
                    {% for record in records %}
                    <li><a href="{{ record.link }}">{{ record.title }}</a></li>
                    {% endfor %}</ul>
                    <section>
                        <h3>Kommentare</h3>
                        <section id="isso-thread"></section>
                    </section>
        </div> 
    </div>   
</div>
<div class="footer">{% include '_footer.twig' %}</div>
{{< /highlight >}}

Den ersten Unterschied findet man in Zeile 14 bis 18. Hier wird als erstes geprüft, ob die Variable user.id definiert ist. Wenn dies der Fall ist erfolgt die Prüfung, ob der Nutzer die Nutzer-ID X hat. Wenn ja, wird die Zeile in der das Datum der Veröffentlichung, die Kategorien sowie die Kommentare angezeigt wird um einen weiteren Link erweitert, mit dem man den betreffenden Beitrag ändern kann.

Zeile 22 bis 26 erstellt eine Liste von 5 zufälligen Artikeln, die eventuell für den Leser auch interessant sein könnten und gibt deren Titel aus die auf die einzelnen Artikel verlinken. Hier werden aktuell noch Artikel kategorieübergreifend angezeigt. Geplant habe ich hier, dass nur Artikel angezeigt werden, die den gleichen Kategorien zugeordnet sind, wie der aufgerufene Artikel.

Zeile 27 bis 30 bindet die Kommentarfunktion von Isso Comment ein.
