---
title: Index.twig des Fryboyter-Themes
date: 2017-07-04T09:10:18+0100
categories:
- OSBN
- Allgemein
tags:
- Fryboyter
- Theme
- Bolt CMS
slug: index-twig-des-fryboyter-themes
---
Da ich gefragt wurde, wie ich das auf fryboyter.de verwendet Theme erstellt habe, habe ich mir gedacht ich mache mal eine Artikelreihe daraus in der ich die 9 Twig-Dateien erkläre.

Fangen wir mal mit der Datei index.twig an. Diese wird angezeigt, wenn man die Hauptseite aufruft.

{{< highlight twig >}}
{% include '_header.twig' %}
<div class="header"><h1><a href="{{ paths.hosturl }}">Fryboyter</a></h1></div>
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            {% setcontent records = "entries/latest/5" allowpaging %}
                {% for record in records %}
                    <h2><a href="{{ record.link }}">{{ record.title }}</a></h2>
                        {% if record.taxonomy is defined %}
                            {% for type, values in record.taxonomy %}
                            <div class="article-info">Veröffentlicht am {{ record.datepublish|localdate("%d. %B %Y") }} unter:
                            {% for link, value in values %}
                            <a href="{{ link }}">{{ value }}</a>{% if not loop.last %} & {% endif %}
                            {% endfor %}
                            | <a href="{{ record.link }}#isso-thread">Comments</a> </div>
                            {% endfor %}
                        {% endif %}
                    {{ record.body }}
                    <hr/>
                {% endfor %}
            {{ pager('', 3, '_sub_mypager.twig') }}
        </div>
    </div>
</div>
<div class="footer">{% include '_footer.twig' %}</div>
</body>
</html>
{{< /highlight >}}

Twig bietet die Möglichkeit bestimmte Bereich einer Seite in extra Dateien auszulagern. Mittels include kann man diese dann wie im Baukasten zusammensetzen. In diesem Fall wird in Zeile 1 somit die Datei _header.twig importiert.

In Zeile 2 wird mittels path.hosturl auf die Adresse der Seite verlinkt. In dem Fall also https://fryboyter.de/. Zieht man die Seite irgendwann man auf eine andere Domain um, braucht man hier die Adresse nicht händisch ändern.

In Zeile 6 werden die letzten 5 Einträge aus der Datenbank herausgefischt. Allowpaging ermöglicht es, dass man am Ende einer Seite auf weitere Seiten mit wiederum 5 Artikeln wechseln kann.

Ab Zeile 7 wird definiert wie jeder einzelne Artikel angezeigt wird.

In Zeile 8 wird als erstes der Titel inkl. entsprechenden Link eines Artikels ausgegeben.

Zeile 9 bis 17 ist schon etwas aufwändiger. Hier wird erst einmal geprüft, ob dem betreffenden Artikel Taxonomien zugeordnet wurden. Auf dieser Seite wären das die Kategorien wie osbn oder Allgemein. Wenn dem so ist, wird unter der Zeile der Artikelüberschrift das Datum der Veröffentlichung, gefolgt von den zugeordneten Kategorien angezeigt. Die Anzeige der Kategorien erfolgt hierbei in einer Schleife, sodass zwischen den Kategorien ein &amp; angezeigt wird. Zum Schluss wird am Ende noch die Anzahl der vorhandenen Kommentare angezeigt und auf die Kommentarfunktion verlinkt (Zeile 18).

In Zeile 21 wird dann der Haupttext des Artikels angezeigt (ich arbeite nie mit Teasern).

Zeile 24 bindet die Datei _sub_mypager.twig ein. Hiermit wird am Ende einer Seite der Umschalter angezeigt, mit dem man die Seiten wechseln kann.

Abschließend wird in Zeile 28 die Datei _footer.twig importiert.
