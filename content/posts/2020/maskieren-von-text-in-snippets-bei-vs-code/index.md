---
title: Maskieren von Text in Snippets bei VS Code
date: 2020-01-03T17:45:23+0100
categories:
- OSBN
- General
tags:
- VS Code  
- Snippets
slug: maskieren-von-text-in-snippets-bei-vs-code
---
Urlaubszeit ist bei mir Bastelzeit. Daher bin ich gerade dabei mal wieder VS Code zu testen, obwohl ich mir vorab schon sicher bin, dass ich bei Sublime Text bleiben werde. Na ja egal, darum geht es gerade nicht.

Damit ich relativ bequem Artikel veröffentlichen kann, habe ich mir unter Sublime Text diverse Snippets angelegt. Bei einem geht es darum, dass ich Text markiere und per Shortcut der HTML-Code hinzugefügt wird, mit dem ich Text als Code hervorhebe. Somit wird zum Beispiel aus "10 PRINT "hallo welt"" folgendes.

{{< highlight bash >}}
<pre class="line-numbers language-bash" style="white-space:pre-wrap;">
<code class="language-bash">10 PRINT "hallo welt"</code>
</pre>
{{< /highlight >}}

Unter VS Code habe ich mir folgendes Snippet angelegt.

{{< highlight bash >}}
"wrap_highlight": {
		"prefix": "wrap_highlight",
		"body": [
			"<pre class="line-numbers language-bash" style="white-space:pre-wrap;">",
			"<code class="language-bash">$TM_SELECTED_TEXT</code>",
			"</pre>"
		],
		"description": "Highlight Code"
	},
{{< /highlight >}}

Sieht eigentlich ganz gut aus, macht aber nicht was es soll. Denn als Ausgabe erhält man Folgendes.

{{< highlight bash >}}
<pre class=
 style=
<code class=
>10 PRINT "hallo welt"</code>
</pre>
{{< /highlight >}}

Die Lösung ist in dem Fall allerdings recht einfach. Die Ursache ist die fehlende Maskierung (auch escapen genannt) der Anführungszeichen innerhalb des Bereichs "body". 

{{< highlight bash >}}
"wrap_highlight": {
		"prefix": "wrap_highlight",
		"body": [
			"<pre class=\"line-numbers language-bash\" style=\"white-space:pre-wrap;\">",
			"<code class=\"language-bash\">$TM_SELECTED_TEXT</code>",
			"</pre>"
		],
		"description": "Highlight Code"
	},
{{< /highlight >}}

Ändert man das Snippet wie oben angegeben macht es auch das, was es soll.