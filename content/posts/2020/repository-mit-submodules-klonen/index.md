---
title: Repository mit Submodulen klonen
date: 2020-07-04T15:57:04+0200
categories:
- OSBN
- Allgemein
tags:
- Git  
- Submodule
slug: repository-mit-submodulen-klonen
---
Ich habe eben ein Repository bei Github auf einen meiner Rechner geklont und festgestellt, das ein Teil der Daten nicht vorhanden ist. Was daran lag, dass der fehlende Teil als [Submodul](https://git-scm.com/book/de/v2/Git-Tools-Submodule) eingebunden ist. Die Daten liegen also in einem anderen Repository. Um ein Projekt inklusive der Submodule zu klonen, kann man folgenden Befehl nutzen.

{{< highlight bash >}}
git clone --recursive <LINK-ZUM-REPOSITORY>
{{< /highlight >}}

Was aber, wenn man das Hauptprojekt bereits geklont hat? Man wechselt einfach in das Verzeichnis und führt folgende Befehle aus. Hiermit werden nachträglich auch die Dateien der Submodule heruntergeladen.

{{< highlight bash >}}
git submodule init
git submodule update
{{< /highlight >}}
