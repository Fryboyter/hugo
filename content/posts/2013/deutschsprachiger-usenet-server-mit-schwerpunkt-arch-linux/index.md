---
title: Deutschsprachiger Usenet-Server mit Schwerpunkt Arch Linux
date: 2013-02-16T21:49:00+0100
categories:
- Linux
- OSBN
tags:
- Usenet
- Diskussion
- Arch Linux
slug: deutschsprachiger-usenet-server-mit-schwerpunkt-arch-linux
---
Unter der IP 84.200.213.109 steht seit einigen Tagen ein Usenet-Server zum Diskutieren (nicht zum Downloaden) bereit. Schwerpunkt ist im Grunde (Arch-)Linux. Folgende Gruppen sind derzeit vorhanden:

- admin.news.groups: Alles Rund um den Server. Z. B. um neue Gruppen vorzuschlagen.

- alt.nerd: Für Dinge Rund um dien IRC-Server irc.malte-bublitz.de

- alt.nerd.themenabend: Planung für eventuell stattfindenden Themenabende

- comp.os.linux: Für das ganze Linux-Gedöns

- comp.os.linux.archlinux: Für Arch Linux im Speziellen

- comp.os.bsd: Für die BSDler unter uns

Das Ganze ist nicht auf meinem Mist gewachsen, sondern auf dem von Malte Bublitz. Ich rühre hier nur etwas die Werbetrommel, da ich es interessant finde abseits von Foren diskutieren zu können. Oldschool sozusagen. Bisher ist es noch etwas leer, wie man sieht, aber vielleicht ändert es sich ja.

{{< image src="Auswahl_016.webp" alt="Usenet Server" >}}

**Der Server ist nun unter nntp.flying-sheep.de erreichbar.**
