---
title: Meine eigene Installation von Etherpad Lite
date: 2013-05-24T16:25:00+0100
categories:
- Linux
- OSBN
tags:
- Installation
- Etherpad Lite
- Uberspace
slug: meine-eigene-installation-von-etherpad-lite
---
{{< notice info >}}
Das Angebot wurde zwischenzeitlich eingestellt.
{{< /notice >}}

Ab und zu benötige ich eine Plattform, auf der ich Texte veröffentlichen kann, welche dann auch von Dritten änderbar sind. In letzter Zeit habe ich immer auf diverse Webseiten wie [https://pad.lqdn.fr](https://pad.lqdn.fr "https://pad.lqdn.fr") zurückgegriffen, die dies in Form von [Etherpad Lite](http://etherpad.org "Etherpad Lite") ermöglichen. Irgendwie habe ich allerdings immer das Glück, dass immer, wenn jemand das von mir erstellte Pad aufrufen will, das ganze Angebot nicht erreichbar ist.

Damit ist jetzt hoffentlich Schluss. Da ich bei meinem Webspace-Anbieter eine ziemliche Narrenfreiheit habe, habe ich mir jetzt kurzerhand Etherpad Lite auf meinem Webspace installiert. Praktischerweise gab es im Wiki auch gleich eine [Anleitung](https://lab.uberspace.de/guide_etherpad "Etherpad Lite bei Uberspace installieren") dazu. Des Weiteren habe ich mich dann noch, {{% deadlink %}}[hier](https://gist.github.com/programmieraffe/5160385){{% /deadlink %}} bedient und die .htaccess-Datei abgekupfert, damit die Pads auch ohne /p/ in der Adresse funktionieren. Was ich allerdings noch nicht geschafft habe, ist, dass neue Pads automatisch ohne /p/ im Link erstellt werden. Kann mir da jemand auf die Sprünge helfen?

Unter {{% deadlink %}}[http://pad.fryboyter.de](http://pad.fryboyter.de "Etherpad lite bei Fryboyter.de"){{% /deadlink %}} steht der Spaß nun zur Verfügung. Ich werde bis auf Weiteres den Zugriff auch für Dritte erlauben. Sollte es aber ausarten oder missbraucht werden, werde ich das Angebot einstellen und nur noch für mich selbst nutzen.

**Nachtrag 25.05.13: Da seit gestern mehr als 50 Test-Pads erstellt wurden, möchte ich darum bitten, zum reinen Testen {{% deadlink %}}http://pad.fryboyter.de/test{{% /deadlink %}} zu nutzen.**
