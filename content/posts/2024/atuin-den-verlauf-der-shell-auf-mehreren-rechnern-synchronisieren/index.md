---
title: Atuin - Den Verlauf der Shell auf mehreren Rechnern synchronisieren
date: 2024-03-03T11:31:12+0100
nocomments: true
categories:
- OSBN
tags:
- Shell
- Verlauf
- Snychronisation
slug: atuin-den-verlauf-der-shell-auf-mehreren-rechnern-synchronisieren
---
Da ich mehrere Rechner nutze, habe ich die Angewohnheit bestimmte Dinge beispielsweise mit dem Notebook anzufangen, dann eine Pause zu machen (weil ich zum Beispiel schlafen will) und dann mit einem meiner normalen Rechner weiterzumachen. An sich kein Problem, allerdings fehlt mir in solch einem Fall oft der Verlauf der Befehle, die ich in der Shell ausgeführt habe.

Daher habe ich mir nun [Atuin](https://github.com/atuinsh/atuin) zugelegt. Hierbei handelt es sich nicht um die [Schildkröte](https://discworld.fandom.com/wiki/Great_A%27Tuin), sondern um ein Tool, welches die Historie einer Shell lokal in einer SQLite-Datenbank speichert, deren Inhalt man mit anderen Rechnern synchronisieren kann.

Die Einträge in dieser Datenbank werden um diverse Informationen erweitert. Zum Beispiel dem Exit Code, Current Working Directory und so weiter. Die Suche innerhalb der Einträge erfolgt in der Standardkonfiguration "[fuzzy](https://de.wikipedia.org/wiki/Unscharfe_Suche)".

Für die Synchronisation zwischen mehreren Rechnern wird ein Server, den die Entwicklerin von Atuin zur Verfügung stellt, genutzt. Die Übertragung der Daten erfolgt hierbei mittels [E2EE](https://de.wikipedia.org/wiki/Ende-zu-Ende-Verschl%C3%BCsselung). Wer ihr nicht vertraut, kann entweder einen solchen Server selbst hosten (https://docs.atuin.sh/self-hosting/server-setup/) oder Atuin nur lokal nutzen. Letzteres hat im Grunde dann nur den Vorteil der besseren Suche.

Es gibt auch noch andere Tools zur Synchronisation von bereits ausgeführten Befehlen in der Shell. Zum Beispiel https://github.com/cantino/mcfly. Aber als jemand der jeden Roman von Terry Pratchett besitzt und gelesen hat, musste ich mich einfach für Atuin entscheiden.