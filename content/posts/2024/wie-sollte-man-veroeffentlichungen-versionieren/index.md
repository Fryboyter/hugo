---
title: Wie sollte man Veröffentlichungen versionieren?
date: 2024-10-11T18:57:00+0200
nocomments: true
categories:
- OSBN
tags:
- Versionierung
slug: wie-sollte-man-veroeffentlichungen-versionieren
---
Wenn es um die Entwicklung von Software geht, wird oft eine Versionierung wie 0.98.1 verwendet. Historisch gesehen hat dies zur Folge, dass viele Nutzer erwarten, dass Version 1.0 ausgereift ist.

Diese [Erwartungshaltung](https://en.wikipedia.org/wiki/Software_versioning#Version_1.0_as_a_milestone) führt dazu, dass einige Projekte eine Veröffentlichung der Version 1.0 vermeiden oder zumindest möglichst lange hinauszögern wollen. Ein Beispiel dürfte [Hugo](https://gohugo.io) sein. Nach Version 0.99.1 wurde nicht Version 1.0 veröffentlicht, sondern Version 0.100.0. Aktuell ist Version 0.135.0.

Natürlich kann man als Entwickler ignorieren, was manche Nutzer von Version 1.0 erwarten. Oder man kann diese Version so lange hinauszögern bis man bereit ist diese Versionsnummer zu verwenden.

Aber man kann sich als Entwickler auch überlegen, ob man dieses Versionierung überhaupt nutzen möchte. Ich persönlich würde eher "[Calendar Versioning](https://calver.org)" verwenden. Zumal meiner Meinung nach 2024.10.11 aussagekräftiger als 0.78.1 ist. Projekte wie pip, Unity oder Ubuntu nutzen CalVer bereits.

Mir geht es jetzt nicht darum, dass nun jedes Projekt auf CalVer oder einer anderen Versionierung von Veröffentlichung wechselt. Aber ich halte es für sinnvoll, wenn sich Entwickler Gedanken machen, ob eine andere Versionierung heutzutage nicht vielleicht sinnvoller wäre.