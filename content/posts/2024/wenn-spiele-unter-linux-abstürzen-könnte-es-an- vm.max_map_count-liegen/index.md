---
title: Wenn Spiele unter Linux abstürzen, könnte es an vm.max_map_count liegen
date: 2024-03-26T10:19:05+0100
nocomments: true
categories:
- OSBN
tags:
- Spiele
- Linux
slug: wenn-spiele-unter-linux-abstuerzen-koennte-es-an-vm-max-map-count-liegen
---
Unter Linux kommt es immer öfter vor, dass bestimmte Spiele, wie zum Beispiel CS:GO 2, Star Citizen oder The Finals abstürzen. Die Fehlermeldung könnte wie folgt aussehen.

{{< highlight bash >}}err:virtual:try_map_free_area mmap() error cannot allocate memory, range 0xf4f60000-0x8ad0000, unix_prot 0x3{{< /highlight >}}

Meist liegt es daran, dass [vm.max_map_count](https://access.redhat.com/solutions/99913) einen zu niedrigen Wert hat.

Der Standardwert, welches fest im Kernel eingetragen ist, ist derzeit 65530. Was heutzutage für viele Spiele, aber auch für andere Anwendungen wie Elasticsearch oft zu wenig ist.

Valve hat auf dem Steam Deck den Wert bereits auf 2147483642 erhöht. Einige Distributionen wie Fedora oder Ubuntu haben den Wert ebenfalls erhöht. Allerdings hat man sich für einen deutlich geringeren Wert von 1048576 entschieden. Dieser sollte aber aktuell ausreichend sein.

Arch Linux ist aktuell noch eine der Distributionen die den Standardwert nutzen, sodass es zu besagten Abstürzen kommen kann. Die Entwickler von Arch besprechen aber aktuell eine Erhöhung. Eine manuelle Anpassung (nicht nur unter Arch Linux) ist aber auch kein Problem.

Hierfür erstellt man die Datei /etc/sysctl.d/10-map-count.conf. Wie man die Datei nennt, ist im Grunde egal, solange die mit 10 beginnt und mit .conf endet. Die 10 ist wichtig, da umso niedriger die Zahl ist umso früher werden die Konfigurationsdateien vom Betriebssystem berücksichtigt.

In dieser Datei trägt man nun zum Beispiel {{< mark >}}vm.max_map_count=1048576{{< /mark >}} ein und speichert die Datei. Nach einem Neustart oder nach dem Ausführen von {{< mark >}}sysctl -p{{< /mark >}} sollte der neue Wert gültig sein. Prüfen kann man dies mit dem Befehl {{< mark >}}sysctl vm.max_map_count{{< /mark >}}. Betroffene Spiele bzw. Programme sollten sich nun normal starten und nutzen lassen.

Aufgrund der aktuellen Diskussion wegen vm.max_map_count unter Arch Linux wurde den Kernel-Entwicklern vorgeschlagen den Wert offiziell zu erhöhen (https://lore.kernel.org/lkml/566168554.272637693.1710968734203.JavaMail.root@zimbra54-e10.priv.proxad.net/T/#u). Ob diese zustimmen steht derzeit noch nicht fest.