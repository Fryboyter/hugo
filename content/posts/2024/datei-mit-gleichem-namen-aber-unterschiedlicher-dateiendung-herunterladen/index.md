---
title: Datei mit gleichem Namen aber unterschiedlicher Dateiendung herunterladen
date: 2024-07-05T15:55:24+02:00
nocomments: true
categories:
- OSBN
tags:
- Wget 
slug: datei-mit-gleichem-namen-aber-unterschiedlicher-dateiendung-herunterladen
---
Ein Mirror von Arch Linux bietet beispielsweise das Verzeichnis /iso/latest/ mit folgendem Inhalt an.

{{< highlight bash >}}
../
arch/
archlinux-2024.07.01-x86_64.iso
archlinux-2024.07.01-x86_64.iso.sig
archlinux-2024.07.01-x86_64.iso.torrent
archlinux-bootstrap-2024.07.01-x86_64.tar.zst
archlinux-bootstrap-2024.07.01-x86_64.tar.zst.sig
archlinux-bootstrap-x86_64.tar.zst
archlinux-bootstrap-x86_64.tar.zst.sig
archlinux-x86_64.iso
archlinux-x86_64.iso.sig
b2sums.txt
sha256sums.txt
{{< /highlight >}}

Von diesem Mirror will man nun sowohl die Datei archlinux-x86_64.iso als auch die Datei archlinux-x86_64.iso.sig herunterladen.

Mit wget wäre das mit beispielsweise {{< mark >}}wget https:&#47;&#47;arch.jensgutermuth.de&#47;iso&#47;latest&#47;archlinux-x86_64.iso{{< /mark >}} und {{< mark >}}wget https:&#47;&#47;arch.jensgutermuth.de&#47;iso&#47;latest&#47;archlinux-x86_64.iso.sig{{< /mark >}} durchführbar. Aber muss man unbedingt pro Datei einen Befehl ausführen?

Da in diesem Fall der Dateiname gleich ist und sich nur die Dateiendung unterscheidet, lautet die Antwort nein.

Mit dem Befehl {{< mark >}}wget https:&#47;&#47;arch.jensgutermuth.de&#47;iso&#47;latest&#47;archlinux-x86_64.iso{,.sig}{{< /mark >}} kann man in einem Rutsch sowohl die Iso-Datei als auch die Sig-Datei herunterladen. 

Nehmen wir nun an, dass es auch noch eine Datei archlinux-x86_64.iso.sha256 gibt und man auch diese mit herunterladen will.

In diesem Fall erweitert man den Befehl einfach wie folgt.

{{< mark >}}wget https:&#47;&#47;arch.jensgutermuth.de&#47;iso&#47;latest&#47;archlinux-x86_64.iso{,.sig,.sha256}{{< /mark >}}
