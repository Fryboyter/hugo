---
title: Kurztest des Chipkartenlesers cyberJack RFID standard unter Linux
date: 2024-08-08T12:21:18+0200
nocomments: true
categories:
- OSBN
tags:
- Chipkartenleser
- Reiner SCT
slug: kurztest-des-chipkartenlesers-cyber-jack-rfid-standard-unter-linux
---
Bei mir stehen in den nächsten Monaten einige Behördengänge an, auf die ich keine Lust habe. Wie ich aber erstaunt festgestellt habe, hält sogar in Bayern langsam die Digitalisierung Einzug, sodass ich diese online erledigen kann.

Allerdings benötige ich hierfür entweder eine BayernID oder ein Nutzerkonto Bund. Hierfür benötige ich wiederum die Online-Ausweisfunktion. Diese war in der zuständigen Gemeindeverwaltung schnell freigeschaltet (ich hatte damals noch die Wahl und hatte mich gegen eine Freischaltung entschieden).

Was ich allerdings nicht bedacht hatte war, dass ein Personalausweis mit Online-Ausweisfunktion per RFID funktioniert, was mein bisheriger Kartenleser (cyberJack e-Com von Reiner SCT) leider nicht unterstützt. Also musste ein neuer Chipkartenleser her.

Da ich mit Produkten von Reiner SCT bisher gute Erfahrungen gemacht habe und Linux offiziell unterstützt wird, habe ich mich für den [cyberJack RFID standard](https://shop.reiner-sct.com/chipkartenleser-fuer-die-sicherheitsklasse-3/cyberjack-rfid-standard-usb) entschieden. Diesen gibt es bei diversen Händlern auch günstiger.

Da der Hersteller nur einen Treiber ([pcsc-cyberjack](https://help.reiner-sct.com/de/support/solutions/articles/101000480008)) für alle Kartenleser anbietet, welcher bereits installiert war, hat das neue Gerät direkt nach dem Anschließen über USB funktioniert.

Von der Größe her ist der neue Kartenleser kleiner, aber meiner Meinung nach nicht zu klein. Das Kabel führt, wie auch beim cyberJack e-Com seitlich aus dem Gerät heraus. Nach hinten wäre mir lieber gewesen. Der metallene Standfuß bietet zwar eine Führung an mit der man das Kabel nach hinten "umleiten" kann aber aus dieser rutscht das Kabel ziemlich schnell wieder heraus. Klebeband dürfte hier weiterhelfen.

Was die Tasten betrifft, sind diese nicht mehr rund bzw. oval, sondern länglich und dünn. Eine Bedienung ist allerdings weiterhin kein Problem zumal zwischen den Tasten genug Abstand ist. Allerdings haben die einzelnen Tasten ziemlich viel Spiel und wackeln daher. Das muss nicht unbedingt negativ sein aber warum konnte man nicht einfach bei den runden Tasten bleiben? Da hat selbst nach über 10 Jahren der Nutzung nichts gewackelt. Dafür musste man die Tasten etwas fester drücken.

Kommen wir zum Display. Diese ist meiner Meinung nach besser als abzulesen als das des alten Kartenlesers.

Was gibt es sonst noch zu sagen? Die zwei Einschübe für die Karten (oben der für die normalen Chipkarten und darunter der für die kontaktlosen Karten) sind am hinteren Ende des Kartenlesers vorhanden. Die Karten in den Schlitz für die kontaktlosen Karten zu stecken ist meiner Meinung nach etwas "fummelig", wenn man mit etwas Abstand direkt vor dem Gerät sitzt.

Auch wenn der neue Kartenleser einige Dinge anders macht wie ich es gerne hätte (z. B. das Kabel) werde ich ihn wohl trotzdem behalten. Im Grunde benutze ich ihn nur ein paar Mal im Monat für Online-Banking und nun auch für den einen oder anderen Behördengang.