---
title: Kurztest Motu M2 USB-Audio-Interface
date: 2024-08-17T15:48:36+0200
nocomments: true
categories:
- OSBN 
tags:
- Audio
slug: kurztest-motu-m2-usb-audio-interface
---
Viele werden vermutlich das Musikhaus Thomann in Burgebrach in der Nähe von Bamberg kennen. Ein Laden der sowohl online als auch offline alles Erdenkliche zum Thema Klang verkauft und als Europas größter Musikhändler gilt.

Bei Thomann kaufe ich gerne Dinge zum Thema Audio ein, weil die Beratung einfach super ist. So hatte ich mit einem Mitarbeiter ca. 2 Wochen lang E-Mail-Kontakt bis wir ein Mikrofon gefunden hatten, dass für meinen Anwendungsfall geeignet war. Andere Händler hätten meine Nachfragen vermutlich schon deutlich früher ignoriert.

Schon länger wollte ich mir Thomann mal vor Ort ansehen anstelle immer nur online zu bestellen. Bisher bin ich nur nie dazu gekommen. Da ich mir ein neues USB-Audiointerface kaufen wollte, habe ich mich heute Morgen spontan auf den Weg gemacht. Um es kurz zu machen, wer in der Nähe von Bamberg ist, sollte mal bei Thomann vorbeischauen. Irre, was die alles anbieten.

OK, nun aber zum eigentlichen Thema. 

Gekauft habe ich schlussendlich das [Motu M2](https://motu.com/de/products/m-series/m2/), welches mit 233 Euro nicht gerade das billigste aber bei weitem nicht das teuerste Audiointerface ist. Daran angeschlossen ist aktuell ein [Beyerdynamic DT 880](https://www.beyerdynamic.de/dt-880-edition.html) (Kopfhörer) sowie ein [Rode NTG2](https://rode.com/de/microphones/shotgun/ntg2) (Mikrofon).

Das Gehäuse sowie alle Drehregler sind aus Aluminium gefertigt. Selbst wenn man ein Mobiltelefon darauf legt und beispielsweise eine Nachricht versendet, hört man keine Störgeräusche. Ich will jetzt nicht die Aussage "gefertigt für die Ewigkeit" verwenden aber solide ist das Teil auf jeden Fall.

Was ich gut finde ist, dass es eine detailliertere Pegelanzeige, sowohl für IN als auch OUT gibt. Viele Interfaces haben nur Lämpchen (rot oder grün).

Ach ja, Linux. Zumindest mit Kernel 6.10.5 funktioniert das Teil "out of the box" nachdem man es für In- und Output eingestellt hat. Sowohl die Ein- als auch die Ausgabe ist meiner laienhaften Meinung sehr gut.

Der Mitarbeiter von Thomann war sich nicht ganz sicher, ob das Motu M2 den Beyerdynamic-Kopfhörer mit 250 Ohm richtig befeuern kann. Er hat mich daher extra auf die 30 Tage Rückgaberecht hingewiesen, die auch beim Kauf im Laden gilt. Ich bin bei dem Thema weit davon ein Experte zu sein. Aber wenn ich beispielsweise den Beginn von https://www.youtube.com/watch?v=J0g0yZKbJP4&t=34s abspiele und sowohl die Lautstärke als auch Gain am Interface auf Maximum stelle, bin ich danach gefühlt taub, habe aber keine störenden Geräusche bei der Wiedergabe. Es ist mir einfach zu laut. Was für mich beispielsweise in Ordnung ist, ist die Lautstärke auf 100 Prozent zu setzen und den Regler für Gain auf ca. 10 bis 12 Uhr zu stellen. Ich würde daher sagen, ja der Motu M2 hat genug "Wumms". Mit den Einstellungen werde ich mich aber noch genauer befassen.

Wer also bereit ist, mehr als 200 Euro auszugeben, sollte sich durchaus das Motu M2 ansehen. 

Ich hätte übrigens noch ein funktionsfähiges Focusrite Scarlett Solo (08/2018) herumliegen. Bei Interesse bitte per E-Mail melden.