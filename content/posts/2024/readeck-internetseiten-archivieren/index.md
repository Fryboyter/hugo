---
title: Readeck - Internetseiten archivieren
date: 2024-09-12T15:56:50+0200
nocomments: true
categories:
- OSBN
tags:
- Wallabag
- Internetseiten
- Archivierung
slug: readeck-internetseiten-archivieren
---
Aufgrund des Artikels [https://www.trommelspeicher.de/blog/die-eigene-lesezeichenverwaltung](https://www.trommelspeicher.de/blog/die-eigene-lesezeichenverwaltung) und weil ich mir auch im Moment alternativen zu [Wallabag](https://github.com/wallabag/wallabag) ansehe, ist dieser Artikel entstanden.

Über die Jahre habe ich mir eine umfangreiche Lesezeichen-Sammlung erstellt. Bis eines Tages eine wichtige Seite nicht mehr über das Lesezeichen erreichbar war. Der Betreiber hatte die Seite gelöscht und aufgrund einer Nachfrage meinerseits hatte er auch keine Datensicherung mehr. Über [https://web.archive.org](https://web.archive.org) konnte man ebenfalls keine gespeicherte Version aufrufen. Kurz gesagt, die Informationen sind verloren gegangen.

Damals habe ich mir daher eine eigene Wallabag-Instanz installiert. Damit lassen sich, zumindest die meisten, Seiten abspeichern und somit der Inhalt archivieren. Ich schreibe bewusst "die meisten", da sich Seiten wie Codepen oder Reddit damit nicht speichern lassen. Vermutlich wegen JavaScript oder irgendwelcher Maßnahmen die es verhindern. In solch einem Fall wird daher ebenfalls nur ein Lesezeichen angelegt und der Inhalt wird nicht gespeichert.

Bis auf besagte Ausnahmen hat Wallabag die letzten Jahre gut funktioniert. Aber das Programm ist teilweise recht träge und die grafische Oberfläche könnte man auch verbessern. Mit Version 3.0 dürfte sich einiges an der aktuellen Situation ändern. Aber die Entwickler lassen sich bewusst Zeit (z. B. https://github.com/wallabag/wallabag/discussions/6177 oder https://github.com/wallabag/wallabag/discussions/6884). Und ja, ich finde das nicht schlecht. Also im Grunde wie damals, wenn jemand ID Software gefragt hat, wenn denn ein bestimmtes Spiel fertig ist, und die Standardantwort "wenn es fertig ist" gelautet hat.

Trotzdem habe ich mir in den letzten Tagen Alternativen angesehen, da ich Urlaub habe. Und bisher muss ich sagen, dass für mich [Readeck](https://readeck.org/) einen sehr guten Eindruck macht. Hier mal ein paar Beispiele die ich nicht schlecht finde.

- Das Tool ist mit Golang erstellt, sodass das Programm aus einer Datei besteht. Somit ist eine Installation sehr einfach.
- Die grafische Oberfläche ist, verglichen mit Wallabag, meiner Meinung nach besser.
- Ein Import aus einigen anderen Lösungen (wie Wallabag) ist vorhanden.
- Ein Export von gespeicherten Seiten ist in Form von Epub-Dateien möglich.
- Innerhalb der gespeicherten Artikel lässt sich ein festgelegter Text hervorheben. Ich bin mir nicht sicher ob Wallabag die auch unterstützt.

Wer also eine Lösung sucht, mit der sich Internetseiten komplett abspeichern lassen, bzw. wer eine Alternative zu Wallabag sucht, sollte sich daher Readeck ansehen.

Da ich auf die Lesezeichenverwaltung von trommelspeicher.de verwiesen habe, möchte ich noch anmerken, dass weder Wallabag noch Readesk die besseren Lösungen sind. Es kommt immer darauf an, was man erreichen will. Ich habe daher in Wallabag auch nur wirklich wichtige Seiten gespeichert und für den Rest Bookmarks erstellt. Nur werde ich nie wieder nur Bookmarks erstellen.