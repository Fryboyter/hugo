---
title: Feststelltaste als zweite Escape-Taste nutzen
date: 2024-08-27T16:04:49+0200
nocomments: true
categories:
- OSBN
tags:
- KDE
- Plasma
- Helix
slug: feststelltaste-als-zweite-escape-taste-nutzen
---
Im Terminal Emulator nutze ich inzwischen fast nur noch den modalen Editor [Helix](https://helix-editor.com). Um beispielsweise vom Einfügemodus in den normalen Modus zu wechseln, muss man in der Standardkonfiguration die Escape-Taste drücken. Was ich persönlich nicht wirklich ergonomisch finde.

Daher habe ich schon vor einiger Zeit den Shortcut {{< mark >}}kj{{< /mark >}} angelegt mit dem ich wieder in den normalen Modus wechsle, sofern ich mich im Einfügemodus befinde. 

{{< highlight toml >}}
[keys.insert]
k = { j = "normal_mode" }
{{< /highlight >}}

Damals habe ich das für eine gute Idee gehalten, weil meine Finger auf der Tastatur sowieso auf k und j liegen. Und mir ist einfach kein Wort eingefallen bei dem auf ein k ein j folgt. Seit kurzer Zeit kenne ich zwei Wörter, bei denen dies zutrifft. Reykjavik und Hallgrimskirkja.

Da der Shortcut Vorrang hat, schaffe ich es also nicht diese Wörter manuell zu schreiben. Verdammte Isländer. ;-)

Nun gibt es auf fast jeder Tastatur eine Taste, die ich im Grunde nicht nutze. Die Feststelltaste. Warum also diese nicht nutzen, um den Modus zu wechseln?

Unter Helix geht das schon mal nicht, weil diese Taste nicht unterstützt wird ([https://docs.helix-editor.com/remapping.html#special-keys-and-modifiers](https://docs.helix-editor.com/remapping.html#special-keys-and-modifiers)). 

Wie sieht es mit einer Änderung abseits von Helix aus? Im Internet findet man hierzu diverse Lösungen mit beispielsweise Tools wie [caps2esc](https://gitlab.com/interception/linux/plugins/caps2esc). Unter KDE Plasma geht es aber auch einfach so. 

Mit dem Befehl {{< mark >}}kcmshell6 kcm_keyboard{{< /mark >}} ruft man als Erstes die Einstellungen für die Tastatur auf. Wer will, kann natürlich auch einfach die Systemeinstellungen im Startmenü aufrufen und dort dann die Einstellungen für die Tastatur auswählen.

Dort klickt man dann auf "Key Bindings" (irgendwie ist bei Plasma 6 noch so einiges nicht übersetzt worden). Dann setzt man als Nächstes den Haken bei "Configure keyboard options". In der Liste darunter sucht man dann "Verhalten der Feststelltaste" und hakt dann die darunter befindliche Option "Feststelltaste als zusätzliche Esc-Taste" an. Abschließend klickt man noch auf "Anwenden" und das war es schon. Von nun an sollte die Feststelltaste wie die Escape-Taste funktionieren.

Ich gehe mal stark davon aus, dass diese Einstellung nur in KDE Plasma funktioniert. Wer diese Änderung tatsächlich überall, also zum Beispiel auch in einer TTY-Session haben will, muss wohl auf ein Tool wie caps2esc zurückgreifen.