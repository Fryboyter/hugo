---
title: Chezmoi - Dateien ignorieren
date: 2024-10-03T00:18:46+0200
nocomments: true
categories:
- OSBN
tags:
- Chezmoi
slug: chezmoi-dateien-ignorieren
---
Mit [chezmoi](https://www.chezmoi.io) lassen sich nicht nur einzelne Konfigurationsdateien verwalten, sondern auch ganze Verzeichnisse.

Auf meinen Rechnern sieht der Inhalt des Verzeichnisses ~/.config/zsh beispielsweise folgendermaßen aus.

{{< highlight bash >}}
zsh
├── aliase.zsh
├── completion.zsh
├── environment.zsh
├── functions.d
│   ├── clrhist
│   ├── sshf
│   ├── up
│   └── updpkgbuild
├── functions.zsh
├── options.zsh
├── prompt.zsh
├── .zhistory
└── .zshrc
{{</ highlight >}}

Führt man {{< mark >}}chezmoi add ~/.config/zsh{{< /mark >}} aus, werden alle darin enthaltenen Dateien übernommen. Was aber nicht immer gewünscht ist. 

Damit chezmoi bestimmte Dateien bzw. Verzeichnisse ignoriert, legt man in der Standardkonfiguration unter ~/.local/share/chezmoi die Datei .chezmoiignore an und trägt die Dateien / Verzeichnisse ein die ignoriert werden sollen. Will man nun, dass chezmoi die Datei .zhistory ignoriert, reicht es in dem Fall aber nicht, einfach .zhistory in die Datei zu schreiben. Man muss [doublestar.Match](https://www.chezmoi.io/reference/special-files-and-directories/chezmoiignore/) verwenden. Somit würde bei diesem Beispiel der Eintrag {{< mark >}}**/.zhistory{{< /mark >}} funktionieren.

Führt man nun {{< mark >}}chezmoi add ~/.config/zsh{{< /mark >}} aus, sollte "chezmoi: warning: ignoring .config/zsh/.zhistory" angezeigt werden und die Datei sollte nicht übernommen werden.