---
title: Einstellungen und Telefonbücher einer FRITZ!Box sichern
date: 2024-12-20T17:18:05+0100
nocomments: true
categories:
- OSBN
tags:
- Fritzbox 
slug: einstellungen-und-telefonbuecher-einer-fritzbox-sichern
---
Heute Vormittag hat mich ein Bekannter gebeten, ob ich nicht kurzfristig vorbeikommen könnte, weil seine FRITZ!Box "abgeraucht" ist.
Erst habe ich gedacht, dass der Begriff "abgeraucht" nicht wörtlich gemeint war. Vor Ort, als ich meine Nase an das Gehäuse gehalten habe, musste ich feststellen, dass diese tatsächlich "abgeraucht" ist, da man deutlich verschmortes Plastik riechen konnte.

Die gute Nachricht, ich konnte kurzfristig noch einen Ersatz für die FRITZ!Box auftreiben. Die schlechte Nachricht war allerdings, dass es keine Sicherung der Einstellungen und Telefonbücher gab. Gut, die FRITZ!Box habe ich wieder entsprechend eingestellt. Die Telefonbücher sind allerdings das Problem meines Bekannten. Hierfür fehlt mir die Zeit und vor allem die Lust.

Da ich selbst eine FRITZ!Box nutze habe ich mir überlegt, wann ich selbst das letzte Mal ein Backup der Konfiguration und der Telefonbücher gemacht habe. Sagen wir es mal so. Ich habe Backups was die FRITZ!Box betrifft aber die sind schon etwas älter. Und manuell erstellt.

Solche Dinge muss man aber doch irgendwie automatisieren können. Ja, man kann. Es gibt das Programm [Fritz!Box Tools](https://www.mengelke.de/Projekte/FritzBox-Tools) von Michael Engelke. Damit kann man einiges anstellen, ohne die grafische Oberfläche einer FRITZ!Box direkt zu nutzen. Wie zum Beispiel das Auslesen der Zugangsdaten für WLAN, DynDNS oder des DSL-Zugangs. Oder eben auch das Erstellen von einem Backup der Einstellungen und Telefonbücher. Hierfür muss man aber die Zugangsdaten der FRITZ!Box kennen. Die Fritz!Box Tools nutzen also keine vorhandene Sicherheitslücke oder ähnliches.

Um beispielsweise die Konfiguration an sich zu sichern, kann man folgenden Befehl nutzen.

{{< highlight bash >}}
fb_tools.php <passwort>@<ip-nummer-der-fritzbox> konfig export backup-$(date +%Y-%m-%d)
{{< /highlight >}}

Hiermit würde eine Sicherung der Konfiguration in Form einer Datei wie backup-2024-12-20 erstellt.

Bezüglich der Telefonbücher wäre folgender Befehl anwendbar.

{{< highlight bash >}}
fb_tools.php <passwort>@<ip-nummer-der-fritzbox> telefonbuch export backup-$(date +%Y-%m-%d)
{{< /highlight >}}

Sollte man mehrere Telefonbücher nutzen, weil man beispielsweise [PhoneBlock ](https://phoneblock.net/phoneblock/) nutzt, um sich ungewünschte Anrufer vom Hals zu halten, würde dieser Befehl ein Verzeichnis in Form von backup-2024-12-20 erstellen und dort dann die Telefonbücher abspeichern und keine einzelne Datei.

Auf der verlinkten Internetseite von Fritz!Box Tools gibt es eine ziemlich umfangreiche Anleitung was noch alles möglich ist und was nicht. Daher gehe ich jetzt nicht weiter darauf ein. Aber mit den genannten Befehlen kann man sich relativ einfach ein Script erstellen, dass mittels eines Timers / Cronjobs die gewünschten Daten exportiert, welche man dann in seinem normalen Backup mit einbezieht.