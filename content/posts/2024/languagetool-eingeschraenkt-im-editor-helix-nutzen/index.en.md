---
title: Use LanguageTool with restrictions in the Helix editor
date: 2024-09-15T12:16:13+0200
nocomments: true
categories:
- Englis(c)h
tags:
- Helix
- LanguageTool
slug: use-language-tool-with-restrictions-in-the-helix-editor
---
The editor [Helix](https://helix-editor.com) does not offer a spell checker by default. This means that in some cases the editor should be used with caution. Therefore, but also for other reasons, I usually use VS Code to create the articles I publish on fryboyter.de. For spell-checking, I use LanguageTool, for which there is a corresponding plugin. Now I have asked myself whether there is a way to use LanguageTool with Helix.

Yes, there is. To do this, you must first install https://github.com/valentjn/ltex-ls. With Arch Linux you will find it in the form of ltex-ls-bin in the AUR. Now create the file languages.toml under ~/.config/helix/ with the following content (the path to ltex-ls may have to be adapted depending on the distribution and type of installation).

{{< highlight toml >}}
[[language]]
name = "markdown"
language-servers = [{ name = "ltex"}]
file-types = ["md", "txt"]

[language-server.ltex]
command = "/usr/bin/ltex-ls"
config = { ltex.language = "de-DE" }
{{< /highlight >}}

This checks markdown and text files for errors in the German language. If you want to use a language other than German, you can change the last line accordingly. A list of supported languages can be found at https://valentjn.github.io/ltex/settings.html#ltexlanguage.

However, this solution has a disadvantage. Under VS Code, LanguageTool highlights incorrect words and offers correction options. With Helix, incorrect words are only highlighted by underlining them. Corrections must therefore still be made by the user.