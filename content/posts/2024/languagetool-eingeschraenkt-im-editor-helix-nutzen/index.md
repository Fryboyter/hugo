---
title: LanguageTool eingeschränkt im Editor Helix nutzen
date: 2024-09-15T12:16:13+0200
nocomments: true
categories:
- OSBN
tags:
- Helix
- LanguageTool
slug: language-tool-eingeschraenkt-im-editor-helix-nutzen
---
Der Editor [Helix](https://helix-editor.com) bietet von Haus aus keine Rechtschreibprüfung. Somit ist eine Nutzung des Editors in manchen Fällen somit mit Vorsicht zu genießen. Daher, aber auch aus anderen Gründen, nutze ich zum Erstellen der Artikel die ich auf fryboyter.de veröffentliche in der Regel VS Code. Für die Rechtschreibprüfung nutze ich LanguageTool für das es ein entsprechendes Plugin gibt. Nun habe ich mir die Frage gestellt, ob es nicht doch eine Möglichkeit gibt, LanguageTool auch mit Helix zu nutzen.

Ja, gibt es. Hierfür muss man als erstes https://github.com/valentjn/ltex-ls installieren. Bei Arch Linux findet man es in Form von ltex-ls-bin im AUR. Nun erstellt man unter ~/.config/helix/ die Datei languages.toml mit folgendem Inhalt (den Pfad zu ltex-ls muss man, je nach Distribution und Art der Installation eventuell anpassen).

{{< highlight toml >}}
[[language]]
name = "markdown"
language-servers = [{ name = "ltex"}]
file-types = ["md", "txt"]

[language-server.ltex]
command = "/usr/bin/ltex-ls"
config = { ltex.language = "de-DE" }
{{< /highlight >}}

Damit werden Markdown- sowie Text-Dateien auf Fehler in der deutschen Sprache geprüft. Will man eine andere Sprache als Deutsch verwenden, kann man die letzte Zeile entsprechend. Unter https://valentjn.github.io/ltex/settings.html#ltexlanguage findet man eine Auflistung der unterstützten Sprachen.

Allerdings hat diese Lösung einen Nachteil. Unter VS Code hebt LanguageTool fehlerhafte Wörter hervor und bietet Korrekturmöglichkeiten an. Mit Helix werden fehlerhafte Wörter nur hervorgehoben, indem sie unterstrich werden. Eine Korrektur muss also weiterhin von Nutzer selbst erfolgen.