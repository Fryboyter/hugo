---
title: Besser nicht mehr auf linuxatemyram.com verweisen
date: 2024-02-08T11:51:17+0100
nocomments: true
categories:
- OSBN
tags:
- Speicher
- Linux
slug: besser-nicht-mehr-auf-linuxatemyram-com-verweisen
---
{{< notice hinweis >}}
Der Pull Request wurde vor ein paar Tagen angenommen und www.linuxatemyram.com somit aktualisiert.
{{< /notice >}}

Zumindest bis auf Weiteres. Wenn es um die Speicherverwaltung von Linux geht bzw. darum wie viel Speicher tatsächlich belegt bzw. frei ist, wird gerne auf [www.linuxatemyram.com](https://www.linuxatemyram.com) verwiesen. Immer mehr Nutzer raten von dieser Quelle allerdings ab, weil die dort genannten Informationen teilweise veraltet, ungenau bzw. falsch sind. Und das seit Jahren.

Ein Nutzer hat sich im April letzten Jahres die Mühe gemacht und einen [Pull Request](https://github.com/koalaman/linuxatemyram.com/pull/31) mit 12 Commits erstellt, um die Internetseite an die aktuellen Gegebenheiten anzupassen.

So wie es aussieht hat die betreffende Seite für den Betreiber, welcher übrigens auch für das von mir geschätzte Tool [shellcheck](https://github.com/koalaman/shellcheck) verantwortlich ist, allerdings keine allzu hohe Priorität. Zumindest ist bisher weder eine Reaktion auf den Pull Request erfolgt noch wurde dieser angenommen oder abgelehnt. Vielleicht wäre es daher sinnvoller die Seite einfach zu deaktivieren. Zumal .com Domains ja auch nicht zu den billigsten Domains gehören.
