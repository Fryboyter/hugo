---
title: Bei Mastodon wird wieder auf neue Artikel hingewiesen
date: 2024-02-11T12:15:05+0100
nocomments: true
categories:
- OSBN
tags:
- Mastodon
- IFTTT
slug: bei-mastodon-wird-wieder-auf-neue-artikel-hingewiesen
---
Gestern hatte ich einen [Artikel](/vorerst-keine-veroeffentlichungen-mehr-bei-mastodon/) veröffentlicht, in dem ich darauf hingewiesen habe, dass ich vorerst nichts mehr (automatisiert) auf Mastodon veröffentlichen werden. Grund hierfür war, dass nach der Deaktivierung des Applets (durch mich selbst) bei [IFTTT](https://ifttt.com) eine erneute Aktivierung gescheitert ist. Der Grund ist wohl, dass die Funktion, neue Einträge eines RSS Feeds bei Mastodon (oder anderen Plattformen) per Webhook zu veröffentlichen, nun kostenpflichtig ist.

Bis gestern hat das Applet, soweit ich es nachvollziehen kann, aber seinen Dienst getan. Hätte ich es nicht deaktiviert, würde es vermutlich immer noch funktionieren. Wenn ich recht habe, finde ich die Art Leute zu kostenpflichtigen Tarifen zu "überreden" fragwürdig. Laut Quellen wie beispielsweise https://old.reddit.com/r/ifttt/comments/18p7pxa/webhooks_suddenly_stopped_working_requires_pro/ sind scheinbar auch andere Nutzer betroffen.

Aufgrund meines Artikels hat PepeCyB eine Alternative bei https://rss-parrot.net erstellt [https://fryboyter.de/vorerst-keine-veroeffentlichungen-mehr-bei-mastodon/#isso-977](/vorerst-keine-veroeffentlichungen-mehr-bei-mastodon/#isso-977). Und Masin hat auf den Dienst https://mastofeed.org hingewiesen [https://fryboyter.de/vorerst-keine-veroeffentlichungen-mehr-bei-mastodon/#isso-978](/vorerst-keine-veroeffentlichungen-mehr-bei-mastodon/#isso-978)

Vielen Dank hierfür.

Ich habe mich allerdings für eine andere Lösung entschieden, da ich möglichst unabhängig von Diensten Dritter sein möchte. Gestern hatte ich mir noch diverse alternative Lösungen angesehen. Schlussendlich bin ich bei https://gitlab.com/chaica/feed2toot gelandet. Das Tool macht, was es soll und man kann es leicht selbst hosten. Auf "Dirks Logbuch" wurde schon vor einiger Zeit eine entsprechende [Anleitung](https://www.deimeke.net/dirk/blog/index.php?/archives/4012-Blogeintraege-in-Mastodon-ankuendigen.html) veröffentlicht, an der ich mich orientiert habe.

Das Konto bei IFTTT habe ich übrigens gelöscht.