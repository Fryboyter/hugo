---
title: Virtuelle Maschinen von VirtualBox auf KVM umstellen
date: 2024-02-21T21:02:35+0100
nocomments: true
categories:
- OSBN
tags:
- VirtualBox
- KVM
slug: virtuelle-maschinen-von-virtual-box-auf-kvm-umstellen
---
So mancher Nutzer wird für virtuelle Maschinen das Programm [VirtualBox](https://www.virtualbox.org) nutzen. Diese virtuellen Maschinen liegen in der Regel in Form von VDI-Dateien vor. Leider ist dieses Format nicht mit [KVM](https://de.wikipedia.org/wiki/Kernel-based_Virtual_Machine) kompatibel.

KVM oder besser gesagt [QEMU](https://www.qemu.org) nutzt nämlich das Format qcow/qcow2. QEMU bietet glücklicherweise eine einfache Lösung die virtuellen Maschinen von einem Format in ein anderes umzuwandeln.

{{< highlight bash >}}
qemu-img convert CentOS7.vdi -O qcow2 CentOS7.qcow2
{{< /highlight >}}

Mit diesem Beispiel wird anhand der unter VirtualBox erstellten virtuellen Maschine CentOS7.vdi eine identische virtuelle Maschine im gcow2-Format (CentOS7.qcow2) erzeugt. 

Die Datei CentOS7.vdi bleibt hierbei erhalten und kann anschließend gelöscht werden. Die Datei CentOS.qcow2 benötigt ungefähr den gleichen Speicherplatz.

In manchen Fällen kann es vorkommen, dass das direkte Umwandeln nicht funktioniert. In solch einem Fall hilft es meist die Datei CentOS7.vdi mit VirtualBox mit folgendem Befehl in ein RAW-Image umzuwandeln und dieses dann in das Format umzuwandeln das QEMU unterstützt.

{{< highlight bash >}}
VBoxManage clonehd --format RAW CentOS7.vdi CentOS7.img
qemu-img convert -f raw CentOS7.img -O qcow2 CentOS7.qcow2
{{< /highlight >}}

Aber Achtung! Images im RAW-Format benötigen mehr Speicherplatz. RAW-Images sind nicht komprimiert. Und sollte die virtuelle Festplatte unter VirtualBox [dynamisch](https://www.virtualbox.org/manual/UserManual.html#create-vm-wizard-virtual-hard-disk) angelegt worden sein, ist das Image so groß wie die maximale Größe der virtuellen Festplatte. Auch dann, wenn diese nur zu einem Bruchteil tatsächlich belegt ist.

Nach der erfolgreichen Umwandlung kann die neue virtuelle Maschine zum Beispiel mit [virt-manager](https://virt-manager.org) direkt genutzt werden.

Wer unter VirtualBox die Guest Additions genutzt hat, sollte diese vor der Umwandlung entfernen. Genauso sollte vorher in den Einstellungen unter VirtualBox geprüft werden, dass beim Grafik-Controller **nicht** VBoxVGA oder VBoxSVGA ausgewählt ist.