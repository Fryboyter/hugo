---
title: Neuer Betreuer für das Hugo-Theme Ananke gesucht
date: 2024-09-16T16:37:43+0200
nocomments: true
categories:
- OSBN
tags:
- Hugo
- Theme
slug: neuer-betreuer-fuer-das-hugo-theme-ananke-gesucht
---
{{< notice hinweis >}}
Zwischenzeitlich hat jemand die weitere Entwicklung übernommen.  
https://github.com/theNewDynamic/gohugo-theme-ananke/commit/9a0a974089461e25f96aa7727e90452754d4388b
{{< /notice >}}

[Ananke](https://github.com/theNewDynamic/gohugo-theme-ananke) ist eines der bekannteren Themes (veröffentlicht unter der MIT-Lizenz) für den statischen Website-Generator [Hugo](https://gohugo.io). Für dieses Theme wird nun ein neuer Betreuer gesucht. 

Wer also Lust hat das Projekt zu übernehmen, kann in dem genannten Repository ein [Issue](https://github.com/theNewDynamic/gohugo-theme-ananke/issues) erstellen um sein Interesse bekunden.

Ich habe selbst nichts mit Ananke zu tun, da ich ein eigenes Theme nutze. Es wäre aber schade, wenn dieses Theme verschwinden würde.