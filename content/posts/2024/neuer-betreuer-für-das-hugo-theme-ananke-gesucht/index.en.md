---
title: New maintainer needed for the Hugo theme Ananke
date: 2024-09-16T16:37:43+0200
nocomments: true
categories:
- Englis(c)h
tags:
- Hugo
- Theme
slug: new-maintainer-needed-for-the-hugo-theme-ananke
---
{{< notice hinweis >}}
In the meantime, someone has stepped up to continue the development.  
https://github.com/theNewDynamic/gohugo-theme-ananke/commit/9a0a974089461e25f96aa7727e90452754d4388b
{{< /notice >}}

[Ananke](https://github.com/theNewDynamic/gohugo-theme-ananke) is one of the better-known themes (published under the MIT license) for the static website generator [Hugo](https://gohugo.io). A new maintainer is now being needed for this theme. 

Anyone who would like to adopt the project can create an [Issue](https://github.com/theNewDynamic/gohugo-theme-ananke/issues) in the repository mentioned to express their interest.

I have nothing to do with Ananke myself because I use my own theme. But it would be a shame if this theme would disappear.