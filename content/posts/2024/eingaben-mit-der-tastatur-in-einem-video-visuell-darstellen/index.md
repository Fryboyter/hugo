---
title: Eingaben mit der Tastatur in einem Video visuell darstellen
date: 2024-01-05T13:06:13+01:00
nocomments: true
categories:
- OSBN
tags:
- Video
- Tastatur
- Eingabe
slug: eingaben-mit-der-tastatur-in-einem-video-visuell-darstellen
---
Heutzutage bevorzugen viele Nutzer eine Anleitung in Form eines Videos und nicht mehr in Textform. Wenn man in einem solchen Video im Terminal Emulator einen Befehl wie beispielsweise {{< mark >}}pacman -Syu{{< /mark >}} eingibt, ist das kein Problem. Was aber, wenn man beispielsweise Shortcuts ausführt? Diese werden nicht angezeigt.

Natürlich kann man in dem Video die nicht angezeigten Eingaben verbal nennen. Aber eine visuelle Darstellung ist einigen Nutzern dann doch lieber. Hierfür gibt es glücklicherweise Tools wie [Show Me The Key](https://showmethekey.alynx.one). Mit diesen wird die Eingabe, die über die Tastatur erfolgt, visuell dargestellt. Will man beispielsweise ein Video über den Editor [Helix](https://helix-editor.com) erstellen, könnte es wie in folgendem Screenshot aussehen.

{{< image src="showmethekey.webp" alt="Beispiel wie die Darstellung mit Show Me The Key aussieht." >}}

In diesem Beispiel wird mit gezeigter Eingabe die betreffende Zeile markiert und in die Zwischenablage kopiert.

Einige, die solche Videos schon länger erstellen, werden sich nun fragen, warum ich nicht das deutlich bekanntere Tool [screenkey](https://www.thregr.org/wavexx/software/screenkey/) genannt habe. Die Antwort ist ganz einfach. Screenkey unterstützt [Wayland](https://wiki.ubuntuusers.de/Wayland/) nicht. Ich habe aber inzwischen alle meine Rechner mit grafischer Oberfläche auf Wayland umgestellt.