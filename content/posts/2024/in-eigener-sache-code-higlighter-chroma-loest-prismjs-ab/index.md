---
title: In eigener Sache - Code Highlighter Chroma löst Prism.js ab
date: 2024-02-10T18:36:56+0100
nocomments: true
categories:
- OSBN 
tags:
- Code Highlighter 
- Prism
- Chroma
slug: in-eigener-sache-code-highlighter-chroma-loest-prism-js-ab
---
Wenn ich meinen [eigenen Aussagen](/fryboyter-de-nun-mit-code-highlighter/) vertrauen kann, habe ich zum Hervorheben von Code seit 2018 [Prism.js](https://github.com/PrismJS/prism) verwendet. Seit einiger Zeit habe ich allerdings öfters darüber nachgedacht, ob ich Prism.js weiterhin nutzen will. Und die Antwort lautet nein.

Ja, Prism.js funktioniert technisch gesehen. Zwar nicht perfekt, aber es gab bisher keine größeren Probleme damit. Prism.js verwendet allerdings JavaScript. Wer also diese Internetseite ohne aktiviertes JavaScript aufruft, bekommt nur den nackten Code angezeigt. Das ist zwar nicht schön aber auch nicht tragisch.

Schlimmer ist, dass das Projekt Prism.js scheinbar tot ist. Die Entwicklung von Version 1.x wurde zugunsten der Entwicklung von Version 2 mehr oder weniger auf Eis gelegt, sodass Version 1.29 die letzte veröffentlichte Version darstellt. Diese wurde im Augst 2022 veröffentlicht. Die letzten Änderungen an Version 2 wurden vor 9 Monaten durchgeführt. Seit dem gab es unter https://github.com/PrismJS/prism/discussions/3531 mehrere Nachfragen in den letzten Wochen und Monaten wie es um Prism.js steht. Eine Reaktion seitens der Entwickler gab es bisher keine, obwohl diese zwischenzeitlich durchaus auf Github aktiv waren. Daher meine Vermutung, dass das Projekt nicht weiterentwickelt wird. Über die Hintergründe kann ich nur spekulieren, also lasse ich es.

Ja, aber Prism.js funktioniert doch? Richtig. Aber es gäbe Verbesserungsbedarf. Und es wurden in den letzten Jahren schon ein paar Sicherheitslücken [entdeckt](https://github.com/PrismJS/prism/security) und behoben. Und genau diese Sicherheitslücken machen mir immer mehr Sorgen. Was, wenn nächste Woche eine weitere entdeckt wird? Wird darauf reagiert und eine neue Version veröffentlicht? Oder zumindest ein Patch? Oder wird darauf nicht reagiert?

Da mir die Situation zu ungewiss ist, habe ich mir einige Code Highlighter wie [Highlight.js](https://highlightjs.org) angesehen. Schlussendlich habe ich mich allerdings für [Chroma](https://github.com/alecthomas/chroma) entschieden. Und das hauptsächlich aus zwei Gründen.

- Ich nutze [Hugo](https://gohugo.io) zum Erzeugen dieser Internetseite. Und Chroma ist Bestandteil von Hugo. 

- Und Chroma kommt ohne JavaScript aus. Denn das Hervorheben des Codes wird direkt beim Erzeugen der Internetseite durchgeführt. Ganz so, wie wenn ich das Hervorheben des Codes manuell mit ein paar Tags und CSS gemacht hätte.

Der erste Grund spart mir somit die manuelle Aktualisierung. Der zweite Grund sorgt dafür, dass der Code auch dann hervorgehoben wird, wenn man JavaScript nicht aktiviert hat. Und die beim Aufruf von fryboyter.de übertragene Datenmenge wird auch weniger. Nicht, dass dies ins Gewicht fallen würde, aber ca. 50 Kilobyte sind für diese Seite prozentual gesehen durchaus nicht wenig.

Es gibt aber auch Nachteile. 

Ein Umbruch langer Zeilen ist mit Chroma für mich nicht zufriedenstellend möglich. Ich frage mich allerdings, ob es vielleicht nicht sowieso besser ist, darauf zu verzichten. Auch, wenn man somit in einigen Fällen horizontal scrollen muss. Ein Zeilenumbruch im Code verbessert auch nicht unbedingt dessen Lesbarkeit.

Ein weiterer Nachteil ist, dass Chroma manchen Code nicht so gut hervorhebt wie es Prism.js gemacht hat. Dafür unterstützt Chroma zum Beispiel Go Templates, welche oft in meinen Code-Beispielen zum Einsatz kommen. Na ja, nichts ist perfekt.

Alles in allem hat sich meiner Meinung nach die Umstellung und der damit verbundene Aufwand aber gelohnt.