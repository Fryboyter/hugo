---
title: Was wir aus dem Fediverse-Spam für Codeberg lernen können
date: 2024-03-01T15:57:59+0100
nocomments: true
categories:
- OSBN
tags:
- Fediverse
- Codeberg
slug: was-wir-aus-dem-fediverse-spam-für-codeberg-lernen-koennen
---
Heute will ich mal nicht mit einem eigenen Artikel langweilen, sondern nur auf einen anderen Artikel hinweisen. Dieser hat zwar direkt nichts mit Linux oder OSS zu tun und ist auch noch in englischer Sprache verfasst, aber ich hoffe um Nachsicht.

Otto, eines der Mitglieder von Codeberg, hat kürzlich einen [Artikel](https://blog.codeberg.org/what-we-can-learn-from-the-fediverse-spam-for-codeberg.html) bezüglich des [Fediverse](https://de.wikipedia.org/wiki/Fediverse) veröffentlicht, den ich recht interessant finde. Ohne ihn bewerten zu wollen.

Bei [Codeberg](https://codeberg.org) handelt es sich um eine Alternative zu Github, die auch für die Entwicklung von Projekten genutzt wird, die unter Linux verwendete werden. Also zumindest so gesehen passt das Ganze doch zu Linux und OSS. Zumal ein Großteil aller Fediverse-Nutzer Linux oder OSS im Allgemeinen nutzen werden. Vergesst das mit der Nachsicht somit einfach. ;-)