---
title: NormCap - Texterkennung für den Bildschirm
date: 2024-09-21T14:49:32+0200
nocomments: true
categories:
- OSBN
tags:
- OCR
slug: normcap-texterkennung-fuer-den-bildschirm
---
Mit der meisten Texterkennungssoftware wandel man beispielsweise eine Bilddatei mit Text in bearbeitbaren Text um. [NormCap](https://dynobo.github.io/normcap/) hingegen könnte man hingegen mit einem Screenshot-Tool vergleichen, dass Text erstellt.

Nehmen wir an, man lässt sich folgendes Bild mit Okular auf dem Bildschirm anzeigen.

{{< image src="normcap.webp" alt="Bild das mit Okular angezeigt wird" caption="Foto eines zutreffenden Schildes das mit Okular angezeigt wird" >}}

Nun will man beispielsweise das, was auf dem Schild steht, in Textform haben. Mit Normcap markiert man einfach wie bei einem Screenshot-Tool den gewünschten Bereich und der erkannte Text wird in die Zwischenablage eingetragen. 

Was aber, wenn man auch den Dateinamen in Textform haben will, den Okular oben anzeigt? Man markiert einfach mit NormCamp den betreffenden Bereich des laufenden Programms und auch in diesem Fall wird der erkannte Inhalt in Textform in der Zwischenablage gespeichert. Als Quelle dient also nicht eine bestimmte Datei, sondern das was der Bildschirm anzeigt.

Für die Texterkennung setzt NormCap auf [Tesseract](https://github.com/tesseract-ocr/tesseract), was als ziemlich zuverlässig gilt. Bei meinen bisherigen Tests musste ich daher den erkannten Text auch wenig bis gar nicht verbessern. Vermutlich dürfte aber auch Tesseract in bestimmten Fällen an seine Grenzen stoßen.