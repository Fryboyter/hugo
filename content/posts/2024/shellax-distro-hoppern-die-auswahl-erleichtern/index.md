---
title: Shellax - Distro-Hoppern die Auswahl erleichtern
date: 2024-07-26T20:38:24+0200
nocomments: true
categories:
- OSBN
tags:
- Auswahl
- Distributionen
- Golang
slug: shellax-distro-hoppern-die-auswahl-erleichtern
---
Gefühlt neigen immer mehr Nutzer dazu, regelmäßig die Distribution zu wechseln. Was in vielen Fällen vermutlich daran liegt, dass sie hoffen, dass Probleme, die sie mit Distribution X haben, unter Distribution Y nicht vorhanden sind.

Leider wechseln solche Distro-Hopper in solch einem Fall beispielsweise oft von einer auf Ubuntu basierenden Distribution mit Gnome 46.3 auf eine andere auf Ubuntu basierende Distribution mit Gnome 46.3. Was meist dazu führt, dass die Probleme immer die gleichen bleiben.

Oftmals erhoffen sich die Nutzer aber auch, dass ein Wechsel der Distribution dazu führt, dass manche Dinge pauschal besser funktionieren oder man durch die Nutzung einer Distribution anderen Nutzern andere Distributionen überlegen ist.

Aber egal welche Gründe diese Nutzer haben, sie fragen oft auf diversen Diskussions-Plattformen, welche Distribution sie als Nächstes nutzen sollen. Und ehrlich gesagt nervt das etwas.

Daher habe ich das Projekt [Shellax](https://codeberg.org/Fryboyter/Shellax) ins Leben gerufen. Mit dem Tool wird dem Nutzer zufällig eine Distribution vorgeschlagen, die er als Nächstes ausprobieren kann. Somit brauchen Distro-Hopper nicht mehr Dritte um ihre überwiegend subjektive Meinung zu fragen.

Vielleicht fragt sich nun jemand, ob Shellax ernst gemeint ist? Nein, ist es nicht. Ich bastle nur gerade dilettantisch mit [Golang](https://go.dev) herum. Aber dass manche Leute regelmäßig die verwendete Distribution wechseln, finde ich in der Tat seltsam. Diesen zeitlichen Aufwand würde ich eher darin investieren, um bestehende Probleme mit einer bestimmten Distribution zu lösen.