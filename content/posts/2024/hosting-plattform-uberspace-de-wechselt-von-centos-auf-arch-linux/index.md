---
title: Hosting-Plattform Uberspace.de wechselt von CentOS auf Arch Linux
date: 2024-06-01T10:51:57+0200
nocomments: true
categories:
- OSBN
tags:
- CentOS
- Arch Linux
- Webspace
slug: hosting-plattform-uberspace-de-wechselt-von-cent-os-auf-arch-linux
---
Seit Anfang 2018 hoste ich diverse Sachen beim Webspace-Anbieter [uberspace.de](https://uberspace.de). Aktuell wird dort CentOS 7 als Betriebssystem für die Server verwendet. Da der Support von CentOS 7 demnächst eingestellt wird, haben die Betreiber schon vor einiger Zeit mitgeteilt, dass auf eine rollende Distribution gewechselt werden soll, da dies aus ihrer Sicht für sie sinnvoller ist.

Um welche Distribution es sich handelt, war meines Wissens bisher nicht öffentlich bekannt. Somit konnte man nur Vermutungen anstellen. Persönlich hatte ich OpenSuse Tumbleweed vermutet und auf Arch Linux gehofft.

Jonas Pasche, der Chef von uberspace.de, hat kürzlich ein [Interview](https://www.websiteplanet.com/blog/uberspace-interview/) gegeben, in dem er etwas aus dem Nähkästchen geplaudert hat. Und laut diesem Interview hat man sich für [Arch](https://archlinux.org) Linux entschieden.

{{< quote src="Jonas Pasche" >}}
Our biggest development right now is the switch from an enterprise-style Linux distribution (our current product line is based on CentOS) to a rolling-release Linux distribution (specifically, ArchLinux).
{{< /quote >}}

Was ich persönlich aus zwei Gründen gut finde. Zum einen, weil ich Arch Linux seit über 10 Jahren selbst nutzte. Was aber im Grunde egal ist. Viel interessanter finde ich, dass sich jemand der seit 2010 eine Hosting-Plattform betreibt, bewusst für eine rollende Distribution mit aktuellen Paketen entscheidet.

Das werden vermutlich einige Administratoren der alten Schule schrecklich finden. Und einige werden vermutlich auch den Untergang von uberspace.de vorhersagen. Weil eine rollende Distribution mit aktuellen Paketen kann für einen Server ja generell gar nicht funktionieren.

Und ich würde sagen, diese Leute irren sich. Der Begriff stable hat beispielsweise zwei Bedeutungen. 

Zum einen, dass sich nach einem Update wenig ändert. Ja, in dem Fall ist Arch Linux unstable. So kann es vorkommen, dass es nötig ist nach einem Update die Konfigurationsdatei eines Pakets anzupassen. In meinem Fall dürfte es dieses Jahr bisher 2 Mal nötig gewesen sein. Letztes Jahr, wenn ich mich recht erinnere, hingegen war ich gar nicht betroffen. Mit Tools wie Ansible ist das zudem auch kein Beinbruch, wenn man mehrere Rechner nutzt.

Die zweite Bedeutung von stable ist die, die in der Regel genutzt wird. Nämlich, dass es nach einem Update keine Probleme gibt. Aufgrund der aktuellen Pakete können unter Arch Linux natürlich neue Bugs vorhanden sein. Bei LTS-Distributionen wie [Debian](https://www.debian.org) besteht dafür die Möglichkeit, dass [Backports](https://en.wikipedia.org/wiki/Backporting) nicht durchgeführt werden. Dieses Pech hatte ich vor ein paar Jahren mit dem Paket ddclient unter Debian. Wenn man den Anbieter afraid.org verwendet hat, wurde die IP-Nummer nicht zuverlässig aktualisiert. Den Entwicklern von ddclient war das Problem bekannt, und sie hatten schon länger eine neue Version veröffentlich, in der das Problem behoben war. Seitens Debian ist aber selbst nach Monaten kein Backport erfolgt. Was ist nun besser? Mögliche neue Bugs oder alte, die eventuell nicht behoben werden?

Ich will jetzt nicht behaupten, dass eine rollende Distribution mit aktuellen Paketen für jeden Anwendungsfall geeignet ist. Wie so oft kommt es darauf an. Es gibt halt nicht nur schwarz und weiß. Daher können Distributionen wie Arch Linux durchaus auch für Dinge abseits eines privaten Hobbys genutzt werden. So basiert beispielsweise das Betriebssystem des Steam Decks ebenfalls auf Arch Linux. Und das Teil verkauft sich meines Wissens nach ganz gut.

Unabhängig davon bin ich auf OpenSuse [Slowroll](https://en.opensuse.org/openSUSE:Slowroll) gespannt. Eine rollende Distribution, bei der normale Updates deutlich langsamer angeboten werden.