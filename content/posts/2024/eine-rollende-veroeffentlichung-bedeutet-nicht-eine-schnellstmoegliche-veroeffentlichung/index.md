---
title: Eine rollende Veröffentlichung bedeutet nicht eine schnellstmögliche Veröffentlichung
date: 2024-03-01T16:26:39+0100
nocomments: true
categories:
- OSBN
tags:
- Arch Linux
- Rolling Release
- Plasma
slug: eine-rollende-veroeffentlichung-bedeutet-nicht-eine-schnellstmoegliche-veroeffentlichung
---
Vor ein paar Tagen wurde KDE Plasma 6 veröffentlicht. Nur wenige Tage später fragen sich schon die ersten Nutzer warum beispielsweise Arch Linux diese neue Version noch nicht anbietet.

Bei einer rollenden Distribution geht es primär nicht darum, dass bestimmte Updates so schnell wie möglich in den Paketquellen angeboten werden. Es geht darum, dass Updates nach und nach über die gleichen Paketquellen angeboten werden. Es geht also nicht darum wer den ersten Platz bei einem Wettrennen hat. Mit OpenSuse [Slowroll](https://news.opensuse.org/2024/01/19/clarifying-misunderstandings-of-slowroll/) soll es daher auch eine Distribution geben, die rollt, aber bei der Updates vergleichsweise langsam angeboten werden.

Im Falle von Arch Linux wird somit in der Regel auch nie erste Hauptversion eines neuen Kernels (z. B. 6.0) über die offiziellen Paketquellen angeboten, sondern in der Regel immer erst das erste "Minor-Update" (z. B. 6.0.1). Ich vermute, das wird bei KDE Plasma 6 auch der Fall sein. Vor nächster Woche wird Plasma 6 also vermutlich nicht in den offiziellen Paketquellen von Arch Linux erscheinen. Wenn aus Sicht der Entwickler von Arch Linux weitere Gründe gegen ein Update auf Plasma 6 sprechen, wird es vermutlich sogar noch länger dauern.