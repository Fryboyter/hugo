---
title: Die letzte Version von HexChat wurde veröffentlicht
date: 2024-02-18T12:59:24+0100
nocomments: true
categories:
- OSBN
tags:
- HexChat
- IRC
- Client
slug: die-letzte-version-von-hex-chat-wurde-veroeffentlicht
---
Wer [IRC](https://de.wikipedia.org/wiki/Internet_Relay_Chat) genutzt hat oder vielleicht noch nutzt, wird den Client HexChat kennen. Und sei es nur vom Namen her. Vor ein paar Tagen wurde Version 2.16.2 veröffentlicht. Was wohl die finale uns somit letzte Version darstellt.

Der Grund ist wie so oft der übliche. Das Projekt wurde längere Zeit nicht gewartet und niemand wollte den Job übernehmen.

Da der Code laut [Ankündigung](https://hexchat.github.io/news/2.16.2.html) weiterhin auf Github vorhanden sein wird, hätte er auch nichts gegen einen Fork. Aus genannten Gründen und weil IRC heutzutage nicht mehr den Stellenwert hat den er früher hatte, wage ich es zu bezweifeln, dass jemand einen Fork erstellt. Zumindest keinen der längerfristig aktiv entwickelt wird.