---
title: Zfind - Dateien mit Filter ähnlich einer SQL-WHERE-Klausel finden
date: 2024-07-16T21:15:14+0200
nocomments: true
categories:
- OSBN
tags:
- Dateien
- Finden
- SQL
- WHERE
slug: zfind-dateien-mit-filter-aehnlich-einer-sql-where-klausel-finden
---
Wer schon einmal mit einer SQL-Datenbank gearbeitet hat, kennt bestimmt die WHERE-Klausel, mit der sich die Auswahlbedingungen eines Befehls definieren lassen.

{{< highlight sql >}}
SELECT *
FROM users 
WHERE age > 40;
{{< /highlight >}}

Mit diesem Beispiel würden alle Einträge der Tabelle users ausgegeben, bei denen ein Alter größer 40 eingetragen ist.

Der Entwickler von [zfind](https://github.com/laktak/zfind) hat nun wohl gedacht, dass es eine gute Idee ist dieses Konzept auf das Suchen nach Dateien anzuwenden.

Hier mal ein paar Beispiele.

{{< highlight bash >}}
zfind 'size<10k'
{{< /highlight >}}
{{< highlight bash >}}
zfind 'ext in ("jpg","jpeg")'
{{< /highlight >}}
{{< highlight bash >}}
zfind 'size between 1M and 1G' Beispiel/
{{< /highlight >}}

Mit dem ersten Befehl werden Dateien angezeigt die kleiner als 10KB sind. Beim zweiten werden Dateien mit der Endung .jpg und .jpeg angezeigt. Und das dritte Beispiel zeigt alle Dateien im Verzeichnis "Beispiel" an die zwischen einem Megabyte und einem Gigabyte groß sind.

Als jemand der seit vielen Jahren mit Datenbanken arbeitet, finde ich das Tool durchaus interessant. Zumal ich ja oft dazu neige alternative Programme anstelle der bekannten, abgehangenen Tools wie cp, grep oder find zu nutzen. Aber in diesem Fall wird das Programm wohl nicht Einzug in meinen persönlichen Werkzeugkasten halten. Denn es springt bei mir in dem Fall der Funke irgendwie nicht über. Somit ist zfind für mich weiterhin nur interessant.

Ach ja, das Tool bietet auch die Möglichkeit in diversen Archiven (tar, zip, 7z und rar) nach Dateien zu suchen.