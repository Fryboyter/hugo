---
title: Man muss nicht programmieren können um bei OSS / Linux mitzuhelfen
date: 2024-02-16T15:00:22+0100
nocomments: true
categories:
- OSBN
tags:
- OSS
- Linux
slug: man-muss-nicht-programmieren-koennen-um-bei-oss-linux-mitzuhelfen
---
Viele, die sich überlegen Linux zu nutzen bzw. die Linux erst seit kurzer Zeit verwenden, stellen sich die Frage wie sie helfen können. Meist führt das zu der Frage, welche Programmiersprache man kennen muss. Meine Antwort lautet, keine.

Viele Neulinge finden es beispielsweise erstrebenswert möglichst bald Änderungen am Linux-Kernel vorzunehmen. Ich sage, lasst es sein. Zumindest vorerst. Denn wenn es um den Kernel geht, arbeiten daran schon viele Leute, die euch deutlich überlegen sind. Und nein, ich bin keiner davon.

Meiner Meinung nach gibt es wichtigere Dinge. 

Zum Beispiel das Erstellen einer guten Dokumentation für Endbenutzer für ein bestimmtes Programm. Denn viele Programmierer sind zwar gut, wenn es um das Programmieren geht. Aber sie sind unfähig oder unwillig eine gute Dokumentation zu erstellen.

Was auch oft fehlt, ist die Übersetzung einer grafischen Oberfläche eines Programms in eine andere Sprache als Englisch. Wer also sprachlich begabt ist, kann sich bei unzähligen Projekten austoben.

Oder offene "Issues". Bei vielen Projekten sind oft sehr viele Issues noch offen. Viele dieser "Probleme" werden von Nutzern erzeugt, weil sie das Programm falsch benutzt. Andere hingegen, wurden zwischenzeitlich aufgrund einer anderen Meldung bereits behoben, aber nicht alle Issues geschlossen. Hier zu testen, ob ein gemeldetes Problem noch aktuell ist, kann den Entwicklern ebenfalls helfen. Oder vielleicht könnt ihr ein Problem sogar selbst beheben und den entsprechenden Code beisteuern.

Vielleicht denkt sich nun der eine oder andere Leser dieses Artikels, dass es vielleicht doch sinnvoller ist, nicht gleich am Kernel zu arbeiten. Aber er stellt sich die Frage, wo anfangen? Im Grunde ist das ganz einfach. Schaut euch an, welche Programme ihr selbst nutzt. Dann schaut euch beispielsweise die Issues an. Oder prüft, ob die grafische Oberfläche auch in einer Sprache angeboten wird, die ihre sprecht und ob es Verbesserungsmöglichkeiten gibt. Oder helft Leuten bei Ihren Problemen. Oder veröffentlicht Artikel zu bestimmten Dingen die es nicht schon unzählige Male im Internet gibt.

Ich persönlich habe ehrlich gesagt mehr Respekt vor Leuten die sich um solche "niederen Aufgaben" kümmern als vor den Leuten die gleich nach der ersten Installation einer Linux-Distribution am Kernel mitarbeiten wollen. Denn ohne die kleinen Zahnräder funktionieren auch die großen meist nicht.