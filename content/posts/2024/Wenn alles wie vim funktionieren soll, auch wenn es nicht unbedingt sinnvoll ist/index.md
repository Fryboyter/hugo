---
title: Wenn alles wie vim funktionieren soll, auch wenn es nicht unbedingt sinnvoll ist
date: 2024-02-01T12:32:10+0100
nocomments: true
categories:
- OSBN
tags:
- Editoren
- vim
- Helix
slug: wenn-alles-wie-vim-funktionieren-soll-auch-wenn-es-nicht-unbedingt-sinnvoll-ist
---
Vermutlich hat es schon fast jeder erlebt. Jemand stellt ein neues Programm vor und früher oder später (meist früher als später) wird die Frage gestellt, warum es nicht die Befehle von [vim](https://www.vim.org) unterstützt bzw. ob eine Unterstützung geplant ist.

Fragt man dann nach dem Grund für diesen Wunsch, wird meist das sogenannte Muskelgedächtnis angegeben. Bei manchen Nutzern haben sich die Befehle von vim scheinbar so tief in dieses eingebrannt, dass ohne diese eine Nutzung von Programmen schwer bis unmöglich ist. Zumindest habe ich dieses Gefühl, wenn ich die eine oder andere Diskussion verfolge.

Im Grunde habe ich damit kein Problem. Wenn ein Programm optional die Befehle von vim unterstützt oder es eine entsprechende Erweiterung gibt, soll es mir recht sein. Ich nutze dann einfach beides nicht und das Thema ist für mich erledigt.

Nehmen wir nun den Editor [Helix](https://helix-editor.com) als Beispiel, über den ich bereits vor ein paar Monaten einen [Artikel](/helix-editor/) veröffentlicht habe. Bei Helix handelt es sich ebenfalls um einen modalen Editor. Allerdings selektiert man bei diesem erst, was man ändern will und führt dann die Aktion aus. Bei vim ist es genau umgekehrt. Es dürfte somit gar nicht machbar sein, die Bedienung von vim auf Helix komplett zu übertragen. Projekten wie https://github.com/LGUG2Z/helix-vim gelingt das daher auch nur teilweise.

Und trotzdem gibt es regelmäßig Anfragen ob Helix nicht die Shortcuts von vim unterstützen kann. Aber warum? 

Ich habe schon immer vermieden vim zu nutzen, da mir dessen Bedienung nicht zusagt. Trotzdem ist es mir bisher noch nie in den Sinn gekommen, dass ich einen Feature Request für vim Stelle, damit dieser Editor beispielsweise die Shortcuts von [micro](https://github.com/zyedidia/micro) unterstützt. Dieser würde vermutlich schneller geschlossen als ich bis 3 zählen kann. Und das zu Recht. Genauso wie ich es akzeptiert habe, dass ich Helix nicht so bedienen kann, wie ich es von anderen Editoren in den letzten Jahren gewohnt bin. Und trotzdem kann ich mit Helix inzwischen ganz gut umgehen. Und das trotz des Muskelgedächtnisses und der Tatsache, dass ich schon einige Jahrzehnte auf dem Zähler habe.

Von daher liebe Nutzer von vim (also der Teil auf den ich mich mit diesem Artikel beziehe), schaut euch gerne Alternativen zu vim an. Aber bitte versucht zu verstehen, dass nicht jede Alternative das Ziel hat eine bessere / andere Version von vim anzubieten. Viele Projekte, wie Helix, wollen einfach nur Ihr eigenes Ding machen. Wenn euch das nicht zusagt, was ich durchaus nachvollziehen kann, nutzt einfach weiterhin vim. Vim ist, objektiv betrachtet, ja kein schlechter Editor.