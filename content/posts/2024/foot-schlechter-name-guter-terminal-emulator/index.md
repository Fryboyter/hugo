---
title: Foot - Schlechter Name, guter Terminal Emulator
date: 2024-04-07T18:39:43+0200
nocomments: true
categories:
- OSBN
tags:
- Terminal Emulator
slug: foot-schlechter-name-guter-terminal-emulator
---
Wer noch X11 nutzt, braucht nicht unbedingt weiterlesen, da es bei diesem Artikel um ein Tool geht, dass nur unter Wayland nutzbar ist. 

Vor ein paar Monaten bin ich wegen [OSC52](https://old.reddit.com/r/vim/comments/k1ydpn/a_guide_on_how_to_copy_text_from_anywhere/) von [Terminator](https://github.com/gnome-terminator/terminator) auf [Alacritty](https://alacritty.org) umgestiegen. Danach wurde das Update auf KDE Plasma 6 anboten, welches bei mir dazu geführt hat, meine Rechner mit grafischer Oberfläche komplett auf Wayland umzustellen. 

Aus einer Laune heraus habe ich dann auch den Terminal Emulator [foot](https://codeberg.org/dnkl/foot) installiert. Dieser ist, wie bereits eingangs angemerkt, nur unter Wayland nutzbar. Laut dem Entwickler, der ausgehend von dem Repository, scheinbar eine ziemlich nette Person ist, ist foot schnell, leichtgewichtig und bietet aber trotzdem einige Funktionen. Wie eben OSC52 (ja mir sind die möglichen Risiken bekannt). 

Also leichtgewichtig ist foot auf jeden Fall. Der Terminal Emulator an sich benötigt gerade mal 670.3 KB an Speicherplatz. Alacritty benötigt mindestens 8.5 MB. Gut, in der heutigen Zeit ist das im Grunde recht egal. Aber da ich weiterhin [Zellij](https://zellij.dev) und dessen Funktionen nutze, kann ein Terminal Emulator meinetwegen gerne wenig Speicherplatz und Funktionen bieten. Von OSC52 abgesehen.

Ich nutze foot nun schon einige Zeit und kann nichts Negatives berichten. Ist foot nun besser als beispielsweise Alacritty? Nein. Dieses "X ist besser als Y" nervt mich sowieso. Jeder sollte die Werkzeuge nutzen, die ihm am meisten zusagen. Aber meiner Meinung nach sollte man auch ab und zu mal über den eigenen Tellerrand schauen und andere Tools testen. Wie eben foot. Oder [Helix](https://helix-editor.com).

Vor ein paar Tagen wurde übrigens die Version [1.17.0](https://codeberg.org/dnkl/foot/releases/tag/1.17.0) von foot veröffentlicht.