---
title: Vorerst keine Veröffentlichungen mehr bei Mastodon
date: 2024-02-10T19:34:59+0100
categories:
- OSBN
tags:
- Mastodon
- IFTTT
slug: vorerst-keine-veroeffentlichungen-mehr-bei-mastodon
---
Seit einiger Zeit wird unter https://social.tchncs.de/@fryboyter ein Hinweis angezeigt, wenn ich einen neuen Artikel veröffentlicht habe. Damit ist vorerst Schluss.

Für die automatische Veröffentlichung hatte ich bisher den Dienst [IFTTT](https://ifttt.com/) verwendet. Vorhin wollte ich etwas an dem betreffenden Applets ändern und habe dies hierbei deaktiviert. Was ich wohl lieber nicht gemacht hätte. Wenn ich das Applet nun aktivieren will, erhalte ich nun den Hinweis "This Applet has Pro features". Scheinbar sind Webhooks bei IFTTT zwischenzeitlich kostenpflichtig geworden. Und sobald man ein ehemalig kostenloses Applet deaktiviert, hat man die Arschkarte gezogen und soll nun zahlen, wenn man es wieder aktivieren will. 2,92 Dollar im Monat, wenn man jährlich zahlt oder 3,49 Dollar, wenn man monatlich bezahlt.

Wenn man bedenkt, dass ich bei Mastodon im Grunde nicht aktiv bin und dort nur entsprechende Hinweise auf neue Artikel veröffentlicht habe, will ich weder 2,92 noch 3,29 Dollar bezahlen. Zumal ich auch beispielsweise RSS-Feeds wie https://fryboyter.de/index.xml anbieten über die man sich über neue Artikel in Kenntnis setzen lassen kann und ich IFTTT für nichts anderes nutze.

Ich hatte schon vor einiger Zeit mit Alternativen wie [Node Red](https://nodered.org) oder [N8N](https://n8n.io) experimentiert. Leider bin ich entweder komplett daran gescheitert, dass es funktioniert hat oder es wurden alle Beiträge der angegebenen Feed-Adresse bei Mastodon veröffentlicht und nicht nur der neuste Beitrag. Falls jemand hierfür eine Lösung hätte und bereit ist mir sinnvoll zu helfen, wäre es sehr nett. Ich aktiviere daher bei diesem Beitrag die Kommentarfunktion. Alternativ kann man mir auch eine E-Mail schreiben. Die Adresse ist im Impressum genannt.