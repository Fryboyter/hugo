---
title: Mit Marp und Markdown Präsentationen erstellen
date: 2024-12-26T17:40:20+0100
nocomments: true
categories:
- OSBN
tags:
- Marp
- Präsentationen
- Markdown
slug: mit-marp-und-markdown-praesentationen-erstellen
---
Ich gehöre zu den Menschen, die sehr selten eine Präsentation halten müssen. Worüber ich nicht traurig bin. Aber ab und zu erwischt es mich doch. So werde ich Anfang 2025 vermutlich mal wieder vor Publikum auftreten müssen. Bisher habe ich hierfür [Hovercraft](https://github.com/regebro/hovercraft) verwendet.

Hovercraft funktioniert zwar technisch gesehen weiterhin aber die Entwicklung wurde, Stand heute, eingestellt. Daher und weil Hovercraft [reStructuredText](https://de.wikipedia.org/wiki/ReStructuredText) für die Erstellung der Präsentation verwendet, habe ich mir mal Alternativen angesehen.

Wie so oft gibt es unterschiedliche Lösungen, die unterschiedliche Nutzer bevorzugen. Ich habe mich in diesem Fall für [Marp](https://marp.app) entschieden. Vor allem, weil man die Präsentationen mit Markdown erstellt. Aber auch, weil Marp durchaus einige nützliche Funktionen bietet.

So könnte eine mit Marp erstellte Präsentation im Rohzustand wie folgt aussehen.

{{< highlight md >}}
---
paginate: true
---

# Mein Präsentation

---

## Seite 1

- Punkt 1
- Punkt 2
- Punkt 3

---

## Seite 2

![Image](https://picsum.photos/800/600)

---

## Seite 3

> Das ist ein Zitat.

---

## Seite 4

| Früchte | Anzahl |
| -------- | -------- |
| Äpfel   | 10   |
| Birnen   | 20   |
| Kirschen   | 45   |
{{< /highlight >}}

Davon ausgehend, dass wir diesen Inhalt als präs1.md gespeichert haben, kann man mit dem Befehl {{< mark >}}marp präs1.md{{< /mark >}} einen Export in Form einer HTML-Datei erstellen, sodass man für die Anzeige nur einen Browser benötigt. Wer ein anderes Dateiformat als Endergebnis bevorzugt, kann beispielsweise {{< mark >}}marp --pdf präs1.md{{< /mark >}} oder {{< mark >}}marp --pptx präs1.md{{< /mark >}} nutzen. Man kann aber auch beispielsweise mittels {{< mark >}}marp --images png präs1.md{{< /mark >}} die Präsentation in Form von einzelnen Bildern exportieren. Und so weiter. Marp ist hier ziemlich flexibel, sodass jeder Nutzer ein Format findet, dass ihm zusagt.

Auch lassen sich unter anderem Notizen für den Redner, die man in die einzelnen Seiten hinterlegt hat, exportieren ([https://marpit.marp.app/usage?id=presenter-notes](https://marpit.marp.app/usage?id=presenter-notes)).

Wer seine Präsentation etwas aufhübschen will, kann auch diverse [Effekte](https://github.com/marp-team/marp-cli/blob/main/docs/bespoke-transitions/README.md#built-in-transitions) nutzen, die man auch noch manuell anpassen kann.

Und so weiter.

Da ich Markdown schon seit Jahren nutze, konnte ich daher mit Marp innerhalb weniger Minuten eine Präsentation erstellen. Marp mag nicht das objektiv beste Tool seiner Art sein. Aber ich denke es sollte durchaus erwähnt werden.

Wer VS Code nutzt, findet unter https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode eine Erweiterung für Marp.