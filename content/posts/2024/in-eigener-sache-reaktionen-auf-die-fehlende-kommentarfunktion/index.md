---
title: In eigener Sache - Reaktionen auf die fehlende Kommentarfunktion
date: 2024-02-02T16:19:44+0100
nocomments: true
categories:
- OSBN
tags:
- Kommentarfunktion
slug: in-eigener-sache-reaktionen-auf-die-fehlende-kommentarfunktion
---
Mitte Dezember 2023 hatte ich [bekannt gegeben](/in-eigener-sache-kommentarfunktion-bei-neuen-artikeln-deaktiviert-und-die-lizenz-aendert-sich/), dass ich die Kommentarfunktion deaktiviere. Seit dem habe ich einige, wenige Rückmeldungen über andere Kommunikationsmöglichkeiten erhalten. Inhaltlich haben sie mir nicht unbedingt zugestimmt, aber sie waren bisher alle neutral bis freundlich formuliert. Es geht also, wenn man will.

In einigen Fällen wurde ich gefragt, wie man mit mir in Kontakt treten kann, da die Kommentarfunktion nun deaktiviert ist. Na ja im Grunde eben wie diese Leute es gemacht haben oder könnten. Zum Beispiel per E-Mail, per Matrix-Netzwerk, per Telefon oder indem man mich persönlich besucht. So gut wie alle Informationen sind auf fryboyter.de genannt bzw. man bekommt sie leicht mit einer Suchmaschine seiner Wahl heraus. Aber darum geht es mir nicht.

Nehmen wir mal meinen [letzten Artikel](/wenn-alles-wie-vim-funktionieren-soll-auch-wenn-es-nicht-unbedingt-sinnvoll-ist/) als Beispiel, der wohl die meisten Reaktionen in der letzten Zeit bewirkt hat. Wer mir deswegen beispielsweise eine E-Mail geschrieben hat, hat allerdings das gleiche Problem wie mit der Kommentarfunktion. Entweder reagiere ich darauf, wann ich will (oder kann) oder ich reagiere gar nicht darauf.

Aber warum muss man es von mir abhängig machen? Wenn ihr beispielsweise etwas zu einem meiner Artikel zu sagen habt, dann erstellt doch einfach einen eigenen Artikel auf eurer eigenen Internetseite. Früher gab es mal etwas, dass als "Stöckchen zuwerfen" bezeichnet wurde. Also jemand hat einen Artikel geschrieben, und jemand anderes hat mit einem Artikel darauf reagiert. Das wurde entweder vorher abgesprochen, wer reagiert oder es war zufällig. Warum diese "Tradition" nicht weiterführen?

Einige werden jetzt vermutlich argumentieren, dass ich auf diese Reaktionen spät oder gar nicht reagieren werde. Korrekt. Aber andere Leute werden vielleicht zeitnah reagieren. Seit es nun in Form von Kommentaren oder indem sie selbst Artikel veröffentlichen. Das finde ich nicht schlecht. Es geht ja nicht nur um mich. Und umso mehr Meinungen umso besser.