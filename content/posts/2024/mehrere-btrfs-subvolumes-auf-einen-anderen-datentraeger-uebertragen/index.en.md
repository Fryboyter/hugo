---
title: Transfer multiple Btrfs subvolumes to another data medium
date: 2024-05-09T18:25:05+0200
nocomments: true
categories:
- Englis(c)h
tags:
- Btrfs
- Subvolumes
slug: transfer-multiple-btrfs-subvolumes-to-another-data-medium
---
I have bought a larger NVMe for one of my computers. On this computer I use Btrfs as the file system and therefore also subvolumes instead of several normal partitions to split the hard disk into areas. This looks like this, for example.

{{< highlight bash >}}
btrfs subvol list /
ID 256 gen 248361 top level 5 path @
ID 257 gen 248361 top level 5 path @home
ID 258 gen 248204 top level 5 path @pkg
ID 259 gen 248361 top level 5 path @log
ID 260 gen 11 top level 5 path @.snapshots
ID 261 gen 20 top level 256 path var/lib/portables
ID 262 gen 21 top level 256 path var/lib/machines
ID 263 gen 88479 top level 256 path var/lib/aurbuild/x86_64/root
{{< /highlight >}}

In this case, only the first 5 subvolumes that are present on a single partition with Btrfs are important.

@ -> The distribution itself.\
@home -> The home directories\
@pkg -> The cache of the packages\
@log -> The log files under /var/log\
@.snapshots -> The snapshots of Btrfs

So basically nothing complicated because all the subvolumes are on one level. I would now like to transfer these subvolumes, including the data stored in them, to the new, larger NVMe. The process should not be a big problem with {{< mark >}}btrfs send{{< /mark >}} ([https://btrfs.readthedocs.io/en/latest/btrfs-send.html](https://btrfs.readthedocs.io/en/latest/btrfs-send.html)) and {{< mark >}}btrfs receive{{< /mark >}} ([https://btrfs.readthedocs.io/en/latest/btrfs-receive.html](https://btrfs.readthedocs.io/en/latest/btrfs-receive.html)). 

Today, however, it was too inconvenient for me because I would first have to create a snapshot without write permissions. I would then have to transfer this with btrfs send and receive to the new hard disk. There it would then be necessary to create a new snapshot to restore write access. Finally, I would then have to delete the snapshot without the write permissions. And that for each subvolume. At first glance, this sounds like a lot of work. But it is not. It can be done with just a few commands per subvolume. But I wanted to see if there is a more convenient solution.

And yes, it does exist. Someone else was probably faced with the same problem and developed the [btrfs-clone](https://github.com/mwilck/btrfs-clone) tool as a solution, which is based on Python. Btrfs-clone also uses btrfs send / receive internally. But in the best case you don't have to take care of anything.

Because I have backups and therefore nothing to lose, I simply gave the tool a try. First, I created a partition on the new NVMe with btrfs as the file system and then mounted it in the nvmenew directory. I then executed the command {{< mark >}}btrfs-clone / nvmenew{{< /mark >}}. Then it took some time until the process was completed. And lo and behold, all subvolumes with the data were present on the new hard disk.

Finally, I adjusted the UUID of the subvolumes in the /etc/fstab and /boot/loader/entries/arch.conf files (btrfs-clone does not adopt the old ones so that both disks can be used in one computer). And then I was able to boot from the new hard disk. Not quite. Earlier I had also created a small EFI partition on the new hard disk and copied the contents of the old EFI partition into it and reinstalled the boot loader using {{< mark >}}bootctl install{{< /mark >}}. All in all, it took less time than if I had reinstalled the distribution and restored the personal data from a backup.

I bet some people have already wondered why I didn't use dd. Or Clonezilla. Or whatever. For several reasons.

The most important reason, however, is probably that I simply wanted to work directly with the subvolumes. 