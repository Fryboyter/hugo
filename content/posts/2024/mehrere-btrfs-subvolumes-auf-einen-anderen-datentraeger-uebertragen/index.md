---
title: Mehrere Btrfs-Subvolumes auf einen anderen Datenträger übertragen
date: 2024-05-09T18:25:05+0200
nocomments: true
categories:
- OSBN
tags:
- Btrfs
- Subvolumes
slug: mehrere-btrfs-subvolumes-auf-einen-anderen-datentraeger-uebertragen
---
Ich habe einen meiner Rechner eine größere NVMe spendiert. Auf diesem Rechner nutze ich Btrfs als Dateisystem und somit auch Subvolumes anstelle von mehreren normalen Partitionen um die Festplatte in Bereiche zu unterteilen. Das sieht beispielsweise wie folgt aus.

{{< highlight bash >}}
btrfs subvol list /
ID 256 gen 248361 top level 5 path @
ID 257 gen 248361 top level 5 path @home
ID 258 gen 248204 top level 5 path @pkg
ID 259 gen 248361 top level 5 path @log
ID 260 gen 11 top level 5 path @.snapshots
ID 261 gen 20 top level 256 path var/lib/portables
ID 262 gen 21 top level 256 path var/lib/machines
ID 263 gen 88479 top level 256 path var/lib/aurbuild/x86_64/root
{{< /highlight >}}

Wichtig sind in diesem Fall nur die ersten 5 Subvolumes welche auf einer einzelnen Partition mit Btrfs vorhanden sind.

@ -> Die Distribution an sich.\
@home -> Die Home-Verzeichnisse\
@pkg -> Der Cache der Pakete \
@log -> Die Log-Dateien unter /var/log\
@.snapshots -> Die Snapshots von Btrfs

Also im Grunde genommen nichts Kompliziertes, da alle Subvolumes auf einer Ebene vorhanden sind. Diese Subvolumes inklusive der darin gespeicherten Daten möchte ich nun auf die neue, größere NVMe übertragen. Der Vorgang sollte mit {{< mark >}}btrfs send{{< /mark >}} ([https://btrfs.readthedocs.io/en/latest/btrfs-send.html](https://btrfs.readthedocs.io/en/latest/btrfs-send.html)) und {{< mark >}}btrfs receive{{< /mark >}} ([https://btrfs.readthedocs.io/en/latest/btrfs-receive.html](https://btrfs.readthedocs.io/en/latest/btrfs-receive.html)) kein großes Problem sein. 

Heute war es mir aber zu umständlich, da ich erst einen Snapshot ohne Schreibberechtigung erstellen müsste. Diese müsste ich dann mit btrfs send und receive auf die neue Festplatte übertragen. Dort wäre es dann noch nötig einen neuen Snapshot zu erstellen, um den Schreibzugriff wiederherzustellen. Abschließend müsste ich dann den Snapshot ohne Schreibberechtigung wieder löschen. Und das für jedes Subvolume. Im ersten Moment hört sich das nach ziemlich viel Aufwand an. Ist es aber nicht. Das lässt sich mit nur wenigen Befehlen pro Subvolume erledigen. Ich wollte aber mal schauen, ob es nicht eine bequemere Lösung gibt.

Und ja, gibt es. Jemand anderes stand wohl vor dem gleichen Problem und hat als Lösung das Tool [btrfs-clone](https://github.com/mwilck/btrfs-clone) entwickelt, welches auf Python basiert. Btrfs-clone nutzt hierbei intern auch btrfs send / receive. Nur muss man sich halt im besten Fall um nichts kümmern.

Da ich Backups besitze und somit nichts zu verlieren habe, habe ich das Tool einfach mal probiert. Als Erstes habe ich auf der neuen NVMe eine Partition mit btrfs als Dateisystem erstellt und diese dann in das Verzeichnis nvmeneu gemountet. Anschließend habe ich den Befehl {{< mark >}}btrfs-clone / nvmeneu{{< /mark >}} ausgeführt. Dann hat es einige Zeit gedauert bis der Vorgang abgeschlossen war. Und siehe da, alle Subvolumes mit den Daten waren auf der neuen Festplatte vorhanden.

Abschließend habe ich in der Datei /etc/fstab und /boot/loader/entries/arch.conf noch die UUID der Subvolumes angepasst (btrfs-clone übernimmt nicht die alten, damit man beide Datenträger in einem Rechner nutzen kann). Und schon konnte ich von der neuen Festplatte booten. Nein nicht ganz. Ich hatte vorhin auch noch eine kleine EFI-Partition auf der neuen Festplatte erstellt und in diese den Inhalt der alten EFI-Partition kopiert und mittels {{< mark >}}bootctl install{{< /mark >}} den Bootloader neu installiert. Alles in allem hat es aber weniger Zeit benötigt als, wenn ich die Distribution neu installiert und die persönlichen Daten aus einem Backup wiederhergestellt hätte.

Ich wette es haben sich schon einige gefragt, warum ich nicht dd genutzt habe. Oder Clonezilla. Oder was auch immer. Aus mehreren Gründen.

Der wichtigste Grund dürfte aber sein, dass ich einfach mit den Subvolumes direkt arbeiten wollte. 