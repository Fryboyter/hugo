---
title: Chezmoi - Standardverzeichnis ändern
date: 2024-10-02T23:03:31+02:00
nocomments: true
categories:
- OSBN
tags:
- Chezmoi
slug: chezmoi-standardverzeichnis-aendern
---
Um Konfigurationsdateien auf mehreren Rechnern zu verwalten, kann man das Tool [chezmoi](https://www.chezmoi.io) nutzen. 

Das Tool erstellt hierfür unter ~/.local/share/chezmoi ein lokales Git-Repository. Allerdings habe ich alle meine anderen Repositories in einem anderen Verzeichnis gespeichert. Daher habe ich mich gefragt, ob man das Standardverzeichnis von chezmoi ändern kann. Ja, kann man.

Hierfür legt man, falls nicht bereits vorhanden, die Datei ~/.config/chezmoi/chezmoi.toml an und trägt in dieser beispielsweise folgendes ein.

{{< highlight bash >}}
sourceDir = "~/repository/dotfiles"
{{</ highlight >}}

Führt man nun {{< mark >}}chezmoi init{{< /mark >}} aus, wird das Repository nicht mehr in ~/.local/share/chezmoi, sondern im angegebenen Verzeichnis erstellt.