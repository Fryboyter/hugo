---
title: Amd-ucode ist unter Arch Linux und vielleicht anderen Distributionen meist nutzlos
date: 2024-11-02T11:54:55+0100
nocomments: true
categories:
- OSBN
tags:
- Amd-ucode
- Arch Linux
slug: amd-ucode-ist-unter-arch-linux-und-vielleicht-anderen-distributionen-meist-nutzlos
---
In Wiki von Arch Linux wird empfohlen für Updates des Microcodes entweder amd-ucode oder intel-ucode zu installieren ([https://wiki.archlinux.org/title/Microcode](https://wiki.archlinux.org/title/Microcode)). Wenn man eine CPU von AMD nutzt, ist das aber meist nur ein Placebo.

Unter https://gitlab.com/kernel-firmware/linux-firmware/-/blob/main/amd-ucode/README sind die unterstützten Prozessoren genannt. Was überschaubar wenige sind. Und viele davon dürfte man auf einem durchschnittlichen Desktop-System nicht nutzen.

Um zu prüfen, ob man einen unterstützen Prozessor nutzt, kann man folgenden Befehl ausführen und diesen dann mit der README-Datei vergleichen.

{{< highlight bash >}}
lscpu | awk -F: '$1 ~ /^(Prozessorfamilie|Modell|Stepping)$/ {printf("%s: 0x%.2x\n", $1, $2)}'
{{</ highlight >}}

Bei dem Rechner mit dem ich gerade diesen Artikel schreibe, erhalte ich folgende Ausgabe.

{{< highlight bash >}}
Prozessorfamilie: 0x17
Modell: 0x08
Stepping: 0x02
{{</ highlight >}}

Es handelt sich hierbei um einen AMD Ryzen 5 2600X. Also um eine nicht ganz aktuellen aber auch nicht um einen Prozessor aus dem letzten Jahrhundert. Und sie wird nicht unterstützt. Was auch in vielen anderen Fällen zutreffen sollte.

Jetzt kann man, nein sollte man sich fragen, warum AMD (im Gegensatz zu Intel) so wenige CPU mit Microcode-Updates direkt versorgt. Meines Wissens nach liegt es daran, dass AMD davon ausgeht, dass die Hersteller von Motherboards solche Updates zusammen mit BIOS- / UEFI-Updates anbieten. Somit veröffentlicht AMD diese Update nicht direkt in Form von Paketen wie amd-ucode. Was "lustig" ist, da manche Hersteller allgemein höchsten 1 oder 2 BIOS- / UEFI-Updates veröffentlichen. Wenn überhaupt.

Was kann man dagegen machen? Unter Arch würde ich empfehlen amd-ucode zu deinstallieren und dafür https://aur.archlinux.org/packages/amd-zen-ucode-platomav zu installieren. Hierbei handelt es sich um ein, von AMD unabhängiges, Projekt dass die jeweils aktuellen Microcode-Updates diverser Prozessoren in Form eines Pakets anbietet.

Betrifft das nun auch andere Distributionen? Ich bin mir nicht sicher. Entweder machen sich andere Distributionen die Arbeit und bieten von Haus aus ein Paket wie amd-zen-ucode-platomav an. Oder sie bieten ebenfalls keine aktuellen Microcode-Upodates für AMD an.

Vermutlich fragen sich jetzt einige, ob Microcode-Updates überhaupt wichtig sind. Ja, nein, vielleicht. Manche dieser Updates beheben nur Bugs von denen man in seltenen Fällen betroffen ist. Andere Update hingegen beheben Sicherheitslücken. Also im Zweifelsfall sind sie wichtig.