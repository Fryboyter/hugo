---
title: Erstellung der Fallback Initramfs-Archive unter Arch verhindern
date: 2024-11-09T15:48:23+01:00
nocomments: true
categories:
- OSBN
tags:
- Initramfs
- Arch
- Mkinitcpio
slug: erstellung-der-fallback-initramfs-archive-unter-arch-verhindern
---
Unter Arch Linux werden pro Kernel jeweils zwei [Initramfs](https://de.wikipedia.org/wiki/Initramfs)-Archive erstellt. Bei der normalen Version wird das berücksichtigt was in der Datei /etc/mkinitcpio.conf eingetragen ist. In der Fallback-Version ist hingegen alles vorhanden, was möglich ist. Dies hat eine deutliche Auswirkung auf die Dateigröße.

{{< highlight bash >}}
Size User Date Modified Name
410k root 26 Okt 08:04  amd-ucode.img
   - root 26 Sep  2022  EFI
159M root  5 Nov 18:27  initramfs-linux-zen-fallback.img
 50M root  5 Nov 18:26  initramfs-linux-zen.img
   - root  6 Nov 18:15  loader
 14M root  2 Nov 19:33  vmlinuz-linux-zen
{{< /highlight >}}

Wie man sieht, ist in meinem Fall die Fallback-Version deutlich größer als die normale Version.

Auch wenn 159 MB heutzutage meist nicht ins Gewicht fallen, habe ich mir überlegt, ob man die Fallback-Version überhaupt benötigt. Für mich selbst lautet die Antwort nein.

Die Fallback-Version dient sozusagen als Notfall-Lösung, wenn man bei der Konfiguration des Initramfs-Archis der normalen Version Mist gebaut hat. In den letzten Jahren dürfte ich die Datei /etc/mkinitcpio.conf aber vermutlich keine 5 Mal geändert haben. Zudem habe ich für die Fallback-Version keinen Eintrag im Bootloader erstellt. Bei Problemen mit Initramfs würde ich daher einfach von der Iso-Datei von Arch booten, mich mit arch-chroot in die Installation einklinken und dann das Problem beheben.

Also hinfort mit der Fallback-Version. Hierzu muss man unter /etc/mkinitcpio.d/ die Preset-Dateien anpassen bei denen man das Erzeugen der Fallback-Version deaktivieren will. Vor der Änderung sieht eine solche Datei zum Beispiel wie folgt aus.

{{< highlight bash >}}
# mkinitcpio preset file for the 'linux-zen' package

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-linux-zen"

PRESETS=('default' 'fallback')

#default_config="/etc/mkinitcpio.conf"
default_image="/boot/initramfs-linux-zen.img"
#default_options=""

#fallback_config="/etc/mkinitcpio.conf"
fallback_image="/boot/initramfs-linux-zen-fallback.img"
fallback_options="-S autodetect"
{{< /highlight >}}

Alles, was man im Grunde machen muss, ist in der Zeile PRESETS 'fallback' zu entfernen und die Datei speichern.

Wer gleich testen will, ob es klappt, kann den Befehl {{< mark >}}mkinitcpio -P{{< /mark >}} ausführen. Hiermit werden alle Initramfs-Archive aller installierten Kernel neu erzeugt. Oder man wartet einfach auf das nächste Kernel-Update.

Anschließend löscht man die eventuell vorhandenen, nicht mehr gewünschte Fallback-Archive der betreffenden Kernel unter /boot/.

Wer im Bootloader entsprechende Einträge hat, sollte diese noch löschen. Herauf gehe ich an der Stelle aber nicht ein, da es nicht nur einen Bootloader gibt.

Wie bereits weiter oben geschrieben ist die Fallback-Version für den Notfall gedacht. Wer diese daher entfernt sollte eine nutzbare Iso-Datei von Arch (z. B. auf einem USB-Stick) griffbereit haben.