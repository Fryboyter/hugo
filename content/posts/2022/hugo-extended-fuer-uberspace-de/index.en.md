---
title: Hugo Extended for Uberspace.de / CentOS 7
date: 2022-04-23T17:09:41+0200
lastmod: 2025-02-26T18:56:09+0100
categories:
- Englis(c)h
tags:
- Hugo
- Uberspace
- Extended
slug: hugo-extended-for-uberspace-de
version: 0.145.0
sha256sum: e3dd93593179bd1a9087fcdfeb8337140c9397cae051b24e09599bcfb3f2425f
---
To create fryboyter.de I use [Hugo](https://gohugo.io). And for webspace I use [uberspace.de](https://uberspace.de). For example, when I write a new article, I create a commit for it, which I upload to a [Mercurial](https://www.mercurial-scm.org) repository on the webspace. Each new commit then triggers the execution of a script that rebuilds the page.

The script uses the compiled version of Hugo provided by the developers which is stored on the webspace. Due to recent changes to the website I now need the so called extended version of Hugo. But unfortunately it doesn't work at Uberspace.de, because they currently use CentOS 7, so some needed packages are too stable. Or in other words too outdated. Hugo therefore aborts with the following error message.

{{< highlight bash >}}
/home/laythos/bin/hugo: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.20' not found (required by /home/laythos/bin/hugo)
/home/laythos/bin/hugo: /lib64/libstdc++.so.6: version `CXXABI_1.3.8' not found (required by /home/laythos/bin/hugo)
/home/laythos/bin/hugo: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.21' not found (required by /home/laythos/bin/hugo)
{{< /highlight >}}

The problem is known to the developers ([https://github.com/gohugoio/hugo/issues/9330](https://github.com/gohugoio/hugo/issues/9330)), but for understandable reasons no ready compiled versions are offered for old distributions.

So in this case there are two possibilities. One creates the web page locally each time and loads it for example with rsync on the web space. Or you compile a version of Hugo that supports CentOS 7. I decided for the latter.

Compiling directly on an Uberspace should not be tried though. Because there you can use a maximum of 1.5 GB RAM. Anything that needs more will be terminated automatically. Which is the case with Hugo.

So I downloaded a virtual environment of CentOS 7 from https://www.osboxes.org and started it with VirtualBox. Then I installed golang as well as gcc-c++. The former via [detours](https://computingforgeeks.com/install-go-golang-on-centos-rhel-linux/), since Go is apparently not available in the official package sources.

After that I proceeded as follows.

{{< highlight bash >}}
wget https://github.com/gohugoio/hugo/archive/refs/tags/v0.97.3.zip
unzip v0.97.3.zip
cd hugo-0.97.3
CGO_ENABLED=1 go install --tags extended
{{< /highlight >}}

The first command downloads the source code of the current version. The second command unpacks it. The third command changes to the directory where the files were unpacked. And the last command compiles the extended version of Hugo.

When the last command has been successfully executed, which can take a few minutes depending on your hardware, you should find the file {{< mark >}}hugo{{< /mark >}} in the directory ~/go/bin/. You simply copy this file to your Uberspace. With this the creation of the website should work without error messages.

At https://e1.pcloud.link/publink/show?code=kZSMfzZx0nn3yYPAEVOyuhqvPPdgLfvF5ek I uploaded the current version of Hugo that works at Uberspace.de. If you want (and trust me), you can use it. 

{{< notice info "Hugo for Uberspcae / CentOS 7" >}}
Last updated on {{< lastmod >}}.  
Version: {{< param version >}}  
Sha256sum: {{< param sha256sum >}}
{{< /notice >}}