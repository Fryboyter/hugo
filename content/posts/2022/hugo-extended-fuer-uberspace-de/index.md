---
title: Hugo Extended für Uberspace.de / CentOS 7
date: 2022-04-23T17:09:41+0200
lastmod: 2025-02-26T18:56:09+0100
categories:
- Allgemein
- OSBN
tags:
- Hugo
- Uberspace
- CentOS
slug: hugo-extended-für-uberspace-de
version: 0.145.0
sha256sum: e3dd93593179bd1a9087fcdfeb8337140c9397cae051b24e09599bcfb3f2425f
---
Zum Erstellen von fryboyter.de nutze ich [Hugo](https://gohugo.io). Und als Webspace nutze ich [uberspace.de](https://uberspace.de). Wenn ich beispielsweise einen neuen Artikel schreibe, erstelle ich für diesen einen Commit, den ich in ein [Mercurial](https://www.mercurial-scm.org)-Repository auf den Webspace hochlade. Jeder neue Commit löst dann die Ausführung eines Scripts aus, welches die Seite neu erzeugt.

Das Script nutzt hierfür die fertig kompilierte Version von Hugo, die von den Entwicklern angeboten wird und die auf dem Webspace abgespeichert ist. Aufgrund von kürzlich erfolgten Änderungen an der Internetseite benötige ich nun die sogenannte extended Version von Hugo. Die aber leider bei Uberspace.de nicht funktioniert, da dort aktuell CentOS 7 zum Einsatz kommt, sodass einige benötigte Pakete zu stabil sind. Oder anders ausgedrückt zu veraltet sind. Hugo bricht daher mit der folgenden Fehlermeldung ab.

{{< highlight bash >}}
/home/laythos/bin/hugo: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.20' not found (required by /home/laythos/bin/hugo)
/home/laythos/bin/hugo: /lib64/libstdc++.so.6: version `CXXABI_1.3.8' not found (required by /home/laythos/bin/hugo)
/home/laythos/bin/hugo: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.21' not found (required by /home/laythos/bin/hugo)
{{< /highlight >}}

Das Problem ist den Entwicklern bekannt ([https://github.com/gohugoio/hugo/issues/9330](https://github.com/gohugoio/hugo/issues/9330)), aber aus nachvollziehbaren Gründen werden für alte Distributionen keine fertig kompilieren Versionen angeboten.

Also bleiben in dem Fall zwei Möglichkeiten. Man erzeugt die Internetseite jedes Mal lokal und lädt diese beispielsweise mit rsync auf den Webspace. Oder man kompiliert eine Version von Hugo die CentOS 7 unterstützt. Ich habe mich für letzteres entschieden.

Das Kompilieren direkt auf einem Uberspace sollte man allerdings nicht probieren. Denn dort kann man maximal 1,5 GB RAM nutzen. Alles, was mehr braucht, wird automatisch beendet. Was bei Hugo der Fall ist.

Daher habe ich mir von https://www.osboxes.org eine virtuelle Umgebung von CentOS 7 heruntergeladen und mit VirtualBox gestartet. Anschließend habe ich golang sowie gcc-c++ installiert. Ersteres über [Umwege](https://computingforgeeks.com/install-go-golang-on-centos-rhel-linux/), da Go scheinbar nicht in den offiziellen Paketquellen vorhanden ist.

Danach bin ich wie folgt vorgegangen.

{{< highlight bash >}}
wget https://github.com/gohugoio/hugo/archive/refs/tags/v0.97.3.zip
unzip v0.97.3.zip
cd hugo-0.97.3
CGO_ENABLED=1 go install --tags extended
{{< /highlight >}}

Mit dem ersten Befehl wir der Sourcecode der derzeit aktuellen Version heruntergeladen. Mit dem zweiten Befehl wird dieser entpackt. Der dritte Befehl wechselt in das betreffende Verzeichnis, in dem die Dateien entpackt wurden. Und der letzte Befehl kompiliert die extended Version von Hugo.

Wenn der letzte Befehl erfolgreich ausgeführt wurde, was je nach Hardware ein paar Minuten dauern kann, sollte man im Verzeichnis ~/go/bin/ die Datei {{< mark >}}hugo{{< /mark >}} finden. Diese kopiert man dann einfach auf seinen Uberspace. Damit sollte dann das Erzeugen der Internetseite ohne Fehlermeldungen funktionieren.

Unter https://e1.pcloud.link/publink/show?code=kZSMfzZx0nn3yYPAEVOyuhqvPPdgLfvF5ek habe ich die aktuelle von mir erzeugte Version von Hugo die bei Uberspace.de funktioniert hochgeladen. Wer will (und mir vertraut), kann diese gerne nutzen. 

{{< notice info "Hugo für Uberspcae / CentOS 7" >}}
Zuletzt aktualisiert am {{< lastmod >}}.  
Version: {{< param version >}}  
Sha256sum: {{< param sha256sum >}}
{{< /notice >}}