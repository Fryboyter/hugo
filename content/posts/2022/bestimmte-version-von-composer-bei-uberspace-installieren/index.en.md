---
title: Install specific version of Composer at Uberspace
date: 2022-07-28T19:38:14+0200
categories:
- Linux
- Englis(c)h
tags:
- Composer
- Uberspace
slug: install-specific-version-of-composer-at-uberspace
---
When I just tried to update a [Wallabag](https://github.com/wallabag/wallabag) installation at [Uberspace.de](https://uberspace.de/), I got the following error message.

{{< highlight bash >}}
Your lock file does not contain a compatible set of packages. Please run composer update.

  Problem 1
    - Root composer.json requires composer < 2.3, found composer[2.3.10] but it does not match the constraint.
{{< /highlight >}}

The recommendation to run {{< mark >}}composer update{{< /mark >}} does not help in this case. The problem is that Uberspace has version 2.3.10 of Composer installed. Wallabag, however, currently requires Composer in a version < 2.3. So far I had with Uberspace actually only cases where certain packages were too stable (one could also say too old).

So, at least temporarily, a workaround is necessary. On the Uberspace in question, I ran the following commands.

{{< highlight bash >}}
wget -qO composer-setup.php https://getcomposer.org/installer
php composer-setup.php --install-dir=/home/$USER/bin --filename=composer --version=2.2.17
{{< /highlight >}}

The first command downloads the installer of Composer. The second one downloads version 2.2.17 of Composer and saves it in the ~/bin directory with the filename composer.

I was then able to update Wallabag without any problems. Since ~/bin is part of $PATH and Composer is not necessary for using Wallabag, I renamed the file to composer.old to avoid problems with other installed software that needs a newer version.