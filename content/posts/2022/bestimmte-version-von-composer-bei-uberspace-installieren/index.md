---
title: Bestimmte Version von Composer bei Uberspace installieren
date: 2022-07-28T19:38:14+0200
categories:
- OSBN
- Linux
tags:
- Composer
- Uberspace
slug: bestimmte-version-von-composer-bei-uberspace-installieren
---
Als ich eben eine [Wallabag](https://github.com/wallabag/wallabag)-Installation bei [Uberspace.de](https://uberspace.de/) aktualisieren wollte, habe ich folgende Fehlermeldung erhalten.

{{< highlight bash >}}
Your lock file does not contain a compatible set of packages. Please run composer update.

  Problem 1
    - Root composer.json requires composer < 2.3, found composer[2.3.10] but it does not match the constraint.
{{< /highlight >}}

Die Empfehlung {{< mark >}}composer update{{< /mark >}} auszuführen, hilft in diesem Fall nicht. Denn das Problem ist, dass bei Uberspace Version 2.3.10 von Composer installiert ist. Wallabag setzt aktuell aber Composer in einer Version < 2.3 voraus. Bisher hatte ich bei Uberspace eigentlich nur Fälle bei denen bestimmte Pakete zu stable (man könne auch sagen zu alt) waren. Öfter mal was Neues.

Also ist, zumindest vorübergehend, ein Workaround nötig. Auf dem betreffenden Uberspace hab ich folgende Befehle ausgeführt.

{{< highlight bash >}}
wget -qO composer-setup.php https://getcomposer.org/installer
php composer-setup.php --install-dir=/home/$USER/bin --filename=composer --version=2.2.17
{{< /highlight >}}

Der erste Befehl lädt den Installer von Composer herunter. Mit dem zweiten wird die Version 2.2.17 von Composer heruntergeladen und im Verzeichnis ~/bin mit dem Dateinamen composer gespeichert.

Anschließend konnte ich Wallabag ohne Probleme aktualisieren. Da ~/bin Teil von $PATH ist und Composer nicht für die Nutzung von Wallabag nötig ist, habe ich die Datei abschließend in composer.old umbenannt um eventuelle Probleme mit anderer, installierter Software vorzubeugen die eine neuere Version benötigt.