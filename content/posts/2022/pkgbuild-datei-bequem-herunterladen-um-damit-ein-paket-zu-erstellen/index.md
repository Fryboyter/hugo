---
title: PKGBUILD-Datei bequem herunterladen um damit ein Paket zu erstellen
date: 2022-09-05T23:05:03+0200
categories:
- OSBN 
- Linux
tags:
- PKGBUILD
- Makepkg
- Arch Linux
slug: pkgbuild-datei-bequem-herunterladen-um-damit-ein-paket-zu-erstellen
---
Ab und zu benötige ich eine PKGBUILD-Datei, um damit ein Paket für Arch Linux zu erstellen, um dieses beispielsweise auf mehreren Computern im LAN zu installieren.

Nehmen wir https://aur.archlinux.org/packages/ttf-iosevka-term als beliebiges Beispiel. Man kann beispielsweise unter genannten Link auf "View PKGBUILD" klicken und dann den angezeigten Inhalt in einer PKGBUILD-Datei lokal speichern. Alternativ kann man auch {{< mark >}}git clone --depth 1 https://aur.archlinux.org/ttf-iosevka-term.git{{< /mark >}} ausführen. Dieser speichert die gewünschte Datei automatisch aber man muss den genauen Link kennen. Ich finde beide Lösungen nicht besonders gut.

Heute bin ich auf das Tool [pbget](https://xyne.dev/projects/pbget/) gestoßen. Mit diesem muss man nur den Namen des zu erstellenden Pakets kennen. Somit reicht der Befehl {{< mark >}}pbget --aur ttf-iosevka-term{{< /mark >}} aus um die gewünschte PKGBUILD-Datei herunterzuladen. Die Angabe von {{< mark >}}--aur{{< /mark >}} ist in dem Fall nötig, da ttf-iosevka-term nur im AUR vorhanden ist und pbget von Haus aus nur die offiziellen Paketquellen durchsucht.

Der Befehl sollte das Verzeichnis ttf-iosevka-term anlegen und in diesem die PKBUILD-Datei speichern. Wechselt man in dieses Verzeichnis und führt {{< mark >}}makepkg -crs PKGBUILD –noconfirm{{< /mark >}} aus wird das Paket erzeugt. Oder man führt {{< mark >}}makepkg -cirs PKGBUILD –noconfirm{{< /mark >}} aus, um das Paket auch gleich zu installieren.

Pbget findet man übrigens im AUR.