---
title: Download PKGBUILD file conveniently to create a package with it
date: 2022-09-05T23:05:03+0200
categories:
- Linux
- Englis(c)h
tags:
- PKGBUILD
- Makepkg
- Arch Linux
slug: download-pkgbuild-file-conveniently-to-create-a-package-with-it
---
Every now and then I need a PKGBUILD file to create a package for Arch Linux, for example to install it on multiple computers in a LAN.

Let's take https://aur.archlinux.org/packages/ttf-iosevka-term as a random example. For example, one can click on "View PKGBUILD" at mentioned link and then save the displayed content in a PKGBUILD file locally. Alternatively, one can also run {{< mark >}}git clone --depth 1 https://aur.archlinux.org/ttf-iosevka-term.git{{< /mark >}}. This automatically saves the desired file but you have to know the exact link. I don't like either solution very much.

Today I came across the tool [pbget](https://xyne.dev/projects/pbget/). With this one only needs to know the name of the package to be created. Thus, the command {{< mark >}}pbget --aur ttf-iosevka-term{{< /mark >}} is sufficient to download the desired PKGBUILD file. It is necessary to use {{< mark >}}--aur{{< /mark >}} in this case, because ttf-iosevka-term is only available in the AUR and pbget only searches the official package sources by default.

The command should create the directory ttf-iosevka-term and store the PKBUILD file in it. If you change to this directory and run {{< mark >}}makepkg -crs PKGBUILD -noconfirm{{< /mark >}} the package will be created. Or you can run {{< mark >}}makepkg -cirs PKGBUILD -noconfirm{{< /mark >}} to also install the package.

By the way, pbget can be found in the AUR.