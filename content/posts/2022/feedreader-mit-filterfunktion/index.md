---
title: Feedreader mit Filterfunktion
date: 2022-02-09T13:55:40+0100
categories:
- OSBN
tags:
- RSS
- Feed
- Filterfunktion
slug: feedreader-mit-filterfunktion
---
Neben vielen anderen Feeds habe ich den Feed von beispielsweise osbn.de oder planet.ubuntuusers.de abonniert. Manche Blogs veröffentlichen darüber Artikel die mich in keinster Weise interessieren. Diese möchte ich daher möglichst nicht einmal in meinem Feedreader angezeigt bekommen.

Da ich seit einiger Zeit lokale Feedreader bevorzuge, habe ich in letzter Zeit QuiteRSS genutzt, da dies einer der wenigen Clients ist, die überhaupt eine Filterfunktion anbieten. Die letzte Version von QuiteRSS ist allerdings von 2020 und die Entwicklung ist ziemlich eingeschlafen. Wenn man sich Issues wie [https://github.com/QuiteRSS/quiterss/issues/1470](https://github.com/QuiteRSS/quiterss/issues/1470) oder [https://github.com/QuiteRSS/quiterss/issues/1519](https://github.com/QuiteRSS/quiterss/issues/1519) durchliest, könnte das Programm wohl in absehbarer Zeit auch aus den Paketquellen diverser Distributionen verschwinden. Ich habe mich daher einmal umgesehen was es für Alternativen gibt, die derzeit aktiv weiterentwickelt werden. Erschreckend wenig, wenn man eine halbwegs gute Filterfunktion will. Ist das so ein ungewöhnlicher Anwendungsfall? Oder habe ich nur die falschen Programme angesehen?

Wie auch immer. Aktuell teste ich [RSS Guard](https://github.com/martinrotter/rssguard). Dessen Filterfunktion ist zwar nicht so einfach nutzbar, dafür aber deutlich mächtiger als die von QuiteRSS. Was daran liegt, dass diese auf Javascript basiert. Ein Filter der beispielsweise alle Artikel eines bestimmten Autors automatisch löscht, würde beispielsweise wie folgt aussehen.

{{< highlight javascript >}}
function filterMessage() {
  if (msg.author == "soeren-hentzschel.at") {
    return MessageObject.Ignore;
  }
}
{{< /highlight >}}

Ein Vorteil der Filterfunktion von RSS Guard ist, dass man den Filter erst einmal testen kann ohne das tatsächlich Änderungen vorgenommen werden und dass die betroffenen Artikel farblich markiert werden. Neben Linux wird das Programm übrigens auch für Windows und macOS angeboten.

Abschließend noch eine Anmerkung zum Schluss. Nicht, dass es böses Blut gibt. Nur weil bestimmte Artikel bestimmter Seiten für mich nicht von Interesse sind, sagt das weder etwas über deren Qualität aus, noch möchte ich, dass diese Seiten mit der Veröffentlichung dieser Artikel aufhören. Ich will die Artikel einfach nicht angezeigt bekommen und auch nicht lesen. Genauso wie vermutlich einige Nutzer meine Artikel nicht lesen wollen.