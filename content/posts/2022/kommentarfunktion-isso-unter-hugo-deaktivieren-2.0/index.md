---
title: Kommentarfunktion Isso unter Hugo deaktivieren 2.0
date: 2022-01-02T22:25:00+0100
categories:
- OSBN
- Allgemein
tags:
- Hugo
- Isso
- Kommentarfunktion
slug: kommentarfunktion-isso-unter-hugo-deaktivieren-2.0
---
2019 hatte ich bereits einen [Artikel](/kommentarfunktion-isso-unter-hugo-deaktivieren/) veröffentlicht, wie man das Kommentarsystem [Isso](https://github.com/posativ/isso) für bestimmte Artikel unter [Hugo](https://gohugo.io) deaktiviert. Hierbei wurde aber nur der Code entfernt der für das Anlegen neuer Kommentare und das Anzeigen vorhandener Kommentare zuständig ist. Im Hintergrund wurde beispielsweise die JavaScript-Datei "embed.min.js" weiterhin geladen. Auch wenn diese nur ca. 25 kB groß ist (was einen Großteil von https://fryboyter.de ausmacht), bin ich mit der Lösung unzufrieden.

Da ich aktuell privat etwas Zeit habe, und ich diese Funktion demnächst wohl öfter nutzen werde, habe ich mir eine bessere Lösung überlegt.

Als Erstes habe ich mir die Datei [single.html](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/themes/GythaOgg/layouts/_default/single.html) vorgenommen. Diese ist für das Anzeigen von einzelnen Artikeln zuständig. Den betreffenden Code habe ich wie folgt geändert.

{{< highlight go-template >}}
{{ if and ( isset .Params "nocomments" ) ( eq .Params.nocomments true ) -}}
    <p>Die Kommentarfunktion ist für diesen Artikel deaktiviert</p>
{{ else }}
    <section id="isso-thread" data-title="{{ .Title }}"></section>
	<noscript>
		<p>Die Kommentarfunktion kann nur mit aktiviertem Javascript genutzt werden</p>
	</noscript>
{{ end }}
{{< /highlight >}}

Wichtig ist hier im Grunde genommen die erste Zeile. Damit wird geprüft, ob im Front-Matter-Bereich des jeweiligen Artikels die Zeile nocomments: vorhanden ist und ob diese den Wert true hat. Wenn ja, wird der Hinweis angezeigt, dass die Kommentarfunktion für diese Artikel deaktiviert ist. Wenn nicht, wird der Code eingetragen, mit dem man als Nutzer neue Kommentare erzeugen kann und vorhandene Kommentare anzeigt. Also im Grunde genommen, was im Artikel von 2019 beschrieben wurde.

Als Nächstes habe ich die Datei [metadata.html](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/themes/GythaOgg/layouts/partials/metadata.html) angepasst. Diese erzeugt die Zeile direkt unter dem Titel des Artikels, in der bisher unter anderem die Anzahl der Kommentare angezeigt wird. Was bei einer deaktivierten Kommentarfunktion nicht wirklich nötig ist.

{{< highlight go-template >}}
{{ if or (not ( isset .Params "nocomments" )) ( ne .Params.nocomments true ) -}} | <a href="{{ .Permalink }}#isso-thread">Comments</a>{{ end }}
{{< /highlight >}}

Hiermit wird geprüft, ob die Zeile nocomments: nicht hinterlegt ist oder wenn doch, ob dessen Wert nicht true entspricht. Wenn eines der beiden zutrifft, wird der Code für die Anzahl der vorhandenen Kommentare angezeigt. Den Code habe ich bewusst etwas verkompliziert, da ich verhindern wollte, dass beispielsweise {{< mark >}}nocomments: treu{{< /mark >}} unabsichtlich dazu führt, dass die Kommentare deaktiviert werden. Wer hierfür eine bessere Lösung hat, die weniger Code benötigt, kann sich gerne melden. Oder einfach einen [Pull Request](https://codeberg.org/Fryboyter/fryboyter.de/pulls) erstellen. Denn an der Stelle zeigt es sich, dass ich nicht wirklich programmieren kann.

Abschließend ist die Datei [head.html](https://codeberg.org/Fryboyter/fryboyter.de/src/branch/main/themes/GythaOgg/layouts/partials/head.html) an der Reihe.

{{< highlight go-template >}}
{{ if or (not ( isset .Params "nocomments" )) ( ne .Params.nocomments true ) -}}
	<script data-isso="https://isso.fryboyter.de/" data-isso-feed="true" data-isso-css="false"
		data-isso-lang="{{.Site.Language.Lang }}" data-isso-max-comments-top="inf" data-isso-max-comments-nested="inf"
		data-isso-vote="false" data-isso-reply-notifications="true"
		src="https://isso.fryboyter.de/js/embed.min.js"></script>
	{{ end }}
{{< /highlight >}}

Hier kommt unterm Strich die gleiche Abfrage wie für metadata.html zum Einsatz. Nur das in diesem Fall nicht die Anzeige der vorhandenen Kommentare ausgeblendet wird, sondern die Datei embed.min.js geladen wird oder eben nicht.

Das ganze Changeset kann man sich unter [https://codeberg.org/Fryboyter/fryboyter.de/commit/5dfb4459aa7f9c8b08a21a9568d775808e290b49](https://codeberg.org/Fryboyter/fryboyter.de/commit/5dfb4459aa7f9c8b08a21a9568d775808e290b49) ansehen.

Warum nun diese Änderung? Im Grunde aus zwei Gründen. 

Zum einen schalte ich bereits jetzt weniger als 80 Prozent der Kommentare frei, da viele Kommentare nur SEO-Spam oder Kommentare sind, die nicht weiterhelfen. Daher auch die manuelle Freischaltung der Kommentare.

Das größere Problem ist aber, dass ich oft keine Lust oder Zeit habe Kommentare zeitnah freizuschalten, sodass diese oft Tage oder gar Wochen nicht angezeigt werden. Das nützt daher weder den Nutzern noch mir etwas. Daher verzichte ich im Zweifelsfall zukünftig lieber auf Kommentare Dritter.