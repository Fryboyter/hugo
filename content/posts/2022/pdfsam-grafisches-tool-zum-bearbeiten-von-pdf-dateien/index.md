---
title: PDFsam - Grafisches Tool zum Bearbeiten von PDF-Dateien
date: 2022-02-20T15:15:17+0100
categories:
- OSBN
tags:
- PDF
slug: pdfsam-grafisches-tool-zum-bearbeiten-von-pdf-dateien
---
Ich musste heute diverse PDF-Dateien splitten und aus unterschiedlichen einzelnen Dateien wieder neue PDF-Dateien erstellen. Darauf, dies mit Befehlen im Terminal Emulator zu erledigen, hatte ich aber keine Lust. Also habe ich mir spontan PDFsam installiert.

Bei [PDFsam](https://pdfsam.org) handelt es sich um ein Tool mit grafischer Oberfläche, mit dem man PDF-Dateien bearbeiten kann. Die [Basic-Version](https://github.com/torakiki/pdfsam), die unter der AGPL-3.0 Lizenz veröffentlicht ist und die kostenlos nutzbar ist, unterstützt das Teilen (Seitenzahl, Lesezeichen und Größe), Zusammenführen, Extrahieren, Mischen und Drehen von PDF-Dateien. Die kostenpflichtigen Versionen wie PDFsam Enhanced oder PDFsam Visual bieten noch zusätzliche Funktionen. Allerdings werden diese nur im Abo-Modell angeboten und sind meiner Meinung nach ziemlich teuer. Was alle Versionen gemeinsam haben ist, dass es sich um Java-Programme handelt. Was in meinem Fall kein Problem ist, da ich bereits Java wegen eines anderen Tools installiert habe.

{{< image src="pdfsam.webp" alt="pdfsam" >}}

Der Screenshot zeigt die Oberfläche für das Zusammenführen von PDF-Dateien. Meiner Meinung nach ist diese, wie auch die der anderen Funktionen, aufgeräumt und selbsterklärend. Das Tool an sich arbeitet schnell und meiner bisherigen Erfahrung nach zuverlässig. Wer also mal keine Lust hat PDF-Dateien im Terminal Emulator zu bearbeiten, sollte sich das Programm zumindest einmal ansehen.