---
title: Insert notices in articles under Hugo
date: 2022-01-04T20:05:45+0100
categories:
- Englis(c)h
tags:
- Hugo
- Notices
- Update
slug: insert-notices-in-articles-under-hugo
---
Recently I published an [article](/comment-function-isso-under-hugo-deactivate-2.0/), which referred to an older article, which is therefore basically no longer relevant. Because I don't want to delete this article, I thought about adding a note to this article. In case someone accesses the article from 2019 directly.

I have solved this by creating a [shortcode](https://gohugo.io/content-management/shortcodes/), which can be used independently of the articles. In the directory of the theme I used, I created the file notice.html in the subdirectory shortcodes and entered the following content.

{{< highlight go-template >}}
<div class="shortcode-notice {{ .Get 0 }}">
    <div class="shortcode-notice-title {{ .Get 0 }}">
        {{- if len .Params | eq 2 -}}
        {{ .Get 1 }}
        {{ else }}
        {{ .Get 0 }}
        {{- end -}}
    </div>
    <div class="notice-content">{{ .Inner | markdownify }}</div>
</div>
{{< /highlight >}}

Additionally I created the file notice.css in the subdirectory static/css in the theme directory and entered the following content.

{{< highlight css >}}
.shortcode-notice .notice-content {
    padding: .6em 1em;
	display: block;
	font-size: 1em;
	margin-top: 0;
	margin-bottom: 0;
	color: #666;
    border-radius: 4px;
}

.shortcode-notice-title {
	color: #fff;
	padding-left: 1em;
	font-weight: 700;
	text-transform: capitalize;
    border-radius: 4px 4px 0 0;
}

.shortcode-notice-title.hinweis {
	background-color: rgba(92, 184, 92, .8)
}

 .shortcode-notice.hinweis .notice-content {
	background: #e6f9e6
}

 .shortcode-notice-title.tip {
	background-color: #6ab0de
}

.shortcode-notice.tip .notice-content {
	background: #e7f2fa
}

.shortcode-notice-title.info {
	background-color: #f0b37e
}

.shortcode-notice.info .notice-content {
	background: #fff2db
}

.shortcode-notice-title.warnung {
	background-color: rgba(217, 83, 79, .8)
}

.shortcode-notice.warnung .notice-content {
	background: #fae2e2
}

{{< /highlight >}}

If you now want to add a corresponding note to an article, you can use the following shortcode.

{{< highlight go-template >}}
{{%/* notice info */%}}
This is an information
{{%/* /notice */%}}
{{< /highlight >}}

Instead of info, you can use hint, tip and warning in the first line, which results in the background of the display having a different color accordingly. In the case of warning, for example, red. If this is not enough or too much, you can of course extend or remove the entries in the CSS file. 

In the case of the already addressed outdated 2019 article, this generates the following [note](/comment-function-isso-under-hugo-deactivate/).
