---
title: Hinweise in Artikeln unter Hugo einfügen
date: 2022-01-04T17:17:06+0100
categories:
- OSBN
- Allgemein
tags:
- Hugo
- Hinweis
- Aktualisierung
slug: hinweise-in-artikeln-unter-hugo-einfügen
---
Kürzlich habe ich einen [Artikel](/kommentarfunktion-isso-unter-hugo-deaktivieren-2.0/) veröffentlicht, der sich auf einen älteren Artikel bezogen hat, der somit im Grunde nicht mehr relevant ist. Da ich diesen aber nicht löschen will, habe ich mir überlegt in diesem einen entsprechenden Hinweis einzutragen. Also für den Fall, dass jemand den Artikel von 2019 direkt aufruft.

Gelöst habe ich dies mittels eines [Shortcodes](https://gohugo.io/content-management/shortcodes/), der unabhängig von den Artikeln genutzt werden kann. Im Verzeichnis des von mir verwendeten Themes habe ich im Unterverzeichnis shortcodes die Datei notice.html angelegt und folgenden Inhalt eingetragen.

{{< highlight go-template >}}
<div class="shortcode-notice {{ .Get 0 }}">
    <div class="shortcode-notice-title {{ .Get 0 }}">
        {{- if len .Params | eq 2 -}}
        {{ .Get 1 }}
        {{ else }}
        {{ .Get 0 }}
        {{- end -}}
    </div>
    <div class="notice-content">{{ .Inner | markdownify }}</div>
</div>
{{< /highlight >}}

Zusätzlich habe ich im Unterverzeichnis static/css im Theme-Verzeichnis die Datei notice.css angelegt und folgenden Inhalt eingetragen.

{{< highlight css >}}
.shortcode-notice .notice-content {
    padding: .6em 1em;
	display: block;
	font-size: 1em;
	margin-top: 0;
	margin-bottom: 0;
	color: #666;
    border-radius: 4px;
}

.shortcode-notice-title {
	color: #fff;
	padding-left: 1em;
	font-weight: 700;
	text-transform: capitalize;
    border-radius: 4px 4px 0 0;
}

.shortcode-notice-title.hinweis {
	background-color: rgba(92, 184, 92, .8)
}

 .shortcode-notice.hinweis .notice-content {
	background: #e6f9e6
}

 .shortcode-notice-title.tip {
	background-color: #6ab0de
}

.shortcode-notice.tip .notice-content {
	background: #e7f2fa
}

.shortcode-notice-title.info {
	background-color: #f0b37e
}

.shortcode-notice.info .notice-content {
	background: #fff2db
}

.shortcode-notice-title.warnung {
	background-color: rgba(217, 83, 79, .8)
}

.shortcode-notice.warnung .notice-content {
	background: #fae2e2
}
{{< /highlight >}}

Will man nun einen entsprechenden Hinweis in einen Artikel eintragen, kann man folgenden Shortcode nutzen.

{{< highlight go-template >}}
{{%/* notice info */%}}
Das ist eine Information
{{%/* /notice */%}}
{{< /highlight >}}

Anstelle von info kann man in der ersten Zeile hinweis, tip und warnung verwenden was zur Folge hat, dass der Hintergrund der Anzeige entsprechend eine andere Farbe hat. Im Falle der Warnung beispielsweise rot. Wem das nicht ausreicht oder zu viel ist, kann natürlich die Einträge in der CSS-Datei erweitern oder entfernen. 

Im Falle des bereits angesprochenen, veralteten Artikels von 2019 erzeugt dies folgenden [Hinweis](/kommentarfunktion-isso-unter-hugo-deaktivieren/).