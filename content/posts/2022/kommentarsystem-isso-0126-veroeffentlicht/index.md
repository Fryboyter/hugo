---
title: Kommentarsystem Isso 0.12.6 veröffentlicht
date: 2022-03-15T22:03:46+0100
categories:
- OSBN
tags:
- Python
- JavaScript
- Kommentare
slug: kommentarsystem-isso-0-12-6-veroeffentlicht
---
Vor ein paar Stunden wurde Version 0.12.6 des Kommentarsystems [Isso](https://github.com/posativ/isso) veröffentlicht welches ich für fryboyter.de verwende.

Das Changelog findet man unter [https://github.com/posativ/isso/releases/tag/0.12.6](https://github.com/posativ/isso/releases/tag/0.12.6).

Ein Update mittels pip hat bei mir problemlos funktioniert. Was mich positiv überrascht hat da ich mit Isso bei der Installation bzw. bei einem Update schon öfter Probleme hatte.
