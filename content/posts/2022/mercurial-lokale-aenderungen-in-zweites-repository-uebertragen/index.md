---
title: Mercurial - Lokale Änderungen in zweites lokales Repository übertragen
date: 2022-01-25T20:36:35+0100
categories:
- OSBN
- Allgemein
tags:
- Mercurial
- versionsverwaltung
slug: mercurial-lokale-aenderungen-in-zweites-lokales-repository-uebertragen
---
Ab und zu beginne ich damit, Änderungen in einem lokalen [Mercurial](https://www.mercurial-scm.org) Repository durchzuführen und werde damit nicht fertig. In der Regel mache ich dann später einfach weiter und nutze dafür den gleichen Rechner. Ab und zu möchte ich aber lieber mit einem anderen Rechner weiterarbeiten. Zum Beispiel mit meinem Notebook auf dem Sofa.

Wie nun am besten die lokalen Änderungen von Rechner A auf Rechner B übertragen?

Am einfachsten wäre es wohl, einen Commit über die unfertigen Änderungen zu erstellen, diesen bei beispielsweise Github hochzuladen und dann auf Rechner B das Arbeitsverzeichnis damit zu aktualisieren. Ich vermeide aber möglichst solche Work in Progress Commits.

Das Repository mit Tools wie [Syncthing](https://syncthing.net) von einem Rechner auf den anderen übertragen? Das klappt bei Repositories wohl in der Regel. Aber es gibt auch schon einige negativen Erfahrungen. Wohl hauptsächlich mit den Verzeichnissen wie .git oder .hg in denen unter anderem die Historie gespeichert ist. Ein zusätzliches bare Repository zu nutzen wäre vermutlich eine Alternative.

Da ich den Anwendungsfall aber nur sehr habe, habe ich mich für eine andere Lösung entschieden. Im Arbeitsverzeichnis des Repositories, in dem ich mit den Änderungen begonnen habe, erstelle ich mittels {{< mark >}}hg diff > patch.diff{{< /mark >}} einen Patch über die vorhandenen Änderungen. Diesen Patch spiele ich dann im zweiten Repository mittels {{< mark >}}hg import --no-commit patch.diff{{< /mark >}} ein. Wichtig ist hierbei den Parameter --no-commit zu nutzen, da sonst für die Änderungen ein Commit erstellt wird, was ich ja erst einmal vermeiden will bis ich wirklich fertig bin.

Das mag nun nicht die beste Lösung sein. Und vielleicht kann man es mit einer anderen Versionsverwaltung (die ich nicht nutzen will) einfacher lösen. Aber für mich ist es erst einmal ausreichend.