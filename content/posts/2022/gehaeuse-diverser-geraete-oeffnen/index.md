---
title: Gehäuse diverser Geräte öffnen
date: 2022-04-03T11:13:44+0200
categories:
- OSBN
- Allgemein
tags:
- Plektrum
- Gehäuse
- Wartung
slug: gehäuse-diverser-geräte-öffnen
---
Vor einigen Monaten habe ich mir ein neues Notebook gekauft. Ein Thinkpad E14 Gen 3 um genau zu sein. Unter Linux gibt es nur zwei Probleme. Der Fingerabdrucksensor, den ich nicht benötige, funktioniert aktuell gar nicht. Und es ist eine WLAN-Karte von RealTek verbaut. Was zwar immer noch besser als Broadcom ist, aber eine Karte von Intel wäre besser als alle beide.

Ich habe mir daher die Karte AX200 von Intel gekauft, die unter Linux "out of the box" unterstützt wird und die einfach funktioniert.

Als ich die Karte einbauen wollte, habe ich die Schrauben am Gehäuseboden den Notebooks entfernt. Danach hat sich dieser aber keinen Millimeter bewegt. Lenovo ist, zumindest bei diesem Modell, dazu übergegangen das Ganze noch zusätzlich mit Clips zu sichern. Das ist immer noch besser als das Gehäuse zu verkleben, aber was soll das? Mein altes Thinkpad X230 hatte keine Clips und es ist weder explodiert noch auseinander gefallen. Dafür ist die Hardware im E14 zugegeben besser zugänglich.

In solch einem Fall versuche ich die Clips zu lösen, indem ich beispielsweise mit einer alten Kreditkarte oder einer Telefonkarte (die älteren Leser werden wissen, was ich meine) im Schlitz zwischen dem oberen und unteren Teil des Gehäuses mit etwas Druck entlang fahre und dabei mantraartig den Satz "hoffentlich brechen die Clips nicht ab" wiederhole. Wartungs-Voodoo sozusagen.

Da diese Karten relativ instabil sind, ist diese Lösung keine gute. Auch wenn sie meist funktioniert. Ein Musiker, den ich kenne, hat mir einen verdammt guten Tipp gegeben. Ein Plektrum. Also diese dreieckigen Dinger mit denen man eine Saite einer Gitarre anschlägt. Diese gibt es in verschiedenen Steifigkeiten. Wenn man ein ziemlich hartes Plektrum nimmt, ist das eigentlich ideal um solche Gehäuse zu öffnen.

Aber es gibt doch Reparaturkits von beispielsweise iFixit mit speziellen Tools, um Gehäuse zu öffnen? Ja gibt es. Diese Dinger ähneln verblüffend Plektren. Kosten dafür aber mehr. Und in meinem Fall habe ich schon diverse Schraubendreher, Pinzetten und so weiter, sodass sich ein komplettes Set nicht lohnen würde. Wer also in der gleichen Lage ist wie ich, sollte sich daher eher ein Plektrum kaufen. Oder eines abstauben, wenn er einen Gitarrenspieler kennt.

Was vielleicht auch funktionieren könnte, wäre ein Reifenheber, den man beim Wechsel eines Fahrradreifens nutzt. Ausprobiert habe ich es aber noch nicht.