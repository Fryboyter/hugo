---
title: PDF-Dateien mit durchsuchbarer Textebene erstellen
date: 2022-11-30T19:06:35+0100
categories:
- OSBN
tags:
- PDF
- OCR
slug: pdf-dateien-mit-durchsuchbarer-textebene-erstellen
---
Sicherlich hat jeder schon einmal eine PDF-Datei erhalten, deren Text man beispielsweise nicht markieren und kopieren kann. Dies liegt in der Regel daran, dass der eingescannte Text als Bild in der PDF-Datei eingebettet wurde. Der Multifunktionsdrucker, den ich beruflich nutze, macht das zum Beispiel. Das Problem lässt sich allerdings relativ leicht und zuverlässig lösen.

Das Tool [OCRmyPDF](https://github.com/ocrmypdf/OCRmyPDF) fügt in die PDF-Datei eine zusätzliche Textebene über dem Bild ein, deren Inhalt man markiere und kopieren sowie durchsuchen kann. Für die Texterkennung wird [Tesseract](https://github.com/tesseract-ocr/tesseract) verwendet.

Im besten Fall führt man einfach {{< mark >}}ocrmypdf input.pdf output.pdf{{< /mark >}} aus. Input.pdf ist hierbei die Originaldatei und output.pdf ist die Datei, die mit der zusätzlichen Textebene gespeichert wird. Weitere Funktionen und mögliche Optimierungsmöglichkeiten kann man in der [Dokumentation](https://ocrmypdf.readthedocs.io/en/latest/) nachlesen. Je nachdem in welcher Sprache der Inhalt der Originaldatei vorliegt, muss man ggf. vorher noch ein Sprachpaket wie beispielsweise tesseract-data-eng (englische Sprachpaket) installieren. Fehlt diese, gibt ORCmyPDF aber auch einen entsprechenden Hinweis aus und bricht ab.

Getestet habe ich OCRmyPDF mit ein paar einfachen PDF-Dateien mit gut lesbarem Text getestet. Bei diesen wurde die zusätzliche Textebene sehr gut über die Bilddatei gelegt, sodass der Text beider Ebenen sehr genau übereinander lag, sodass das Markieren und Kopieren des Textes kein Problem war. Auch das Durchsuchen der Datei hat funktioniert. Die neue Datei benötigt logischerweise mehr Speicherplatz aber es hält sich meiner Meinung nach in Grenzen. Die originale PDF-Datei mit einer DIN-A4-Seite mit den ersten Zeilen von "Der Rabe" von Edgar Allan Poe ist beispielsweise 49,4 KB groß. Die um die Textebene erweiterte PDF-Datei ist 49,8 KB groß.