---
title: I/O Scheduler automatisch einstellen
date: 2022-03-07T21:23:48+0100
categories:
- OSBN
- Linux
tags:
- Scheduler
- Udev
slug: i-o-scheduler-automatisch-einstellen
---
Mit einem I/O Scheduler wird die zeitliche Abfolge von Lese- und Schreibvorgänge bei Speichermedien koordiniert. Je nachdem ob es sich um eine normale HDD, eine SSD oder eine NVMe handelt, können unterschiedliche Scheduler sinnvoll sein. Oft nutzen Distributionen allerdings den gleichen Scheduler für alle Speichermedien.

Bei einem normalen Rechner eines durchschnittlichen Benutzers fällt dies meist nicht ins Gewicht. Optimal ist es trotzdem nicht. Also sollte man sein System entsprechend konfigurieren.

Nun ist es so, dass ich in meinem Hauptrechner sowohl HDD, SSD als auch NVMe verbaut habe und regelmäßig zusätzliche Speichermedien temporär anschließe. Somit möchte ich den Scheduler automatisch zuweisen lassen. Dies lässt sich mit einer [udev](https://de.wikipedia.org/wiki/Udev) Regel umsetzen. Hierfür erstellt man die Datei /etc/udev/rules.d/60-ioschedulers.rules und füllt diese mit folgendem Inhalt.

{{< highlight bash >}}
# Scheduler für HDD
ACTION=="add|change", KERNEL=="sd[a-z]*", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
# Scheduler für SSD
ACTION=="add|change", KERNEL=="sd[a-z]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
# Scheduler für NVMe
ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="none"
{{< /highlight >}}

Hiermit wird normalen HDD der Scheduler bfq zugeordnet. Bei SSD kommt der Scheduler mq-deadline zum Einsatz. Und im Falle von NVMe wird meist none eingestellt, da bei diesen Speichermedien ein anderer Scheduler oft keinen Vorteil bringt. Wer will, kann natürlich etwas anderes einstellen.

Anschließend liest man mit Root-Rechten mittels {{< mark >}}udevadm trigger{{< /mark >}} die angeschlossene Hardware neu ein damit die Regeln genutzt werden. Alternativ kann man auch einfach den Rechner neu starten.

Danach sollte je nach Speichermedium der entsprechende Scheduler aktiv sind. Überprüfen lässt sich dies mit dem Befehl {{< mark >}}grep "" /sys/block/*/queue/scheduler{{< /mark >}}. Der jeweils verwendete Scheduler wird in eckigen Klammern angezeigt.