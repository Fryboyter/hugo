---
title: Mehrere Befehle mit einem Shortcut unter VS Code ausführen
date: 2022-01-15T11:16:29+0100
categories:
- OSBN
- Allgemein
tags:
- VS Code
- Shortcuts
slug: mehrere-befehle-mit-einem-shortcut-unter-vs-code-ausfuehren
---

Wenn ich einen Artikel erstelle, in dem ich ein oder mehrere Code-Beispiele zeige, wandle ich erst einmal diverse Zeichen des Codes in [Entitäten](https://de.wikipedia.org/wiki/HTML-Entit%C3%A4t) um, sodass der Browser nicht versucht den Code auszuführen. Dies hat sonst oft ungewollte Nebenwirkungen. Somit wird das Zeichen < beispielsweise in &amp;lt; umgewandelt. Danach packe ich den Code in einen pre und einen code Tag der für das Hervorheben des Codes zuständig ist. Dies sieht dann beispielsweise wie folgt aus.

{{< highlight bash >}}
<pre class="line-numbers language-bash" style="white-space:pre-wrap;">
<code class="language-bash">#!/bin/bash

if [ $# -eq 0 ]
  then
    echo &quot;Usage: $0 &lt;user_name&gt; &quot;
    exit;
fi

USER=$1

for repo in $(curl -s https://api.github.com/users/&quot;$USER&quot;/repos?per_page=1000 |grep git_url |awk &apos;{print $2}&apos;| sed &apos;s/&quot;\(.*\)&quot;,/\1/&apos;);do
git clone &quot;$repo&quot;;
done;</code>
</pre>
{{< /highlight >}}

Hierfür nutze ich jeweils einen extra Shortcut der dies automatisiert. Für das Umwandeln der Zeichen wäre es dieser Shortcut.

{{< highlight bash >}}
{
    "key": "shift+f3",
    "command": "extension.htmlEntities"
}
{{< /highlight >}}

Mittels Shift + F3 wird die Erweiterung [html-entities](https://marketplace.visualstudio.com/items?itemName=christopherstyles.html-entities) ausgeführt. Und genau das vergesse ich ab und zu.

Nützlich wäre daher ein Shortcut der automatisch beide Aktionen durchführt. Das unterstützt VS Code aber leider aktuell nicht. Da ich derzeit keinen anderen Editor nutzen möchte, habe ich mir die Erweiterung [multi-command](https://marketplace.visualstudio.com/items?itemName=ryuta46.multi-command) installiert. Damit ist es möglich mehrere Befehle nacheinander auszuführen. Herausgekommen ist schlussendlich folgender Shortcut.

{{< highlight bash >}}
{
    "key":"shift+f5",
    "command":"extension.multiCommand.execute",
    "args":{
        "sequence":[
            "extension.htmlEntities",
            {
                "command":"editor.action.insertSnippet",
                "args":{
                    "name":"wrap_highlight"
                }
            }
        ]
    }
}
{{< /highlight >}}


Der Shortcut Shift + F5 ruft die Erweiterung multi-command auf welche als Erstes die Erweiterung html-entities und danach das Snippet [wrap_highlight](/maskieren-von-text-in-snippets-bei-vs-code/) ausführt. Letzteres hebt in meinem Fall den Code entsprechend hervor. Zukünftig werde ich also nicht mehr vergessen, Code-Beispiele vor der Veröffentlichung eines Artikels umzuwandeln.
