---
title: Nachrichtenportale stellen sukzessive ihre RSS-Angebote ein. Aber...
date: 2022-09-02T18:41:28+0200
categories:
- OSBN
- Allgemein 
tags:
- RSS
- Feed
slug: nachrichtenportale-stellen-sukzessive-ihre-rss-angebote-ein-aber
---
Viktor Garske hat kürzlich über https://blog.v-gar.de den Artikel [Neuer Podcast über IT-Sicherheit: Risikozone](https://blog.v-gar.de/2022/08/neuer-podcast-uber-it-sicherheit-risikozone/) veröffentlicht in dem er die Aussage getroffen hat, dass immer mehr Nachrichtenportale die RSS-Feeds eingestellt haben.

Der Aussage will ich nicht unbedingt widersprechen. Viele Internetseiten nutzen aber beispielsweise WordPress. Und viele dieser Seiten bieten zwar offiziell keinen RSS-Feed an, vergessen allerdings diesen zu deaktivieren. Somit kann man, wenn man die Adressen kennt, trotzdem die Feeds abrufen. Im Falle von WordPress kann man sich an https://wordpress.org/support/article/wordpress-feeds/ orientieren um die Adresse des Feeds herauszufinden. 

Somit kann man in vielen Fällen trotzdem RSS-Feeds abrufen, auch wenn die betreffende Seite offiziell keine Feeds anbietet.

Https://fryboyter.de bietet beispielsweise über https://fryboyter.de/categories/osbn/index.xml einen Feed für die Artikel an, die auf https://osbn.de und https://planet.ubuntuusers.de veröffentlicht werden. Über https://fryboyter.de/index.xml erreicht man den Feed für alle Artikel die ich auf fryboyter.de veröffentliche.

Vielleicht wäre es daher keine schlechte Idee, Feed-Adressen von Internetseiten zu sammeln, die offizielle gar keinen RSS-Feed anbieten.