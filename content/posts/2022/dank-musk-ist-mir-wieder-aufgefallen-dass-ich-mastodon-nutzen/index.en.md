---
title: Thanks to Musk I noticed again that I use Mastodon
date: 2022-11-01T14:31:07+0100
categories:
- Englis(c)h
tags:
- Twitter
- Mastodon
- Musk
slug: thanks-to-musk-i-noticed-again-that-i-use-mastodon
---
Recently, after some back and forth, Elon Musk finally bought Twitter. Shortly after, various articles were published about what alternatives there are. Why not before? Never mind.

Due to the takeover of Twitter, I was wondering if I already used something like Twitter for fryboyter.de. Spontaneously the answer is no, because I don't care about the so-called "social media". That's why I don't have an account at Twitter or Facebook. But there was something there some time ago.

After thinking about it a bit, I remembered that at some point I experimented with [IFTTT](https://ifttt.com) to publish the articles published on fryboyter.de on other platforms. But that was a long time ago.

As I just noticed, the account at IFTTT still exists. And the applet is on the one hand still active and on the other hand it publishes the articles of fryboyter.de on a, my, Mastodon account. And even more surprising, there are people reading my publications there.

For now I will leave the applet / account active. So if you want, you can keep up with my releases via https://social.tchncs.de/@fryboyter.