---
title: Dank Musk ist mir wieder aufgefallen, dass ich Mastodon nutze
date: 2022-11-01T14:31:07+0100
categories:
- OSBN
tags:
- Twitter
- Mastodon
- Musk
slug: dank-musk-ist-mir-wieder-aufgefallen-dass-ich-mastodon-nutze
---
Kürzlich hat Elon Musk, nach einigem hin und her, Twitter endgültig gekauft. Kurz darauf wurden diverse Artikel veröffentlicht, welche Alternativen es gibt. Warum nicht schon vorher? Egal.

Aufgrund der Übernahme von Twitter habe ich mir überlegt, ob ich schon so etwas wie Twitter für fryboyter.de genutzt habe. Spontan lautet die Antwort nein, da ich mit den sogenannten "sozialen Medien" nichts anfangen kann. Daher habe ich beispielsweise kein Konto bei Twitter oder bei Facebook. Aber irgendwie war da mal was.

Nachdem ich etwas überlegt habe, ist mir eingefallen, dass ich irgendwann einmal mit [IFTTT](https://ifttt.com) experimentiert habe um damit die auf fryboyter.de veröffentlichten Artikel auch auf anderen Plattformen zu veröffentlichen. Das ist aber schon lange her.

Wie ich eben festgestellt habe, gibt es das Konto bei IFTTT noch. Und das Applet ist zum einen noch aktiv und zum anderen veröffentlicht es die Artikel von fryboyter.de auf einem, meinen, Mastodon-Konto. Und noch überraschender, es gibt Leute die meine Veröffentlichungen dort lesen.

Vorerst werde ich das Applet / Konto aktiv lassen. Wer also will, kann sich über https://social.tchncs.de/@fryboyter über meine Veröffentlichungen informieren.