---
title: JavaScript - If-Anweisung mit mehreren Bedingungen
date: 2022-02-13T16:13:24+0100
categories:
- OSBN
tags:
- JavaScript
slug: java-script-if-anweisung-mit-mehreren-bedingungen
---
Vor ein paar Tagen habe ich den Artikel [Feedreader mit Filterfunktion](/feedreader-mit-filterfunktion/) veröffentlicht, bei dem es um einen Feedreader geht, der eine auf JavaScript basierende Filterfunktion nutzt. Zwischenzeitlich habe ich mehrere Anfragen erhalten wie man Beiträge mehrerer Autoren entfernen kann.

Mein ursprüngliches Beispiel war Folgendes und hat sich nur auf einen Autor bezogen.

{{< highlight javascript >}}
function filterMessage() {
  if (msg.author == "soeren-hentzschel.at") {
    return MessageObject.Ignore;
  }
}
{{< /highlight >}}

Da ich von JavaScript so gut wie keine Ahnung habe, wollte ich schon vorschlagen, dass man bei Bedarf einfach mehrere Filter anlegt. Wenn man nur wenige Filter insgesamt hat, mag das funktionieren. Mir wäre aber ein Filter lieber der alle Einträge bestimmter Autoren auf einmal ausblendet, damit die Filterliste übersichtlicher bleibt. Die Lösung hierfür ist der Logik-Operator ||.

Will man obiges Beispiel also erweitern würde dieser Code funktionieren:

{{< highlight javascript >}}
function filterMessage() {
  if (msg.author == "soeren-hentzschel.at" || msg.author == "fryboyter.de") {
    return MessageObject.Ignore;
  }
}
{{< /highlight >}}

Der Operator || steht in diesem Fall für "oder". Somit wird abgefragt, ob der Autor soeren-hentzschel.at oder fryboyter.de ist. Und wenn ja, wird der Beitrag entfernt. Will man nun Beiträge eines weiteren Autoren löschen, dann fügt man einfach ein weiteres {{< mark >}}|| msg.author =={{< /mark >}} mit entsprechender Bedingung ein.