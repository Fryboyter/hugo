---
title: Alle Repositories eines Nutzers bei Github klonen
date: 2021-12-28T20:03:28+0100
categories:
- Allgemein
- OSBN
tags:
- Repositories
- Github
slug: alle-repositories-eines-nutzers-bei-github-klonen
---
Manchmal möchte man alle Repositories eines Nutzers bei Github auf den eigenen Rechner klonen. Je nach Anzahl der Repositories kann dies etwas aufwändig sein, jedes manuell mit "git clone" herunterzuladen. Um dies zu automatisieren, kann man folgendes Script nutzen. Nennen wir das Script beispielsweise gitdownload.sh.

{{< highlight bash >}}
#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Usage: $0 <user_name> "
    exit;
fi

USER=$1

for repo in $(curl -s https://api.github.com/users/"$USER"/repos?per_page=1000 |grep git_url |awk '{print $2}'| sed 's/"\(.*\)",/\1/');do
git clone "$repo";
done;
{{< /highlight >}}

Um damit die betreffenden Repositories herunterzuladen führt man einfach ./gitdownload.sh $benutzername aus. Anstelle von $benutzername trägt man den jeweiligen Namen des Nutzers bei Github ein.