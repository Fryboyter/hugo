---
title: Clone all repositories of a user at Github
date: 2022-10-19T18:55:12+0200
categories:
- Englis(c)h
tags:
- Repositories
- Github
slug: clone-all-repositories-of-a-user-at-github
---
Sometimes you want to clone all of a user's repositories on Github onto your own computer. Depending on the number of repositories, this can be a bit time-consuming to download each one manually with "git clone". To automate this you can use the following script. For example, let's call the script gitdownload.sh.

{{< highlight bash >}}
#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Usage: $0 <user_name> "
    exit;
fi

USER=$1

for repo in $(curl -s https://api.github.com/users/"$USER"/repos?per_page=1000 |grep git_url |awk '{print $2}'| sed 's/"\(.*\)",/\1/');do
git clone "$repo";
done;
{{< /highlight >}}

To download the repositories in question, simply run ./gitdownload.sh $username. Instead of $username, enter the name of the user at Github.