---
title: Mercurial - Mehrere Commits zu einem Commit zusammenfügen
date: 2022-07-31T17:16:28+0200
categories:
- OSBN
tags:
- Mercurial
- Histedit
- Commit
slug: mercurial-mehrere-commits-zu-einem-commit-zusammenfuegen
---
Nehmen wir an, dass man in einem lokalen [Mercurial](https://www.mercurial-scm.org)-Repository mehrere kleine Commits erstellt hat. Nun will man nicht jeden dieser Commits pushen, sondern aus mehreren kleinen Commits vorher einen größeren Commit erstellen.

Gehen wir von folgendem fiktiven Beispiel aus.

{{< highlight bash >}}
hg log -v -l 2
Änderung:        408:f5a5d6c7d965
Lesezeichen:     main
Marke:           tip
Nutzer:          Fryboyter <hg@fryboyter.de>
Datum:           Sun Jul 31 15:47:53 2022 +0200
Dateien:         README.md
Zusammenfassung: Fügt einen Text hinzu

Änderung:        407:6d5f6ac48ddb
Nutzer:          Fryboyter <hg@fryboyter.de>
Datum:           Sun Jul 31 15:47:31 2022 +0200
Dateien:         README.md
Zusammenfassung: Fügt eine Überschrift hinzu
{{< /highlight >}}

Wie man sieht, beziehen sich die letzten beiden Commits auf einfache Änderungen in der gleichen Datei, für die ein Commit gereicht hätte. Also machen wir doch einfach aus den zwei Commits einen einzelnen Commit.

Hierfür habe ich mich für [histedit](https://www.mercurial-scm.org/wiki/HisteditExtension) entschieden. Hierbei handelt es sich nicht um ein externes Plugin wie [hg-git](https://foss.heptapod.net/mercurial/hg-git), sondern um eine Funktion die Mercurial von Haus aus bietet. Diese muss man allerdings erst aktivieren. Hierfür reicht es aus in der Konfigurationsdatei hgrc unter [extensions] histedit = einzutragen. Also beispielsweise wie folgt.

{{< highlight bash >}}
[extensions]
hggit = ~/repository/hg-git/hggit
histedit =
{{< /highlight >}}

Will man nun beispielsweise die beiden letzten Commits zusammenfügen, führt man den Befehl {{< mark >}}hg histedit -r -2{{< /mark >}}. Nun sollte man eine Anzeige, ähnlich der folgenden erhalten.

{{< highlight bash >}}
?: help, k/up: move up, j/down: move down, space: select, v: view patch   
d: drop, e: edit, f: fold, m: mess, p: pick, r: roll
pgup/K: move patch up, pgdn/J: move patch down, c: commit, q: abort
Older commits are shown above newer commits.
  #0  pick   407:6d5f6ac48ddb   407 Fügt eine Überschrift hinzu
  #1  pick   408:f5a5d6c7d965   408 Fügt einen Text hinzu

changeset: 408:f5a5d6c7d965
user:      Fryboyter <hg@fryboyter.de>
bookmark:  main
summary:   Fügt einen Text hinzu
files:     README.md
no overlap
{{< /highlight >}}

Wir möchten nun, dass Commit #1 Teil von Commit #0 wird, sodass wir diesen pushen können. Hierfür wählen wir den Commit #1 aus und drücken auf der Tastatur f. In der betreffenden Zeile sollte anstelle von pick nun ^fold angezeigt werden. Abschließend drücken wir auf der Tastatur noch c. Nun ändert man die Commit-Nachricht ab und speichert sie.

Führt man nun hg log -v -r 2 aus, sollte der Commit #1 nicht mehr angezeigt werden und beim Commit #0 sollte die neue Commit-Nachricht angezeigt werden. Führt man {{< mark >}}hg diff -c tip{{< /mark >}} aus, sollte der Commit auch beide Änderungen enthalten. Bei "tip" handelt es sich übrigens um den letzten vorhandenen Commit.