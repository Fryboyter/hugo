---
title: Automatically update the Pkgbuild file and install it
date: 2022-07-07T20:25:29+0200
categories:
- Linux
- Englis(c)h
tags:
- Arch Linux
- PKGBUILD
slug: automatically-update-the-pkgbuild-file-and-install-it
---
Under Arch I have installed a few packages whose updates are sometimes offered with a time delay. For example, because the respective package maintainer does not have the necessary time or because he wants to wait for the first minor release. Hugo is often such a package.

Therefore I often install the current version myself. For this I created the directory ~/pkgbuilds/hugo/ and stored the PKGBUILD file in this directory to install the package. In the case of Hugo, this currently looks like this.

{{< highlight bash >}}
pkgname=hugo
pkgver=0.101.0
pkgrel=1
pkgdesc="Fast and Flexible Static Site Generator in Go"
arch=('x86_64')
url="https://gohugo.io/"
license=('Apache')
depends=('glibc')
makedepends=('go' 'git')
optdepends=('python-pygments: syntax-highlight code snippets'
            'python-docutils: reStructuredText support')
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/gohugoio/${pkgname}/archive/v${pkgver}.tar.gz)
sha512sums=('541d0e04e868845119f2b488fd53b92929ea4dc08685d438a2914b41586e204588b193522013e8eed908dc0c3fbc2714aefb1afad0beae875d57d71aadc59c70')

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw"
  go build -tags extended

  ./hugo gen man
  ./hugo completion bash > ${pkgname}.bash-completion
  ./hugo completion fish > ${pkgname}.fish
  ./hugo completion zsh > ${pkgname}.zsh
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  install -Dm755 "${pkgname}" "${pkgdir}"/usr/bin/${pkgname}
  install -Dm644 LICENSE "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE

  install -Dm644 "${srcdir}"/${pkgname}-${pkgver}/man/*.1  -t "${pkgdir}"/usr/share/man/man1/
  
  install -Dm644 ${pkgname}.bash-completion "${pkgdir}"/usr/share/bash-completion/completions/${pkgname}
  install -Dm644 ${pkgname}.fish "${pkgdir}"/usr/share/fish/vendor_completions.d/${pkgname}.fish
  install -Dm644 ${pkgname}.zsh "${pkgdir}"/usr/share/zsh/site-functions/_${pkgname}
}
{{< /highlight >}}

If a new version was published I enter the new version in the PKGBUILD file at the line pkgver=. Then I execute the commands {{< mark >}}updpkgsums PKGBUILD{{< /mark >}}, {{< mark >}}makepkg -cirs PKGBUILD -noconfirm{{< /mark >}} and {{< mark >}}rm -- *.tar.*{{< /mark >}} in the directory where the file is located.

The first command downloads the archive file containing the source code, creates the checksum of the file, and adds it to the PKGBUILD file. The second command uses the instructions in the PKGBUILD file to build and install the package. The last command deletes both the archive file with the source code and the created package.

Because I am already quite skilled at this, this doesn't take a minute. However, I still want to automate the process. Therefore I have created a function for myself.

{{< highlight bash >}}
updpkgbuild () {
	new_ver="$1"
	sed -E "s#(pkgver=).*#\1$new_ver#" -i PKGBUILD
	updpkgsums PKGBUILD
	makepkg -cirs PKGBUILD --noconfirm
	rm -- *.tar.*
}
{{< /highlight >}}

With this I only need to run for example {{< mark >}}updpkgbuild 0.102.0{{< /mark >}} in the directory of the PKGBUILD file and version 0.102.0 of the package will be installed automatically. Of course, the whole process only works if only the version as well as the checksum needs to be updated. But this is mostly the case.

I created this function for the zsh. Whether this also works in other shells like bash or fish, I can't say.