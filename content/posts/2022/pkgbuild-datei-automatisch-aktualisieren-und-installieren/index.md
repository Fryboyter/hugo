---
title: Pkgbuild-Datei automatisch aktualisieren und installieren
date: 2022-07-07T20:25:29+0200
categories:
- OSBN
- Linux
tags:
- Arch Linux
- PKGBUILD
slug: pkgbuild-datei-automatisch-aktualisieren-und-installieren
---
Unter Arch habe ich ein paar Pakete installiert, deren Updates teilweise zeitverzögert angeboten werden. Zum Beispiel, weil der jeweilige Paketbetreuer nicht die nötige Zeit hat oder weil er das erste Minor-Release abwarten will. Hugo ist oft solch ein Paket.

Daher installiere ich mir die aktuelle Version oft selbst. Hierfür habe ich das Verzeichnis ~/pkgbuilds/hugo/ erstellt und in diesem die PKGBUILD-Datei gespeichert anhand der darin enthaltenen Anweisungen das Paket installiert wird. Im Falle von Hugo sieht diese aktuell folgendermaßen aus.

{{< highlight bash >}}
pkgname=hugo
pkgver=0.101.0
pkgrel=1
pkgdesc="Fast and Flexible Static Site Generator in Go"
arch=('x86_64')
url="https://gohugo.io/"
license=('Apache')
depends=('glibc')
makedepends=('go' 'git')
optdepends=('python-pygments: syntax-highlight code snippets'
            'python-docutils: reStructuredText support')
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/gohugoio/${pkgname}/archive/v${pkgver}.tar.gz)
sha512sums=('541d0e04e868845119f2b488fd53b92929ea4dc08685d438a2914b41586e204588b193522013e8eed908dc0c3fbc2714aefb1afad0beae875d57d71aadc59c70')

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw"
  go build -tags extended

  ./hugo gen man
  ./hugo completion bash > ${pkgname}.bash-completion
  ./hugo completion fish > ${pkgname}.fish
  ./hugo completion zsh > ${pkgname}.zsh
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  install -Dm755 "${pkgname}" "${pkgdir}"/usr/bin/${pkgname}
  install -Dm644 LICENSE "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE

  install -Dm644 "${srcdir}"/${pkgname}-${pkgver}/man/*.1  -t "${pkgdir}"/usr/share/man/man1/
  
  install -Dm644 ${pkgname}.bash-completion "${pkgdir}"/usr/share/bash-completion/completions/${pkgname}
  install -Dm644 ${pkgname}.fish "${pkgdir}"/usr/share/fish/vendor_completions.d/${pkgname}.fish
  install -Dm644 ${pkgname}.zsh "${pkgdir}"/usr/share/zsh/site-functions/_${pkgname}
}
{{< /highlight >}}

Wurde nun eine neue Version veröffentlicht, trage ich in der PKGBUILD-Datei in der Zeile pkgver= die neue Version ein. Anschließend führe ich im Verzeichnis, in dem die Datei liegt, die Befehle {{< mark >}}updpkgsums PKGBUILD{{< /mark >}}, {{< mark >}}makepkg -cirs PKGBUILD –noconfirm{{< /mark >}} und {{< mark >}}rm -- *.tar.*{{< /mark >}} aus.

Der erste Befehl läd die Archivdatei mit dem Sourcecode herunter, erstellt die Prüfsumme der Datei und trägt diese in der PKGBUILD-Datei ein. Der zweite Befehl erstellt anhand der Anweisungen in der PKGBUILD-Datei das Paket und installiert es. Der letzte Befehl löscht sowohl die Archivdatei mit dem Sourcecode sowie das erstellte Paket.

Da ich darin schon ziemlich geübt bin, dauert dies keine Minute. Ich möchte den Vorgang aber trotzdem automatisieren. Daher habe ich mir eine Funktion erstellt.

{{< highlight bash >}}
updpkgbuild () {
	new_ver="$1"
	sed -E "s#(pkgver=).*#\1$new_ver#" -i PKGBUILD
	updpkgsums PKGBUILD
	makepkg -cirs PKGBUILD --noconfirm
	rm -- *.tar.*
}
{{< /highlight >}}

Mit dieser brauche ich im Verzeichnis der PKGBUILD-Datei nur beispielsweise {{< mark >}}updpkgbuild 0.102.0{{< /mark >}} ausführen und Version 0.102.0 des Pakets wird automatisch installiert. Das ganze klappt natürlich nur, wenn nur die Version sowie die Prüfsumme geändert werden muss. Was aber in den meisten Fällen zutrifft.

Die Funktion habe ich für die zsh erstellt. Ob diese auch in anderen Shells wie der bash oder fish funktioniert, kann ich nicht sagen.