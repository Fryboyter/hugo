---
title: Alternative zu IFTTT
date: 2022-12-02T17:24:38+0100
categories:
- OSBN
tags:
- ITTT
- Automatisierung
slug: alternative-zu-ittt
---
Bei [IFTTT](https://ifttt.com) (If This Then That) handelt es sich um einen Dienst, mit dem sich diverse Dinge automatisieren lassen. In meinem Fall wird geprüft, ob über den RSS-Feed ein neuer Artikel veröffentlicht wurde. Wenn ja, dann wird bei Mastodon einen Beitrag mit der Überschrift und einem Link auf den Artikel erstellt. Hierzu hatte ich vor ein paar Wochen erst einen [Artikel](/dank-musk-ist-mir-wieder-aufgefallen-dass-ich-mastodon-nutze/) veröffentlicht.

Technisch gesehen funktioniert es ohne Probleme. Allerdings habe ich mir trotzdem Gedanken gemacht, da ich zwei mögliche Probleme sehe.

Zum einen läuft das Ganze über Server Dritter. In dem Fall ist es kein Problem, da nur der frei zugängliche RSS-Feed abgefragt wird, der wiederum nur den Blödsinn enthält, den ich veröffentliche. Aber trotzdem...

Was ich problematischer finde ist, dass ich das kostenlose Angebot von IFTTT nutze. Dies könnte eines Tages eingestellt werden. Prinzipiell bin ich bereit für einen Dienst zu bezahlen. Allerdings nicht in diesem Fall, weil ich auch gut ohne Mastodon leben kann.

Daher habe ich mir mal rein aus Interesse mögliche Alternativen angesehen. Im Grunde sind das alles ähnliche Dienste, die von Dritten angeboten werden. Und deren Funktionsumfang oft geringer, und die Nutzung schwieriger ist.

Als Alternative die man selbst hosten kann, habe ich [Huginn](https://github.com/huginn/huginn) gefunden. Das Tool begeistert mich auf der einen Seite, schreckt mich allerdings auf der anderen Seite ab.

Huginn bietet von Haus aus viele Möglichkeiten. Zum Beispiel, dass man automatisch informiert wird, wenn bei einem Wetterbericht ein regnerisches Wetter gemeldet wird. Oder eben, dass über meinen RSS-Feed ein neuer Artikel bei Mastodon veröffentlicht wird.

Allerdings ist die Einrichtung nicht so einfach zu erledigen wie bei IFTTT. Vor allem, wenn man eigene Agents anstelle der fertigen nutzen will. Huginn erfordert also Einarbeitszeit. Und die [Dokumentation](https://github.com/huginn/huginn/wiki) könnte besser sein.

Daher ist das ganze eher kein Wochenendprojekt für mich, sondern eher eines für den Jahresendurlaub. Daher gebe ich an der Stelle auch kein Beispiel, was man wie mit Huginn alles machen kann.