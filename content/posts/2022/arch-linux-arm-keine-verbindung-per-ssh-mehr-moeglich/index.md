---
title: Arch Linux ARM - Keine Verbindung per SSH mehr möglich
date: 2022-11-25T15:13:28+0100
categories:
- OSBN
tags:
- Raspberry Pi
- Arch Linux ARM
- SSH
slug: arch-linux-arm-keine-verbindung-per-ssh-mehr-moeglich
---
In meinem privaten LAN ist ein Raspberry Pi 4 vorhanden der neben ein paar anderen Dingen eine Kombination von [PiHole](https://pi-hole.net) und [unbound](https://www.nlnetlabs.nl/projects/unbound/about/) zur Verfügung stellt. Vor ein paar Tagen hat die Namensauflösung auf den Rechnern im LAN nicht mehr funktioniert.

Als ich mit SSH mit dem Raspberry Pi verbinden wollte, konnte keine Verbindung aufgebaut werden. Sehr seltsam...

Meine erste Vermutung war, dass die Speicherkarte defekt ist, da ich diese schon einige Jahre nutze und auch schon ein viele Schreibvorgänge stattgefunden haben. Also habe ich die Karte aus dem Raspberry Pi entfernt und mittels eines Kartenlesers an mein Notebook angeschlossen. Sofort wurden die beiden Partitionen auf der Speicherkarte erkannt, ich konnte sie mounten und auf die Dateien zugreifen. Also ist es scheinbar doch nicht die Speicherkarte.

Trotzdem habe ich erst mal ein Image der Karte erstellt und dieses dann mittels [PiShrink](/raspberry-pi-images-schrumpfen/) auf eine annehmbare Größe verkleinert.

Vielleicht steht ja in den Log-Dateien etwas, dass auf das Problem hinweist. Da am Raspberry Pi weder ein Monitor noch eine Tastatur angeschlossen ist, habe ich die größere der beiden Partitionen gemountet. Nehmen wir an im Home-Verzeichnis des Notebooks im Verzeichnis rasp. Dann habe ich mittels {{< mark >}}journalctl -D ~/rasp/var/log/journal -r{{< /mark >}} auf die Log-Dateien auf der Speicherkarte zugegriffen. Schon nach wenigen Zeilen habe ich die erste und einzige Fehlermeldung gefunden.

{{< highlight bash >}}
alarmpi systemd[1]: Failed to start Wait for Network to be Configured.
alarmpi systemd[1]: systemd-networkd-wait-online.service: Failed with result 'exit-code'.
alarmpi systemd[1]: systemd-networkd-wait-online.service: Main process exited, code=exited, status=1/FAILURE
alarmpi systemd-networkd-wait-online[322]: Timeout occurred while waiting for network connectivity.
{{< /highlight >}}

Der Raspberry Pi bootet also, kann aber keine Netzwerkverbindung aufbauen. Aber warum? Die Konfigurationen habe ich seit mindestens einem Jahr nicht geändert.

Trotzdem habe ich mir die Konfigurationsdatei eth0.network unter ~/rasp/etc/systemd/network/ angesehen.

Sieht gut aus. 

{{< highlight bash >}}
[Match]
Name=eth0
...
{{< /highlight >}}

Wobei... Moment! Eth0 bei einer Distribution die systemd und somit [Predictable Network Interface Names](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/) nutzt? Könnte das die Ursache sein?

Einen Versuch ist es wert. Also habe ich Name=eth0 in Name=en* geändert, da der Netzwerkanschluss per Kabel immer eine Bezeichnung zugeteilt bekommt die mit en beginnt. Welche genau, kann ich nicht sagen, da ich ja keinen Zugriff im laufenden Betrieb habe.

Und siehe da, der Raspberry Pi bootet und eine Verbindung per SSH ist wieder möglich. Mittels {{< mark >}}ip addr{{< /mark >}} habe ich mir dann die Netzwerkverbindungen des Raspberry Pi anzeigen lassen und festgestellt, dass der Netzwerkanschluss per Kabel die Bezeichnung end0 hat. Also habe ich abschließend Name=en* noch in Name=end0 geändert. Wobei das ist in dem Fall nicht nötig ist, da nicht zwei unterschiedliche Netzwerkanschlüsse mit Kabel vorhanden sind. Aber sicher ist sicher.

Aber warum ist das erst jetzt passiert? Predictable Network Interface Names gibt es unter systemd schon ewig und sind in der Standardkonfiguration aktiv. Nachdem ich im offiziellen Forum von Arch Linux ARM gesucht habe, habe ich mehrere Fälle mit dem gleichen Problem gefunden. Z. B. https://archlinuxarm.org/forum/viewtopic.php?f=15&t=16245. Zumindest bin ich nicht alleine. Ich vermute, dass durch ein Update Predictable Network Interface Names aktiviert wurden und es deshalb zu Problemen gekommen ist, weil meine Installation schon sehr alt ist. Aber das ist eine Vermutung.