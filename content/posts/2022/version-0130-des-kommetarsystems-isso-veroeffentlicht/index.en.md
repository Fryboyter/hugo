---
title: Version 0.13.0 of the Isso comment system released
date: 2022-06-13T19:19:06+0200
categories:
- Englis(c)h 
tags:
- Isso
- Comment system
- Python
slug: version-0-13-0-of-the-isso-comment-system-released
---
Isso is a Python-based commenting system that you can host yourself, so that users' comments are not stored on third-party servers.

Isso had two problems for a long time. First, development was a bit slow at times. And second, which was the bigger problem, only one person could publish new versions. And this person had, and probably still has, a lot to do in real life. 

This has changed since version 0.12.6. New developers are participating and the necessary rights to release new versions are available. So a little less than 24 hours ago version 0.13.0 was released. The changes are quite extensive and can be read at [https://isso-comments.de/news/#isso-version-0-13-0-released](https://isso-comments.de/news/#isso-version-0-13-0-released).

Furthermore Isso is now accessible via [https://isso-comments.de](https://isso-comments.de) and shall be continued as a community project.

I have already updated the Isso instance I use on fryboyter.de to version 0.13.0. I only had to update the CSS file afterwards, because it was fundamentally overhauled by the Isso developers.