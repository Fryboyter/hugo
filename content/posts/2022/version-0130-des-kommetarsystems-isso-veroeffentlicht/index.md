---
title: Version 0.13.0 des Kommetarsystems Isso veröffentlicht
date: 2022-06-13T19:19:06+0200
categories:
- OSBN 
tags:
- Isso
- Kommetarsystems
- Python
slug: version-0-13-des-kommetarsystems-isso-veroeffentlicht
---
Bei Isso handelt es ich um ein auf Python basierendes Kommentarsystem, dass man selbst hosten kann, sodass die Kommentare der Nutzer nicht auf Server Dritter gespeichert werden.

Isso hatte lange Zeit zwei Probleme. Zum einen verlief die Entwicklung teilweise etwas schleppend. Und zum anderen, was das größere Problem war, konnte nur eine Person neue Versionen veröffentlichen. Und diese hatte, und hat vermutlich weiterhin, im echten Leben sehr viel zu tun. 

Das hat sich seit Version 0.12.6 geändert. Neue Entwickler beteiligen sich und die nötigen Rechte um neue Versionen zu veröffentlichen sind vorhanden. Daher wurde vor etwas weniger als 24 Stunden die Version 0.13.0 veröffentlicht. Die Änderungen sind ziemlich umfangreich und können unter [https://isso-comments.de/news/#isso-version-0-13-0-released](https://isso-comments.de/news/#isso-version-0-13-0-released) nachgelesen werden.

Weiterhin ist Isso nun über [https://isso-comments.de](https://isso-comments.de) erreichbar und soll als Gemeinschaftsprojekt weitergeführt werden.

Ich habe die Isso-Instanz, die ich auf fryboyter.de nutze, bereits auf Version 0.13.0 aktualisiert. Ich musste lediglich nachträglich die CSS-Datei aktualisieren, da diese grundlegend von den Isso-Entwicklern überholt wurde.