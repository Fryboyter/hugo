---
title: Es ist auch möglich Arch Linux zu nutzen wenn man ein Leben hat
date: 2022-07-08T17:21:21+0200
categories:
- OSBN
- Linux
tags:
- Arch Linux
slug: es-ist-auch-moeglich-arch-linux-zu-nutzen-wenn-man-ein-leben-hat
---
Gestern wurde auf gnulinux.ch ein Artikel bezüglich [OS Entscheidungsbäume](https://gnulinux.ch/os-entscheidungsbaeume) veröffentlicht. Dieser ist, hoffentlich, nicht ganz ernst gemeint. Trotzdem möchte ich bezüglich Arch Linux meinen Senft (ja in Franken ist es Senft nicht Senf) abgeben.

Um Arch Linux ranken sich viel Mythen. Und somit auch viel Bullshit. Wie zum Beispiel, dass man nur etwas lernt, wenn man beispielsweise Arch Linux nutzt. Ja das ist Bullshit. Ein Großteil meiner Kenntnisse zum Thema Linux habe ich mir beispielsweise unter Mandrake / Mandriva angeeignet. Was damals sozusagen das Ubuntu dieser Zeit war. Seit ich Arch nutze, ist natürlich einiges hinzugekommen. Aber nicht, weil ich Arch nutze, sondern weil ich eben bestimmte Probleme lösen oder bestimmte Aufgaben erfüllen musste. Aber egal, darum geht es nicht.

Es geht darum, dass viele behaupten, dass Arch Linux von Leuten genutzt wird die kein echtes Leben haben. Zum Beispiel, weil man nach fast jedem Update etwas reparieren muss. Bullshit! 

Ich nutze Arch Linux seit ca. 2010 auf mehreren Rechnern mit unterschiedlicher Konfiguration. Sowohl was die Hard- als auch die Software betrifft. Und ich kann beim besten Willen nicht sagen, wann es das letzte Mal wegen eines Updates Probleme gab. Ich nutze Arch Linux sogar für Server im privaten Bereich.

Ich installiere nicht mehrmals täglich irgendwelche Updates. Bei manchen Rechnern installiere ich Updates sogar nur einmal die Woche, da ich diese nur am Wochenende nutze.

Vor einem Update prüfe ich, ob etwas unter https://archlinux.org/news/ veröffentlicht wurde, was meine Installationen betrifft. Um dies zu automatisieren, nutze ich [Informant](https://aur.archlinux.org/packages/informant). Wenn es der Fall sein sollte, berücksichtige ich dies. Ohne Wenn und Aber.

Und von Zeit zu Zeit gleiche ich meine Konfigurationsdateien mit den Pacnew-Dateien ab [https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave](https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave). Zudem leere ich den Cache von pacman regelmäßig automatisch mittels eines Hooks [https://wiki.archlinux.org/title/pacman#Cleaning_the_package_cache](https://wiki.archlinux.org/title/pacman#Cleaning_the_package_cache).

Ansonsten nutze ich Arch Linux einfach. Ja, ernsthaft. Arch Linux ist im Grunde genommen eine ganz normale Distribution. Wie beispielsweise OpenSuse. Es ist kein Betriebssystem für die Elite. Oder für Leute ohne echtes Leben. Es ist nur eine Distribution bei der manche Dinge anders funktionieren als bei anderen Distributionen. Wie beispielsweise die Installation.

Dieser Artikel soll nun kein Aufruf sein, dass möglichst viele Nutzer Arch Linux installieren. Nein. Jeder soll die Distribution nutzen die er / sie / es für richtig hält. Dieser Artikel soll nur dazu dienen, Arch Linux zu entmystifizieren und somit potenzielle Nutzer zu informieren. Mir persönlich ist es total egal welche Distributionen andere Nutzer einsetzen. Und ich habe mit dem Artikel von gnulinux.ch daher auch kein Problem.