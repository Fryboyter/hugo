---
title: It is also possible to use Arch Linux if you have a life
date: 2022-07-08T17:21:21+0200
categories:
- Linux
- Englis(c)h
tags:
- Arch Linux
slug: it-is-also-possible-to-use-arch-linux-if-you-have-a-life
---
Yesterday, an article was published on gnulinux.ch regarding [OS decision trees](https://gnulinux.ch/os-entscheidungsbaeume). This is, hopefully, not meant to be completely serious. Nevertheless, I would like to give my Senft (yes, in francs it is Senft not Senf) regarding Arch Linux.

There are many myths about Arch Linux. And therefore also a lot of bullshit. For example, that you only learn something if you use Arch Linux. Yes, that is bullshit. I acquired a large part of my knowledge about Linux under Mandrake / Mandriva, for example. Which was the Ubuntu of that time, so to speak. Since I've been using Arch, a lot has been added, of course. But not because I use Arch, but because I had to solve certain problems or fulfil certain tasks. But anyway, that's not the point.

The point is that many claim that Arch Linux is used by people who have no real life. For example, because you have to fix something after almost every update. Bullshit! 

I have been using Arch Linux since about 2010 on several computers with different configurations. Both in terms of hardware and software. And for the life of me I can't say when the last time there were problems because of an update. I even use Arch Linux for servers in the private sector.

I don't install updates several times a day. On some computers I even install updates only once a week because I only use them on weekends.

Before an update, I check whether something has been published at https://archlinux.org/news/ that affects my installations. To automate this, I use [Informant](https://aur.archlinux.org/packages/informant). If this is the case, I take it into account. Without ifs and buts.

And from time to time I synchronise my configuration files with the Pacnew files ([https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave](https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave)). I also regularly clear pacman's cache automatically using a hook ([https://wiki.archlinux.org/title/pacman#Cleaning_the_package_cache](https://wiki.archlinux.org/title/pacman#Cleaning_the_package_cache)).

Otherwise I just use Arch Linux. Yes, seriously. Arch Linux is basically a normal distribution. Like OpenSuse, for example. It's not an operating system for the elite. Or for people without a real life. It's just a distribution where some things work differently than other distributions. Like the installation, for example.

This article is not meant to encourage as many users as possible to install Arch Linux. No. Everyone should use the distribution he / she / it thinks is right. This article is only meant to demystify Arch Linux and thus inform potential users. Personally, I couldn't care less which distributions other users use. And I therefore have no problem with the article from gnulinux.ch.