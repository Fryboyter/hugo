---
title: Shrink Raspberry Pi Images
date: 2022-10-19T19:09:44+0200
categories:
- Linux
- Englis(c)h
tags:
- Raspberry Pi
- Image
slug: shrink-raspberry-pi-images
---
Let's say you have a Raspberry Pi fully set up on a 120GB SD card and you want to create an image to install it on an other Raspberry Pi. Most users will use commands like {{< mark >}}dd if=/dev/mmcblk0 of=/home/username/raspberry.img bs=4M{{< /mark >}} to do this. This is simple. However, it has a disadvantage. The image file is 120 GB no matter how much space is actually used. Even though storage space is no longer very expensive, it is wasted in this case. So you would have to shrink the image. This can be done manually. Or you can use [PiShrink](https://github.com/Drewsif/PiShrink).

With this, image files created with dd can be shrunk to the size actually used. In my case, the original 120 GB image had a size of only 13 GB. The simple command {{< mark >}}pishrink.sh rasberry.img{{< /mark >}} is sufficient for this.

If you now use dd to write the contents of the shrunk image to another SD card and boot from it, the partitions automatically adjust to the size of the SD card so that the entire storage space is available. PiShrink includes a corresponding command in the image file for this purpose.