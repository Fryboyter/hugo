---
title: Aktiven Fork bei Github finden
date: 2022-10-06T14:34:09+0200
categories:
- OSBN
tags:
- Github
- Fork
slug: aktiven-fork-bei-github-finden
---
Ab und an nutze ich sshfs ganz gerne um ein Verzeichnis über SSH lokal zu mounten. Die Entwicklung des Projekts wurde allerdings vor ein paar Monaten eingestellt. Heute wollte ich nachschauen, ob es einen aktiv weiterentwickelten Fork von sshfs gibt.

Was leider, zumindest direkt über github.com, nicht so einfach ist. Über https://github.com/libfuse/sshfs/network/members kann man sich zwar die Forks ansehen aber diese sind alphabetisch sortiert. Da es von sshfs derzeit 403 Forks bei Github gibt, könnte es etwas dauern bis man den aktuellsten Fork findet. Oder man nutzt https://techgaun.github.io/active-forks/index.html. Dort braucht man nur die Adresse des Projekts eingeben (z. B. https://github.com/libfuse/sshfs) und erhält eine Liste der Forks, die nach der Aktivität sortiert ist. Über die Schaltfläche "Add Condition" kann man die Suchabfrage noch nach seinen Wünschen anpassen.

Besser wäre es natürlich, wenn man die Liste direkt bei Github sortieren könnte. Allerdings ist in der Hinsicht Codeberg.org, was auf Gitea basiert, auch nicht besser und bietet ebenfalls nur die alphabetisch sortiere Liste an.