---
title: Find active fork on Github
date: 2022-10-06T14:34:09+0200
categories:
- Englis(c)h
tags:
- Github
- Fork
slug: find-active-fork-on-github
---
From time to time I like to use sshfs to mount a directory locally via SSH. However, the development of the project was stopped a few months ago. Today I wanted to see if there is an actively developed fork of sshfs.

Unfortunately, this is not so easy, at least directly via github.com. Via https://github.com/libfuse/sshfs/network/members you can view the forks but they are sorted alphabetically. Because there are currently 403 forks of sshfs on github, it could take a while to find the most current fork. Or you can use https://techgaun.github.io/active-forks/index.html. There you only have to enter the address of the project (e.g. https://github.com/libfuse/sshfs) and you will get a list of forks sorted by activity. With the button "Add Condition" you can customize the search query according to your needs.

Of course, it would be better if you could sort the list directly at Github. However, Codeberg.org, which is based on Gitea, is no better in this respect and also only offers the alphabetically sorted list.