---
title: Inhalt am Ende des Front-Matter-Bereichs einfügen
date: 2022-11-02T21:20:10+0100
categories:
- OSBN
tags:
- Front Matter
- Markdown
- Sed
slug: inhalt-am-ende-des-front-matter-bereichs-einfuegen
---
Wenn ich einen Artikel für fryboyter.de schreibe, erstelle ich hierfür eine Markdown-Datei, an deren Beginn ein Front-Matter-Bereich vorhanden ist. Dieser sieht beispielsweise wie folgt aus.

{{< highlight bash >}}
---
title: Inhalt am Ende des Front-Matter-Bereichs einfügen
date: 2022-11-02T20:21:11+0100
categories:
- OSBN
tags:
- Front Matter
- Markdown
- Sed
slug: inhalt-am-ende-des-front-matter-bereichs-einfuegen
---
{{< /highlight >}}

In manchen Fällen will man diesen Bereich nachträglich erweitern. Zum Beispiel mit nositemap: true damit der Artikel nicht in der Sitemap der Internetseite erscheint. Bei einem einzelnen Artikel ist das schnell erledigt, indem man die Datei manuell ändert. Was, aber wenn es nicht einen, sondern mehrere Dateien betrifft? Dann wäre es besser, wenn man die Änderungen automatisieren würde. Allerdings besteht der besagte Bereich nicht immer aus der gleichen Anzahl von Zeilen. Zum Beispiel, weil ich mal mehr, mal weniger Tags verwende. Schlussendlich bin ich zu folgendem Ergebnis gekommen.

{{< highlight bash >}}
for file in $(find . -type f -name "*.md");
do
lines=$( sed -n '/^---$/=' $file | sed -n 2p )
sed -i -e "$lines i nositemap: true" $file
done
{{< /highlight >}}

Die erste Zeile sucht im aktuellen Verzeichnis und allen darin vorhandenen Unterverzeichnissen nach Dateien mit der Endung .md. Die dritte Zeile prüft aus wie vielen Zeilen der jeweilige Front-Matter-Bereich besteht. Die vierte Zeile erweitert den Bereich am Ende abschließend um {{< mark >}}nositemap: true{{< /mark >}}, sodass dieser wie folgt aussieht.

{{< highlight bash >}}
---
title: Inhalt am Ende des Front-Matter-Bereichs einfügen
date: 2022-11-02T20:21:11+0100
categories:
- OSBN
tags:
- Front Matter
- Markdown
- Sed
slug: inhalt-am-ende-des-front-matter-bereichs-einfuegen
nositemap: true
---
{{< /highlight >}}

Wer das Script erst einmal testen will, ohne dass die Dateien gleich direkt geändert wird, sollte in Zeile 4 den Paramter -i entfernen. Dann werden die geänderten Dateien nur angezeigt ohne, dass sie tatsächlich geändert werden.