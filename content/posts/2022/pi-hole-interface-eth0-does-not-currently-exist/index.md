---
title: Pi-hole - Interface eth0 does not currently exist
date: 2022-01-08T10:57:37+0100
categories:
- OSBN
- Linux
tags:
- Pi-hole
- Netzwerk
slug: pi-hole-interface-eth0-does-not-currently-exist
---
Seit einiger Zeit erhält man als Nutzer von Pi-hole unter Umständen die Fehlermeldung {{< mark >}}interface eth0 does not currently exist{{< /mark >}} in der grafischen Oberfläche.

Der Grund hierfür ist in der Regel, dass Pi-hole schneller startet als die Netzwerkverbindung aufgebaut wird. Auf die Funktion hat dies keinen Einfluss, auch noch später getestet wird, ob eine Netzwerkverbindung vorhanden ist. Irgendwie nervt es aber trotzdem, da diese Meldung anhand eines hüpfenden Symbols angezeigt wird. Löscht man die Meldung, verschwindet es zwar, aber nach einem Neustart des Raspberry Pi geht es wieder von vorne los.

Die Lösung diese (kosmetischen) Problems ist ziemlich einfach. Man trägt einfach am Anfang der Datei /etc/pihole/pihole-FTL.conf {{< mark >}}DELAY_STARTUP=5{{< /mark >}} ein. Damit wird der Start um 5 Sekunden verzögert, sodass zwischenzeitlich die Netzwerkverbindung aufgebaut werden kann. 

Je nach Konfiguration und verwendeten Raspberry Pi kann man eventuell einen niedrigeren Wert eintragen. Mit 5 Sekunden sollte man aber immer auf der sicheren Seite sein und es dürfte zu verschmerzen sein, wenn Pi-Hole etwas später zur Verfügung steht. Zumal man in der Regel ja nicht täglich mehrere Neustarts durchführt.