---
title: Hugo - Tote Links mit der Wayback Machine wiederbeleben
date: 2022-12-23T15:48:31+0100
categories:
- OSBN
tags:
- Hugo
- Shortcode
slug: hugo-tote-links-mit-der-wayback-machine-wiederbeleben
---
Seit 2009 veröffentliche ich nun schon diverse Artikel. Bei einigen sind inzwischen die Links auf andere Internetseiten nicht mehr aufrufbar, weil die Betreiber der jeweiligen Seite entweder den Artikel gelöscht haben oder weil es die Seite gar nicht mehr gibt. Daher will ich in den nächsten Tagen etwas aufräumen, ohne hierbei viele Artikel komplett zu löschen.

Ich habe mir daher überlegt, ob es wohl machbar ist, dass ich so viele Links wie möglich durch eine, beim [Internet Archive](https://archive.org) gespeicherte, Version ersetzen kann. Machbar ja, aber manuell ist das ziemlich aufwändig.

Nehmen wir als Beispiel den Artikel [https://fryboyter.de/microsoft-eula-fuer-englischsprachige-dummies/](/microsoft-eula-fuer-englischsprachige-dummies/) aus dem Jahre 2009. Der darin genannte Link auf dailycupoftech.com funktioniert nicht mehr. Händisch würde ich nun den Link unter [https://web.archive.org](https://web.archive.org) eingeben und mir eine der gespeicherten Versionen heraussuchen (z. B. https://web.archive.org/web/20080609041611/http://www.dailycupoftech.com/2008/05/21/ms-eula-in-plain-english/) und diesen beim Artikel anstelle des ursprünglichen Links eintragen.

Das hat zwei Nachteile. Das Heraussuchen einer Version benötigt mehr Zeit. Und ich muss aufpassen, dass ich eine Version erwische, die möglichst nahe am Veröffentlichungsdatum meines Artikels liegt. Was wiederum mehr Zeit benötigt.

Also kommt das nicht infrage. Was also machen? Wenn man [Hugo](https://gohugo.io) zum Erzeugen der Internetseite nutzt, dann wäre eine Lösung ein sogenannter [Shortcode](https://gohugo.io/content-management/shortcodes/). Dieser könnte wie folgt aussehen. 

{{< highlight go-template >}}
<a
	href="https://web.archive.org/web/{{ $.Page.Params.PublishDate.Format "20060102150405" }}/{{ .Get 0 }}"
	class="archive"
>
	{{ .Inner }}
	<img src="{{ "images/wayback.svg" | relURL }}" alt="" />
</a>
{{< /highlight >}}

Und hier dazu gehörenden CSS-Anweisungen.

{{< highlight css >}}
.archive img {
&#x9;vertical-align: super;
&#x9;height: 12px;
&#x9;width: 12px;
}
{{< /highlight >}}

Um beim bereits genannten Artikel zu bleiben, trägt man einfach {{< mark >}}{{&lt; wayback &quot;http://www.dailycupoftech.com/2008/05/21/ms-eula-in-plain-english&quot; &gt;}}Daily Cup of Tech{{&lt; /wayback &gt;}}{{< /mark >}} anstelle des ursprünglichen Links im Artikel ein.

Aufgrund von {{< mark >}}$.Page.Params.PublishDate.Format &quot;20060102150405&quot;{{< /mark >}} sucht sich der Shortcode die beim Internet Archive gespeicherte Version heraus, die möglichst nahe am Veröffentlichungsdatum des Artikels liegt. 

Unter [https://fryboyter.de/microsoft-eula-fuer-englischsprachige-dummies/](https://fryboyter.de/microsoft-eula-fuer-englischsprachige-dummies/) kann man sich dann das Ergebnis ansehen. Was das zusätzliche Icon beim Link betrifft, bin ich mir noch nicht sicher, ob ich es so lasse wie es ist, bzw. ob ich es überhaupt haben will.

Einen Nachteil hat der Shortcode allerdings. Wenn die betreffende Seite nicht bei der Wayback Maschine gespeichert ist, funktioniert er nicht. Etwas Handarbeit ist also weiterhin nötig. Es erspart also nur die Version herauszusuchen, die möglichst nahe am Veröffentlichungsdatum des Artikels liegt.