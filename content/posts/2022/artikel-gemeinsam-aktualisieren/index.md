---
title: Artikel gemeinsam aktualisieren
date: 2022-09-24T17:21:25+0200
categories:
- OSBN
tags:
- Gemeinschaft
slug: artikel-gemeinsam-aktualisieren
---
Bei Arch Linux ist Python 3 schon lange der Standard. Allerdings haben diverse Pakete in den offiziellen Paketquellen immer noch Python 2 verwendet. Kürzlich haben die Entwickler von Arch Linux nun Python 2 endgültig [beerdigt](https://archlinux.org/news/removing-python2-from-the-repositories/). Dies habe ich zum Anlass genommen die bisher von mir veröffentlichten Artikel anzusehen.

Gefunden habe ich einen [Artikel](/e-mail-adressen-mit-python-aus-datenbanksicherung-auslesen/) für ein Script von 2016. Den Artikel konnte ich dank des Tools [2to3](https://docs.python.org/3/library/2to3.html) sehr leicht auf Python 3 aktualisieren.

Ich gehe allerdings davon aus, dass auch noch andere von meiner Artikel, die ich seit 2009 veröffentlicht habe, nicht mehr korrekt sind, da sich zwischenzeitlich einiges geändert hat. Selbst wenn ich nun alle Artikel selbst überprüfe, wäre ich mir nicht sicher, ob ich über jede nötige Änderung Bescheid wüsste.

Nun ist es so, dass es mich selbst nervt, wenn ich über die von mir bevorzugte Suchmaschine Artikel zur Lösung eines Problems finde, die veraltet sind. Was leider oft der Fall ist.

Daher habe ich mir überlegt, ob wir Betreiber diverse Blogs uns nicht gegenseitig unterstützen könnten, indem wir beispielsweise ab und zu eine "Bug Squashing Week" ins Leben rufen während dieser wir vorhandene Artikel anderer Blogs prüfen und bei Bedarf dem Betreiber entsprechend informieren oder, sofern möglich, Pull Requests erstellen.

Was ich damit erhoffe, zu erreichen ist, dass die Suchergebnisse bei den diversen Suchmaschinen möglichst aktuell sind. Denn leider ist es so, dass einmal veröffentlichte Artikel oft nie mehr aktualisiert werden. Wie man bei den genannten Artikel von 2016 sieht, bin ich leider keine Ausnahme.