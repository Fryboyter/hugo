---
title: Front Matter - CMS für statische Website-Generatoren
date: 2021-11-26T15:36:44+0100
categories:
- OSBN
tags:
- CMS
- Hugo
slug: front-matter-cms-fuer-statische-website-generatoren
---
Viele Nutzer entdecken für ihre Internetseiten immer öfter statische Website-Generatoren, da bei diesen der Wartungsaufwand im Vergleich zu beispielsweise WordPress sehr gering ist. Ich selbst nutze deswegen seit 2019 [Hugo](https://gohugo.io).

Um einen neuen Artikel anzulegen, starte ich VS Code, lege ein neues Verzeichnis unter content/posts an und kopiere bei Bedarf Bilder in dieses Verzeichnis, die in dem Beitrag angezeigt werden sollen. Danach erstelle ich die Datei index.md und erstelle in dieser mittels eines [Snippet](/vs-code-snippet-f%C3%BCr-front-matter-anlegen/) den Front-Matter-Bereich, den ich entsprechend anpasse. Unter diesen Bereich erstelle ich dann den eigentlichen Artikel. Abschließend werden diese Änderungen in ein Mercurial-Repository hochgeladen, was mittels eines [Hooks](/hooks-unter-mercurial/) den Inhalt von fryboyter.de erzeugt und die Änderungen zusätzlich mittels [Hg-Git](https://foss.heptapod.net/mercurial/hg-git) bei Github hochlädt.

An sich habe ich damit kein Problem. Es funktioniert. Und die Vorgänge kann ich inzwischen auswendig durchführen. Aber ab und zu wäre für mich etwas mehr Komfort trotzdem schön. Das gilt auch für Nutzer, die bei meinem "Workflow" schreiend wegrennen würden. Also vermutlich die meisten Nutzer.

Vor ein paar Tagen habe ich [Front Matter](https://frontmatter.codes) entdeckt. Hierbei handelt es sich um eine Erweiterung für VS Code die ein CMS für diverse statische Website-Generatoren direkt in VS Code anbietet. Neben Hugo werden auch noch andere Generatoren wie beispielsweise Jekyll, Gatsby, 11ty, Hexo oder Next.js unterstützt.

Aus zeitlichen Gründen habe ich bisher die Erweiterung nur installiert und die Grundkonfiguration erstellt. Ausgehend davon muss ich aber sagen, dass Front Matter einige Dinge sehr vereinfacht bzw. bequemer macht. Wenn man sich die [Dokumentation](https://frontmatter.codes/docs) durchliest, sollten aber durchaus komplexere Dinge möglich sein. Ich würde daher jedem, der einen statischen Website-Generator und VS Code nutzt, raten sich Front Matter zumindest einmal anzusehen. Über den eigenen Tellerrand zu schauen ist ja oft nicht verkehrt.
