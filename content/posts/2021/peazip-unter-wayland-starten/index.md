---
title: PeaZip unter Wayland starten
date: 2021-11-22T21:03:57+0100
categories:
- OSBN
- Linux
tags:
- PeaZip
- Wayland 
slug: pea-zip-unter-wayland-starten
---
Aus reiner Neugierde wollte ich mir das Packprogramm [PeaZip](https://peazip.github.io) ansehen. Direkt nach dem Starten ist das Programm mit folgender Fehlermeldung abgestürzt. 

{{< highlight bash >}}
[FORMS.PP] ExceptionOccurred 
  Sender=EAccessViolation
  Exception=Access violation
  Stack trace:
  $00000000007438D3
  $0000000000723049
  $00000000008ADC66
  $00000000008AD5FC
  $00000000008A9415
  $00000000007B3EFB
  $00000000007B3E02
  $0000000000448D9D
  $0000000000447B43
[FORMS.PP] ExceptionOccurred
{{< /highlight >}}

Die Ursache ist Wayland. PeaZip ist damit wohl noch nicht wirklich kompatibel. Daher muss man PeaZip mit dem x11 backend starten. Führt man also {{< mark >}}QT_QPA_PLATFORM=xcb peazip{{< /mark >}} aus, startet PeaZip ohne Probleme.