---
title: Zeichenkette an bestimmter Position in einer Datei einfügen
date: 2021-11-05T21:02:47+0100
categories:
- OSBN
- Linux
tags:
- Sed
- Loop
slug: zeichenkette-an-bestimmter-position-in-einer-datei-einfuegen
---
In einem Hauptverzeichnis sind diverse Dateien in Unterverzeichnissen vorhanden. In jeder dieser Dateien soll die gleiche Zeichenkette in die gleiche Zeile eintragen werden ohne das eine bereits vorhandene Zeile überschrieben wird. Nehmen wir als Beispiel einmal einen Front-Matter-Bereich eines der hier veröffentlichten Artikel.

{{< highlight bash >}}
---
title: WLAN-USB-Stick Cudy WU1300S unter Linux
date: 2021-05-02T16:49:00+0200
categories:
- OSBN
- Linux
tags:
- WLAN
- USB
slug: wlan-usb-stick-cudy-wu1300S-unter-linux
---
{{< /highlight >}}

Was tun, wenn man nun beispielsweise zwischen der Zeile mit dem Datum und der Kategorie eine Zeile mit {{< mark >}}include_toc: true{{< /mark >}} eintragen will? Am einfachst und aufwändigsten wäre es jede Datei händisch anzupassen. Da es sich aber um Dateien im drei- oder vierstelligen handelt, ist das keine gute Idee.

Um Dateien zu ändern, kann man das Tool sed nutzen, was eines der "Urgesteine" unter Linux ist. 

{{< highlight bash >}}
sed -i'4 i include_toc: true' Dateiname
{{< /highlight >}}

Die 4 gibt hierbei die gewünschte Zeile an. Das i dahinter steht für insert. Also einfügen.

Wer das ganze erst einmal testen will, ohne dass die Datei geändert wird, kann einfach {{< mark >}}-i{{< /mark >}} bei dem Befehl entfernen. Dann wird der Inhalt der Datei inklusive der Änderung ausgegeben, ohne die Änderung zu speichern.

Bleibt nur noch wie man den Befehl auf alle betreffenden Dateien automatisch anwendet. Hier reicht, wie so oft, eine einfache Schleife.

{{< highlight bash >}}
#/bin/bash
for i in $(find posts -name '*.md');
do
    sed -i '4 i include_toc: true' $i
done;
{{< /highlight >}}

Hiermit wird im aktuellen Verzeichnis sowie in den darin vorhandenen Unterverzeichnissen nach Dateien mit der Endung .md (Markdown-Dateien) gesucht. Diese werden dann mit dem sed Befehl entsprechend geändert.