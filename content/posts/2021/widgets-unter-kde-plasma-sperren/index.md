---
title: Widgets unter KDE Plasma sperren
date: 2021-10-10T11:52:24+0200
categories:
  - OSBN
  - Linux
tags:
  - KDE
  - Plasma
  - Widgets
slug: widgets-unter-kde-plasma-sperren
---

Wenn man unter KDE Plasma beispielsweise ein Widget auf den Desktop abgelegt hat, konnte man den Desktop so sperren, dass man die Widgets so lange nicht mehr verschieben konnte bis man die Sperre aufgehoben hat.

Diese Funktion wurde meines Wissens (warum auch immer) aus der grafischen Oberfläche entfernt. Nun ist es mir in letzter Zeit öfters passiert, dass ich auf ein Icon in der Schnellstartleiste zu lange geklickt haben und sich dabei den Mauszeiger bewegt hat. Was dazu geführt hat, dass sich beispielsweise das Icon, über das ich den Browser starte, auf den Desktop verschoben wurde. Was mich nervt.

Die Sperrfunktion gibt es aber intern weiterhin. Hierfür einfach folgendes im Terminal ausführen.

{{< highlight bash >}}
qdbus org.kde.plasmashell /PlasmaShell evaluateScript "lockCorona(true)"
{{< /highlight >}}

Zum Entsperren dann einfach den gleichen Befehl ausführen und das true auf false ändern. Damit man nicht jedes Mal die Sperre manuell aktivieren muss, kann man den Befehl beispielsweise in die Autostart-Funktion eintragen die man über die Systemeinstellung von KDE Plasma bearbeiten kann.
