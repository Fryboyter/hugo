---
title: AUR-Pakete wegen Python-Update neu erstellen
date: 2021-12-13T20:57:22+0100
categories:
- Linux
- OSBN
tags:
- AUR
- Python
- Arch Linux
slug: aur-pakete-wegen-python-update-neu-erstellen
---
Als ich eben die über [AUR](https://wiki.archlinux.org/title/Arch_User_Repository) installierte Software aktualisieren wollte, habe ich folgende Fehlermeldung erhalten.

{{< highlight bash >}}
Der Vorgang konnte nicht durchgeführt werden (In Konflikt stehende Dateien)
tortoisehg: /usr/lib/python3.9/site-packages/hgext3rd/__init__.py existiert im Dateisystem (gehört zu mercurial)
tortoisehg: /usr/lib/python3.9/site-packages/hgext3rd/__pycache__/__init__.cpython-39.pyc existiert im Dateisystem (gehört zu mercurial)
Fehler sind aufgetreten, keine Pakete wurden aktualisiert.
{{< /highlight >}}

Die Ursache ist, dass in den offiziellen Paketquellen von Arch Linux Python auf Version 3.10 aktualisiert wurde. Was es leider nötig macht die Pakete, die Python nutzen und über AUR installiert wurden, neu zu erstellen.

Als Erstes sollte man mittels {{< mark >}}pacman -Syu{{< /mark >}} die Pakete aus den offiziellen Paketquellen aktualisieren.

Um sich nun die betroffenen Pakete im AUR anzusehen, kann man folgenden Befehl ausführen.

{{< highlight bash >}}
pacman -Qqo /usr/lib/python3.9 | pacman -Qm -
{{< /highlight >}}

In meinem Fall werden drei Pakete angezeigt, die ich neu erstellen muss. Wer als AUR-Helper paru verwendet, kann diese mit folgendem Befehl automatisch neu erstellen. Mit anderen AUR-Helpern die eine Rebuild-Funktion anbieten sollte es vergleichbar funktionieren.

{{< highlight bash >}}
pacman -Qoq /usr/lib/python3.9 | pacman -Qmq - | paru -S --rebuild -
{{< /highlight >}}

Führt man den Befehl nun spaßeshalber noch einmal aus, sollte man die Meldung "Fehler: Kein Paket besitzt /usr/lib/python3.9" erhalten.

Und ja, das ist einer der Nachteile an AUR. Dafür lassen sich die dortigen Rezepte leichter prüfen als beispielsweise fertige Pakete aus einem PPA. Und leichter erstellen lassen sie sich in der Regel auch.
