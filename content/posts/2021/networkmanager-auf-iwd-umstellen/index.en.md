---
title: Switch NetworkManager to iwd
date: 2022-10-19T19:02:15+0200
categories:
- Linux
- Englis(c)h
tags:
- NetworkManager
- iwd
- WLAN
- Wpa_supplicant
slug: switch-network-manager-to-iwd
---
Many users use the NetworkManager tool to manage network connections. When it comes to WLAN connections, wpa_supplicant is usually used. If the connection is disconnected, for example because the computer has been put into sleep mode, it takes a relatively long time for the connection to be re-established. 

To speed up the connection, you can use [iwd](https://iwd.wiki.kernel.org) instead of wpa_supplicant. 

First install it with the respective package management (in the case of Arch Linux with {{< mark >}}pacman -S iwd{{< /mark >}}). Now add the following content to the file /etc/NetworkManager/NetworkManager.conf or adapt the file accordingly if another WiFi backend is already defined.

{{< highlight bash >}}
[device]
wifi.backend=iwd
{{< /highlight >}}

Abschließend beendet man wpa_supplicant mittels {{< mark >}}systemctl stop wpa_supplicant.service{{< /mark >}} und startet NetworkManager mit {{< mark >}}systemctl restart NetworkManager.service{{< /mark >}} neu. Nun sollte iwd anstelle von wpa_supplicant verwendet werden und der Verbindungsaufbau sollte von nun an schneller erfolgen.