---
title: WLAN-Verbindung unter KDE Plasma auf 5 GHz umstellen
date: 2021-12-24T14:14:52+0100
categories:
- Linux
- OSBN
tags:
- NetworkManager
- WLAN
slug: wlan-verbindung-unter-kde-plasma-auf-5-ghz-umstellen
---
Ein Notebook ist mit einem WLAN verbunden das sowohl 2,4 als auch 5 Ghz unterstützt. Es verbindet sich allerdings immer nur mit 2,4 Ghz. Der Grund hierfür könnte sein, dass keine [BSSID](https://www.speedcheck.org/de/wiki/bssid/) oder die falsche hinterlegt wurde.

Unter KDE Plasma ruft man hierfür die Systemeinstellungen (systemsettings5) auf und klickt in der linken Spalte im Bereich Netzwerk auf Verbindungen. Beim betreffenden WLAN öffnet man den Reiter Wi-Fi und wählt bei BSSID den Eintrag mit 5 Ghz aus (z. B. Frequenz: 5.180 Mhz). Nun klickt man noch auf Anwenden und trennt anschließend die WLAN-Verbindung und baut sie neu auf. Nun sollte die Netzwerkverbindung über 5 Ghz aufbauen.