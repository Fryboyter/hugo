---
title: Mehrzeilige Ausgabe eines Befehls in einer Zeile darstellen
date: 2023-05-03T23:10:43+0200
categories:
- OSBN
tags:
- Mehrzeilig
- Paste
slug: mehrzeilige-ausgabe-eines-befehls-in-einer-zeile-darstellen
---
Kürzlich wurde nun Python 3.11 unter Arch Linux offiziell veröffentlicht. Nach dem Update wollte ich nachsehen, welche Pakete im AUR einen Rebuild benötigen. Hierfür habe ich den Befehl {{< mark >}}pacman -Qoq /usr/lib/python3.10{{< /mark >}} ausgeführt. Die Ausgabe könnte wie folgt aussehen.

{{< highlight bash >}}
ocrmypdf
pbget
pm2ml
tortoisehg
{{< /highlight >}}

Soweit so gut. Es wäre aber besser, wenn die Ausgabe in einer Zeile erfolgen würde um sie besser weiterverwenden zu können. Hierfür kann man unter anderem den Befehl paste nutzen, welcher Teil des Pakets coreutils ist. Somit stellt der Befehl {{< mark >}}pacman -Qoq /usr/lib/python3.10 | paste -s -d' '{{< /mark >}} die Ausgabe in einer Zeile dar.

{{< highlight bash >}}
ocrmypdf pbget pm2ml tortoisehg
{{< /highlight >}}

Das erleichtert die Weiterverarbeitung ziemlich, sodass ich kurzerhand mit dem Befehl {{< mark >}}aur sync &hyphen;&hyphen;nover -f $(pacman -Qoq /usr/lib/python3.10 | paste -s -d&apos; &apos;){{< /mark >}} die betreffenden AUR-Pakete mit Python 3.11 neu erzeugt habe. Somit musste ich nicht abwarten bis die jeweiligen Betreuer die PKGBUILD-Dateien im AUR aktualisiert haben und somit einen Rebuild veranlassen.