---
title: Zusammenfassung einer mit Hugo erzeugten Internetseite erstellen
date: 2023-06-28T23:25:29+0100
categories:
- OSBN
tags:
- Hugo
- Zusammenfassung
slug: zusammenfassung-einer-mit-hugo-erzeugten-internetseite-erstellen
---
In manchen Fällen will man sich einen Überblick verschaffen aus wie vielen Seiten und Beiträgen eine Internetseite besteht und welche Tags und Kategorien verwendet werden. Unter Hugo ist das relativ einfach umzusetzen.

Ich gehe davon aus, dass lokal folgende Verzeichnisstruktur vorhanden ist, in der die Dateien liegen, anhand derer die Internetseite erstellt wird.

{{< highlight bash >}}
Hauptverzeichnis der Hugo-Seite
├── content
│  └── pages
└── themes
   └── Name des Themes
      ├── layouts
      │  └── shortcodes
      └── static
         └── css
{{< /highlight >}}

Als Erstes erstellt man im Verzeichnis shortcodes die Datei summary.html und fügt Folgendes ein.

{{< highlight go-template >}}
{{ $posts := (where .Site.RegularPages "Section" "==" "posts") }}
{{ $postCount := len $posts }}


{{ $pages := (where .Site.RegularPages "Section" "==" "pages") }}
{{ $pageCount := len $pages }}

<div class="summary">
	<span>Seiten:</span>
	<span>{{ $pageCount }}</span>
	<span>Beiträge:</span>
	<span>{{ $postCount }}</span>
	<span>Beiträge je Kategorie:</span>
	<span
		>{{ range .Site.Taxonomies.categories }}
		<a href="{{ .Page.RelPermalink }}">{{ .Page.Title }}({{ .Count }})</a>&nbsp; {{ end }}</span
	>
	<span>Beiträge je Tag:</span>
	<span
		>{{ range .Site.Taxonomies.tags }} <a href="{{ .Page.RelPermalink }}">{{ .Page.Title }}({{ .Count }})</a>&nbsp;
		{{ end }}
	</span>
</div>
{{< /highlight >}}

Hiermit wird die Anzahl der Seiten und Beiträge sowie alle verwendeten Tags und Kategorien angezeigt und wie oft sie verwendet werden.

Als nächstes erstellt man im Verzeichnis pages die Datei summary.md und befüllt diese wie folgt.

{{< highlight bash >}}
---
title: Zusammenfassung
url: summary
---

{{</* summary */>}}
{{< /highlight >}}

Mit der url-Zeile wird definiert, über welchen Link man die Übersicht aufrufen kann. Die letzte Zeile fügt den anfangs definierten Shortcode ein.

Damit die Übersicht einigermaßen ansehnlich ist, erweitert man die Standard-CSS-Datei (z. B. style.css) im Verzeichnis css noch mit zwei Anweisungen.

{{< highlight css >}}
.summary {
	display: grid;
	grid-template-columns: 1fr 3fr;
	border-top: 1px solid black;
	border-right: 1px solid black;
}

.summary > span {
	padding: 8px 4px;
	border-left: 1px solid black;
	border-bottom: 1px solid black;
}
{{< /highlight >}}

Erzeugt man nun die Seite sollte die Übersicht über /summary also beispielsweise domain.de/summary aufrufbar sein und in etwa so aussehen.

{{< image src="uebersicht.png" alt="Bild das anzeigt aus wie viele Seiten und Beiträge eine Internetseite besteht und welche Tags und Kategorien verwendet werden." >}}

Diese Anzeige lässt sich auch gut für Verbesserungen nutzen. In meinem Fall gibt es beispielsweise Tags, die ich sowohl im Singular als auch im Plural verwendet habe, was nicht unbedingt nötig war.