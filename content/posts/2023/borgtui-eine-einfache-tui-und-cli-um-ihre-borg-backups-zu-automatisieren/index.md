---
title: BorgTUI - Eine einfache TUI und CLI, um Ihre Borg-Backups zu automatisieren
date: 2023-08-26T16:00:39+0200
categories:
- OSBN
tags:
- Borg
slug: eine-einfache-tui-und-cli-um-ihre-borg-backups-zu-automatisieren
---
Für Backups nutze ich seit Jahren [Borg](https://www.borgbackup.org). In meinem Fall in Form diverser Shell Scripte.

Mit beispielsweise [Vortag](https://vorta.borgbase.com) oder [Pika](https://apps.gnome.org/app/org.gnome.World.PikaBackup/) gibt es grafische Oberflächen die Borg nutzen.

Kürzlich bin ich auf [BorgTUI](https://github.com/dpbriggs/borgtui) gestoßen, was eine TUI für Borg anbietet. Also im Grunde genommen eine Lösung zwischen einer grafischen Oberfläche und dem Terminal Emulator.

Ich habe damit noch keine Erfahrungen gemacht, da ich, wie schon gesagt Borg mittels Scripte nutze. Aber vielleicht ist BorgTUI ja für den einen oder anderen von Interesse.