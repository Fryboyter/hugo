---
title: SDDM könnte ein offizielles KDE-Projekt werden
date: 2023-05-23T19:57:46+0200
categories:
- OSBN
tags:
- SDDM
- KDE
- Plasma
slug: sddm-koennte-ein-offizielles-kde-projekt-werden
---
Kürzlich habe ich in einem [Artikel](/warum-etwas-entwickeln-aber-nicht-veroeffentlichen) kritisiert, dass seit November 2020 keine neue offizielle Version des Displaymanagers SDDM veröffentlicht wurde, obwohl dieser aktiv weiterentwickelt wird und zudem zwischenzeitlich einige nervige Bugs behoben wurden. Mit etwas Glück könnte sich das in absehbarer Zeit ändern.

Nate Graham hat den [Vorschlag](https://invent.kde.org/plasma/plasma-desktop/-/issues/91) gemacht, aus SDDM ein offizielles KDE-Projekt zu machen. Zumal ein Großteil aller Entwickler von SDDM auch KDE-Entwickler sind. SDDM würde sich dann ggf. auch am Release-Zyklus von Plasma orientieren, sodass regelmäßig neue Versionen veröffentlicht würden. Wenn alle klappt wie vorgeschlagen, dann könnte SDDM bereits Teil von Plasma 6 sein.