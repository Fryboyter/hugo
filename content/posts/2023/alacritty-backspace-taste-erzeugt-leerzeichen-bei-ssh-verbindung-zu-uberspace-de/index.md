---
title: Alacritty - Backspace-Taste erzeugt Leerzeichen bei SSH-Verbindung zu Uberspace.de
date: 2023-12-18T19:07:33+0100
nocomments: true
categories:
- OSBN
tags:
- Alacritty
- SSH
- Uberspace
slug: alacritty-backspace-taste-erzeugt-leerzeichen-bei-ssh-verbindung-zu-uberspace-de
---
Seit mehreren Jahren habe ich den Terminal Emulator [Terminator](https://github.com/gnome-terminator/terminator) genutzt. Was mich an dem Programm stört ist, dass [OSC52](https://www.reddit.com/r/vim/comments/k1ydpn/a_guide_on_how_to_copy_text_from_anywhere/) nicht unterstützt wird. Das liegt daran, dass Terminator auf VTE3 basiert und das dafür verantwortliche Team möchte OSC52 wegen möglicher Sicherheitsprobleme nicht einbauen ([https://gitlab.gnome.org/GNOME/vte/-/issues/2495](https://gitlab.gnome.org/GNOME/vte/-/issues/2495)).

Auch wenn ich die Gründe nachvollziehen kann, bin ich zu dem Ergebnis gekommen, dass ich mit den möglichen Risiken leben kann. Da ich seit ein paar Wochen verstärkt [Zellij](https://zellij.dev) nutze, habe ich mich auch entschieden, dass ich auch gleich den Terminal Emulator wechsle. Da Zellij Tiling und Tabs anbietet, habe ich mich für den Terminal Emulator [Alacritty](https://alacritty.org) entschieden, der dies nicht anbietet. Dafür wird OSC52 unterstützt.

Seit ich Alacritty nutze, habe ich das Problem, dass sobald ich eine SSH-Verbindung zu Uberspace aufgebaut habe und die Backspace-Taste drücke Leerzeichen angezeigt werden. Das Problem kann ich mit oder ohne Zellij nachvollziehen.

Die Ursache ist aber nicht schlimm und die Lösung somit glücklicherweise auch recht einfach.

Aber kommen wir erst mal zur Ursache. Das Problem wird dadurch ausgelöst, dass Alacritty den Wert von $TERM bei Uberspace auf {{< mark >}}alacritty{{< /mark >}} ändert. Die Server von Uberspace kommen damit scheinbar nicht zurecht.

Wie kann man das Problem lösen? Mir sind in dem Fall zwei Möglichkeiten bekannt den Wert für $TERM bei Uberspace zu ändern. 

Wer SSH-Verbindungen in ~/.ssh/config definiert hat, kann die betreffende Konfiguration entsprechend erweitern.

{{< highlight bash >}}
Host        Uberspace
Hostname    xxx.uberspace.de
SetEnv      TERM=xterm-256color
Port        22
User        User
IdentityFile ~/.ssh/Uberspace
{{< /highlight >}}

Die zweite Möglichkeit ist, die Konfiguration von Alacritty entsprechend anzupassen. Der betreffende Teil der Konfigurationsdatei (~/.config/alacritty/alacritty.yml) würde dann wie folgt aussehen.

{{< highlight bash >}}
env:
 TERM: xterm-256color
{{< /highlight >}}

Baut man dann eine SSH-Verbindung zu Uberspace auf und führt {{< mark >}}echo $TERM{{< /mark >}} aus, sollte man die Ausgabe {{< mark >}}xterm-256color{{< /mark >}} erhalten und die Backspace-Taste sollte wieder funktionieren. Vermutlich sind auch nicht andere Tasten betroffen, aber mir ist das Problem nur bei der Backspace-Taste aufgefallen.