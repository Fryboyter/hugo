---
title: Exa ist vielleicht tot. Lang lebe eza.
date: 2023-08-18T16:39:10+0200
categories:
- OSBN
tags:
- Exa
- Eza
- Ls
slug: exa-ist-vielleicht-tot-lang-lebe-eza
---
Ich neige dazu anstelle der wohlbekannten Tools wie ls, grep und so weiter alternative Programme zu nutzen. Zum Teil, weil sie mir einfach besser gefallen aber auch, weil sie Verbesserungen anbieten.

So nutze ich seit Jahren das Tool [exa](https://the.exa.website), was als Ersatz für ls dient. Exa hat aber einen Nachteil. Der Hauptentwickler ogham ist seit 2021 nicht mehr auf Github aktiv. Und ariasuni kann und will aus diversen Gründen sich nicht genug um das Projekt exa kümmern ([https://github.com/ogham/exa/issues/1139](https://github.com/ogham/exa/issues/1139)). Somit wurde die letzte offizielle Version von exa 2021 veröffentlicht.

Nun hat sich cafkafk aufgerafft und vor ca. 3 Wochen den Fork [eza](https://github.com/eza-community/eza) erstellt. Seit dem hat sie 5 neue Versionen mit vielen Verbesserungen veröffentlicht.

Auch wenn bei dem Projekt exa zwischenzeitlich einige Pull Requests bearbeitet wurden (https://github.com/ogham/exa/commits/master) wurde bisher immer noch keine neue Version veröffentlicht.

Natürlich kann ich nicht hellsehen, aber ausgehend vom aktuellen Stand der Dinge vermute ich, dass eza aufgrund der Aktivität bezüglich der Entwicklung und Veröffentlichung das Rennen für sich entscheidet. Ich bin inzwischen zumindest von exa auf eza gewechselt.