---
title: Mercurial - show commits that were not created by a specific user
date: 2023-07-18T00:13:34+0200
categories:
- Englis(c)h
tags:
- Mercurial
- Revsets
slug: mercurial-show-commits-that-were-not-created-by-a-specific-user
---
Let's assume that in a repository created with Mercurial, except for a few exceptions, all commits were created by a particular user. The GPL is used as the license for the code. This user now wants to change the license.

Basically, there are two options in this case. You contact the people who have contributed to the project so far and ask them to agree to the license change. Or you remove the commits in question and create new commits yourself. In this case you should not take over the code 1:1.

No matter which solution you choose, you first have to find out which commits are affected. So you have to check every single commit manually? In the case of this website, that would be 560. If you count the commit for this article, it would be 561. Other projects have many more commits. So there must be a simpler and faster solution.

With git up to version 2.35 {{< mark >}}git log &hyphen;&hyphen;invert-grep &hyphen;&hyphen;author=&lt;user&gt;{{< /mark >}} would have worked if I'm not mistaken. But the current version is git 2.41.0 and it's about Mercurial.

The log function of Mercurial does not seem to offer a comparable possibility directly (&hyphen;&hyphen;exclude refers to files only).

However, one can use so-called [Revsets](https://repo.mercurial-scm.org/hg/help/revsets). To display all commits that do not originate from the user "Hans", for example, you can use the command {{< mark >}}hg log -r "not(user(Hans))"{{< /mark >}}. But what if you want to ignore not only the commits from Hans, but also from Martin? Then you can simply change the command to {{< mark >}}hg log -r "not(user(Hans)) and not(user(Martin))"{{< /mark >}}.

As you can see from the above link, revsets can be used to create even more complex queries. But that would go beyond the scope of this article.