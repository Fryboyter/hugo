---
title: Mercurial - Commits anzeigen, die nicht von einem bestimmten Nutzer erstellt wurden
date: 2023-07-18T00:13:34+0200
categories:
- OSBN
tags:
- Mercurial
- Revsets
slug: mercurial-commits-anzeigen-die-nicht-von-einem-bestimmten-nutzer-erstellt-wurden
---
Nehmen wir an, dass in einem mit Mercurial erzeugten Repository bis auf wenige Ausnahmen alle Commits von einem bestimmten Nutzer erstellt wurden. Als Lizenz für den Code wird die GPL genutzt. Besagter Nutzer will nun die Lizenz ändern.

Im Grunde gibt es in diesem Fall zwei Möglichkeiten. Man kontaktiert die Leute, die bisher zu dem Projekt beigetragen haben und bittet sie dem Lizenzwechsel zuzustimmen. Oder man entfernt die betreffenden Commits und erstellt selbst neue Commits. In diesem Fall sollte man den betreffenden Code aber nicht 1:1 übernehmen.

Egal für welche Lösung man sich entscheidet, man muss erst einmal herausfinden welche Commits überhaupt betroffen sind. Also jeden Commit einzeln manuell prüfen? Das wären im Falle von dieser Internetseite schon 560. Zählt man den Commit für diesen Artikel mit, wären es schon 561. Andere Projekte haben noch viel mehr Commits. Es muss also einfacher und schneller gehen.

Mit git hätte bis Version 2.35 {{< mark >}}git log &hyphen;&hyphen;invert-grep &hyphen;&hyphen;author=&lt;Nutzer&gt;{{< /mark >}} funktioniert, wenn ich mich nicht irre. Aber aktuell ist git 2.41.0 und es geht um Mercurial.

Die Log-Funktion von Mercurial bietet direkt scheinbar keine vergleichbare Möglichkeit an (&hyphen;&hyphen;exclude bezieht sich nur auf Dateien).

Man kann allerdings sogenannte [Revsets](https://repo.mercurial-scm.org/hg/help/revsets) nutzen. Um sich alle Commits anzuzeigen, die beispielsweise nicht vom Benutzer "Hans" stammen, kann man den Befehl {{< mark >}}hg log -r "not(user(Hans))"{{< /mark >}} nutzen. Was aber, wenn man nicht nur die Commits von Hans, sondern auch von Martin ignorieren will? Dann kann man den Befehl einfach in {{< mark >}}hg log -r "not(user(Hans)) and not(user(Martin))"{{< /mark >}} ändern. 

Wie aus dem genannten Link ersichtlich, lassen sich mit Revsets noch ganz andere, komplexere Abfragen erstellen. Aber das würde den Umfang dieses Artikels sprengen.