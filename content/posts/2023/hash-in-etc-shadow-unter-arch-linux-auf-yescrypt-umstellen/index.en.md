---
title: Change hash in /etc/shadow under Arch Linux to yescrypt
date: 2023-09-25T18:09:12+0200
categories:
- Englis(c)h
tags:
- Hash
- yescrypt
slug: change-hash-in-etc-shadow-under-arch-linux-to-yescrypt
---
A few days ago the update of the shadow package to version 4.14 was [released](https://archlinux.org/news/changes-to-default-password-hashing-algorithm-and-umask-settings/) on Arch Linux. As of this version, yescrypt is used to generate the password hashes in /etch/shadow instead of SHA512.

However, existing entries are not converted in this case. The SHA512 hashes can still be used.

If you want to change entries for existing users to yescrypt, you can do this with the command {{< mark >}}passwd &#x3C;username&#x3E;{{< /mark >}}. If you have not configured [PAM](https://wiki.archlinux.org/title/PAM) accordingly, you can also enter the current password and do not have to choose a new password.

You can verify if it worked by looking at the /etc/shadow file. The hash of the entries for the users in question should now start with {{< mark >}}$y${{< /mark >}}.