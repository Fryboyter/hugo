---
title: Hash in /etc/shadow unter Arch Linux auf yescrypt umstellen
date: 2023-09-25T18:09:12+0200
categories:
- OSBN
tags:
- Hash
- yescrypt
slug: hash-in-etc-shadow-unter-arch-linux-auf-yescrypt-umstellen
---
Vor ein paar Tagen wurde unter Arch Linux das Update des Pakets shadow auf Version 4.14 [veröffentlicht](https://archlinux.org/news/changes-to-default-password-hashing-algorithm-and-umask-settings/). Ab dieser Version wird zum Erzeugen der Passwort-Hashes in /etch/shadow nicht mehr SHA512, sondern yescrypt genutzt.

Bestehende Einträge werden hierbei aber nicht umgestellt. Die SHA512-Hashes sind weiterhin nutzbar. 

Wer Einträge für bestehende Nutzer auf yescrypt umstellen will, kann dies mit dem Befehl {{< mark >}}passwd &#x3C;Benutzername&#x3E;{{< /mark >}} machen. Sofern man [PAM](https://wiki.archlinux.org/title/PAM) nicht entsprechend konfiguriert hat, kann man hierbei auch das bisherige Passwort angeben und muss kein neues Passwort wählen.

Ob es geklappt hat, kann man sich ansehen, indem man sich die Datei /etc/shadow ansieht. Bei den Einträgen bei den betreffenden Nutzern sollte der Hash nun mit {{< mark >}}$y${{< /mark >}} beginnen.