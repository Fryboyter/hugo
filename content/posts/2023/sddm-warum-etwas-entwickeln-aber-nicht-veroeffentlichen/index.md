---
title: Sddm - Warum etwas entwickeln aber nicht veröffentlichen?
date: 2023-04-16T08:32:23+0200
categories:
- OSBN
tags:
- Sddm
- Veröffentlichung
- Entwicklung
slug: warum-etwas-entwickeln-aber-nicht-veroeffentlichen
---
Wer KDE Plasma nutzt, wird mit ziemlicher Wahrscheinlichkeit auch den Displaymanager [sddm](https://github.com/sddm/sddm) verwenden. Dieser wird als eigenständiges Projekt auch aktiv weiterentwickelt. So wurden beispielsweise diesen Monat bisher 6 Commits erstellt. Im März waren es 11. Trotzdem habe ich und andere Nutzer immer noch mit bekannten Bugs zu kämpfen.

So benötigten aktuell alle meine Rechner mit Plasma ca. 14 Sekunden um sddm zu beenden. Ja, davon geht die Welt nicht unter. Aber es nervt. Vor allem, weil der Bug an sich scheinbar schon behoben ist. Denn ich habe vor einigen Tagen einfach mal die Git-Version anstelle der aktuellen offiziellen Version 0.19.0 installiert (im AUR als sddm-git zu finden). Seit dem ist das Problem mit der langen Wartezeit beim Herunterfahren verschwunden. Sobald ich wieder die Version 0.19.0 installiere, tritt das Problem wieder auf.

Nun werden einige sagen, dass ich doch einfach auf die Veröffentlichung der nächsten Version warten soll. Das ist aber genau das Problem. Version 0.19.0 wurde im November 2020 veröffentlicht. Seit dem wurde schon mehrmals um die Veröffentlichung einer neuen Version gebeten. Zum Beispiel unter [https://github.com/sddm/sddm/issues/1471](https://github.com/sddm/sddm/issues/1471). Unter anderem, weil ja zwischenzeitlich andere Verbesserungen gemacht wurden und nicht nur der von mir beschriebene Bug behoben wurde.

Ich kann es ja verstehen, dass man ein neues Release möglichst fehlerfrei veröffentlichen möchte. Und vielleicht auch noch die eine oder andere neue Funktion einbauen will. Aber wenn seit der letzten offiziellen Version bereits einige Bugs behoben wurden, die den Nutzern auf die Nerven gehen, und mehrere Jahre vergangen sind, warum kann man nicht einfach ein Bugfix-Release veröffentlichen? Also Version 0.19.1 und nicht 20.0.0? Dann kann man ja weiterhin am "perfekten" Release arbeiten.

Ich für meinen Teil werde erst einmal die derzeit aktuelle Version von sddm-git weiter nutzen, was inzwischen wohl auch einige andere Nutzer machen, die nicht mehr auf eine neue offizielle Version warten wollen. Bisher scheint damit ja alles zu funktionieren und ich bin daher nicht gezwungen weitere Updates zu installieren. Denn auf einen anderen Displaymanager möchte ich möglichst nicht wechseln.