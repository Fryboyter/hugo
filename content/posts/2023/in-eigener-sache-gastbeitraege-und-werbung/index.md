---
title: "In eigener Sache: Gastbeiträge und Werbung"
date: 2023-03-17T14:53:09+0100
categories:
- OSBN
tags:
- Gastbeiträge
- Werbung
slug: in-eigener-sache-gastbeitraege-und-werbung
---
In letzter Zeit erhalte ich, mal wieder, verstärkt Anfragen Dritter bezüglich Gastbeiträge bzw. Werbung auf fryboyter.de. In vielen Fällen sind es Bots, in manchen Fällen bin ich mir sicher, dass die Anfragen von echten Menschen erstellt wurden.

Um es kurz zu machen. 

Ich werde keine Werbung von Dritten veröffentlichen. Egal ob mir hierfür 50 oder 50000 Euro geboten werden. Wenn ich etwas empfehle, dann weil ich selbst positive Erfahrungen damit gemacht habe.

Gastbeiträge möchte ich derzeit ebenfalls nicht veröffentlichen. Das wird sich vermutlich auch so schnell nicht ändern. Zumal heutzutage im Grunde jeder selbst die Möglichkeit hat etwas im Internet zu veröffentlichen.

Anfragen dieser Art werde ich weiterhin ignorieren und somit auch nicht beantworten.