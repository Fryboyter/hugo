---
title: Hugo - show hint when an article is older than a certain age
date: 2023-10-15T18:48:33+0200
categories:
- Englis(c)h
tags:
- Hugo
slug: hugo-show-hint-when-an-article-is-older-than-a-certain-age
---
On some internet pages a hint is displayed if, for example, an article is already somewhat older and thus possibly no longer current. I wondered if and how this could be done with [Hugo](https://gohugo.io).

Relatively quickly I have fabricated the following code.

{{< highlight go-template >}}
{{ $currentTime := (time now) }}
{{ $delta := $currentTime.Sub (time .Date ) }}
{{ if gt $delta.Hours 8760 }}
    <p>
        Dieser Artikel ist mindestens ein Jahr alt. Der Inhalt könnte daher nicht mehr aktuell sein.
    </p>
{{ end }}
{{< /highlight >}}

This checks whether at least 8760 hours, i.e. 365 days, have passed between the date of an article and the current date. If so, the hint is displayed.

Can the solution really be that simple? When I ask myself this question, the answer is usually no. So also in this case.

In many of the articles I publish, indeed, only the creation date is given. For these, the code mentioned is also sufficient.

However, I do update a few articles from time to time. For those, I additionally add the date of the last update. The most frequently updated article is probably [this](/en/hugo-extended-for-uberspace-de/). It was created on April 23, 2022. Thus a hint would be displayed. However, the article was last updated on September 24, 2023 and is thereby not outdated. Therefore no hint should be displayed.

So how do you solve this? You simply check if there is a date of the last update (.Lastmod). If yes, check its age. Otherwise you check directly the creation date (.Date). Wrong. Because if there is no date for .Lastmod stored in the front matter of an article, .Lastmod still has a value. And this is that of .Date. For whatever reason.

I have therefore adapted the code to first check if .Lastmod and .Date have different values.

If yes, it will be checked if one year has passed between the date of .Lastmod and the current date. If yes, a hint is displayed.

But if .Lastmod and .Date are the same, .Date is used to check the age.

Now here is the final code.

{{< highlight go-template >}}
{{ if ne .Lastmod .Date }}
    {{ $currentTime := (time now) }}
    {{ $delta := $currentTime.Sub (time .Lastmod ) }}
    {{ if gt $delta.Hours 8760 }}
        <p>
            Dieser Artikel ist mindestens ein Jahr alt. Er könnte damit nicht mehr aktuell sein.
        </p>
    {{ end }}
{{ else }}
    {{ $currentTime := (time now) }}
    {{ $delta := $currentTime.Sub (time .Date ) }}
    {{ if gt $delta.Hours 8760 }}
        <p>
            Dieser Artikel ist mindestens ein Jahr alt. Er könnte damit nicht mehr aktuell sein.
        </p>
    {{ end }}
{{ end }}
{{< /highlight >}}