---
title: Hugo - Hinweis ab einem bestimmten Alters eines Artikels anzeigen
date: 2023-10-15T18:48:33+0200
categories:
- OSBN
tags:
- Hugo
slug: hugo-hinweis-ab-einem-bestimmten-alters-eines-artikels-anzeigen
---
Auf manchen Internetseiten wird ein Hinweis angezeigt, wenn beispielsweise ein Artikel schon etwas älter und somit eventuell nicht mehr aktuell ist. Ich habe mich gefragt, ob bzw. wie man es mit [Hugo](https://gohugo.io) umsetzen kann.

Relativ schnell habe ich folgenden Code fabriziert.

{{< highlight go-template >}}
{{ $currentTime := (time now) }}
{{ $delta := $currentTime.Sub (time .Date ) }}
{{ if gt $delta.Hours 8760 }}
    <p>
        Dieser Artikel ist mindestens ein Jahr alt. Der Inhalt könnte daher nicht mehr aktuell sein.
    </p>
{{ end }}
{{< /highlight >}}

Hiermit wird geprüft, ob zwischen dem Datum eines Artikels und dem aktuellen Datum mindestens 8760 Stunden, also 365 Tage, vergangen sind. Wenn ja, wird der Hinweis angezeigt.

Kann die Lösung wirklich so einfach sein? Wenn ich mir diese Frage stelle, lautet die Antwort meist nein. So auch in diesem Fall.

Bei vielen der von mir veröffentlichten Artikel ist in der Tat nur das Erstellungsdatum angegeben. Bei diesen reicht der genannte Code auch aus.

Einige, wenige Artikel hingegen, aktualisiere ich allerdings von Zeit zu Zeit. Bei diesen trage ich zusätzlich noch das Datum der letzten Aktualisierung ein. Der wohl am häufigsten aktualisierte Artikel dürfte [dieser](/hugo-extended-für-uberspace-de/) sein. Erstellt wurde er am 23.04.22. Somit würde ein entsprechender Hinweis angezeigt. Allerdings wurde der Artikel letztmalig am 24.09.23 aktualisiert und ist somit nicht veraltet. Daher sollte kein Hinweis angezeigt werden. 

Wie löst man das nun? Man prüft einfach ob es ein Datum der letzten Aktualisierung (.Lastmod) gibt. Wenn ja, prüft man dessen Alter. Ansonsten prüft man direkt das Erstellungsdatum (.Date). Falsch. Denn wenn bei einem Artikel im Front-Matter-Bereich kein Datum für .Lastmod hinterlegt wurde, hat .Lastmod trotzdem einen Wert. Nämlich den von .Date. Warum auch immer.

Ich habe daher den Code so angepasst, dass zuerst geprüft wird, ob .Lastmod und .Date unterschiedliche Werte haben. 

Wenn ja, wird geprüft, ob zwischen dem Datum von .Lastmod und dem aktuellen Datum ein Jahr vergangen ist. Wenn ja, wird ein Hinweis angezeigt.

Wenn aber .Lastmod und .Date gleich sind, wird für die Prüfung des Alters .Date genutzt.

Hier nun der fertige Code.

{{< highlight go-template >}}
{{ if ne .Lastmod .Date }}
    {{ $currentTime := (time now) }}
    {{ $delta := $currentTime.Sub (time .Lastmod ) }}
    {{ if gt $delta.Hours 8760 }}
        <p>
            Dieser Artikel ist mindestens ein Jahr alt. Er könnte damit nicht mehr aktuell sein.
        </p>
    {{ end }}
{{ else }}
    {{ $currentTime := (time now) }}
    {{ $delta := $currentTime.Sub (time .Date ) }}
    {{ if gt $delta.Hours 8760 }}
        <p>
            Dieser Artikel ist mindestens ein Jahr alt. Er könnte damit nicht mehr aktuell sein.
        </p>
    {{ end }}
{{ end }}
{{< /highlight >}}