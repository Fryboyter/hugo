---
title: VeraCrypt stellt Unterstützung von TrueCrypt ein
date: 2023-07-28T16:13:10+0200
categories:
- OSBN
tags:
- VeraCrypt
- TrueCrypt
slug: vera-crypt-stellt-unterstuetzung-von-true-crypt-ein
---
[VeraCrypt](https://www.veracrypt.fr/en/Home.html) ist ein Programm mit dem man verschlüsselte Container-Dateien und Partitionen erstellen kann. Der Vorteil gegenüber beispielsweise dm-crypt ist, dass VeraCrypt direkt für Windows, Linux, macOS und FreeBSD angeboten wird. Daher lassen sich verschlüsselte Dateien relativ einfach von unterschiedlichen Betriebssystemen nutzen.

Mit VeraCrypt konnte man nun jahrelang Container-Dateien bzw. Partitionen verwenden die noch mit [TrueCrypt](https://de.wikipedia.org/wiki/TrueCrypt) erstellt wurden, da VeraCrypt der Nachfolger von TrueCrypt ist. Diese Unterstützung wird mit Version 1.26 (aktuell Beta-Status) eingestellt.

Wer also noch Container-Dateien bzw. Partitionen hat, die mit TrueCrypt erstellt wurden, sollte zeitnah komplett auf Format von VeraCrypt wechseln. Unter https://blog.doenselmann.com/veracrypt-teil-3-truecrypt-volumes-migrieren/ hat jemand anderes eine entsprechende Anleitung veröffentlicht. Der Wechsel von Systempartitionen ist nicht möglich ([https://www.veracrypt.fr/en/Converting%20TrueCrypt%20volumes%20and%20partitions.html](https://www.veracrypt.fr/en/Converting%20TrueCrypt%20volumes%20and%20partitions.html)).