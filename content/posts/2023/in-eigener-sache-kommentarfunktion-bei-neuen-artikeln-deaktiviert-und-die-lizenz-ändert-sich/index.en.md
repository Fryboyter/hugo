---
title: From the Editor's Desk - Comment function deactivated for new articles and the license changes
date: 2023-12-17T12:55:55+0100
nocomments: true
categories:
- Englis(c)h
tags:
- Comment function
- License
slug: from-the-editors-desk-comment-function-deactivated-for-new-articles-and-the-license-changes
---
I have been thinking about the comment function for some time now. In the end, I decided to deactivate it for new articles with immediate effect and until further notice. There are two main reasons for this. Myself. And some other users.

* I realize that I often approve comments with a great delay. For one thing, I sometimes don't have the time. Be it because I have a lot to do at work or because some things are simply more important to me. I also often don't want to deal with the comments themselves at the moment.

* The fact that I have less and less interest in dealing with this is partly due to the comments themselves. I would say that I don't even approve 80 percent of all comments anymore. For example, because they are spam that is only posted to link to dubious sites. Or comments in which other programs are recommended, even though I don't want an alternative. But also because more and more people are creating comments that seem to have deficits in dealing with third parties or in understanding the text.

Long story short, the situation annoys me. Although I can't say what bothers me more. But I would almost say it's the first reason.

I have therefore decided that no more comments will be possible on new articles (starting with this one) until further notice. Comments that are currently still in the queue will be processed in the next few days.

For existing articles, the comment function will remain active for the time being so that any existing comments are still displayed. I have not yet found a satisfactory solution as to how I can prevent new comments but display existing comments.

I can't yet say whether this will be a permanent decision. I may also decide to reactivate the comment function for certain articles only. Time will tell.

Regardless of this, I have decided to change the license of the code I created to display the website to the [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html). The articles themselves will continue to be published under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.de) license. The third-party code I use ([Isso](https://isso-comments.de), [Prism](https://prismjs.com) and [Atkinson Hyperlegible](https://brailleinstitute.org/freefont)) is of course not affected by the license change.