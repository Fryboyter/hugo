---
title: In eigener Sache - Kommentarfunktion bei neuen Artikeln deaktiviert und die Lizenz ändert sich
date: 2023-12-17T12:55:55+0100
nocomments: true
categories:
- OSBN
- Allgemein
tags:
- Kommentarfunktion
- Lizenz
slug: in-eigener-sache-kommentarfunktion-bei-neuen-artikeln-deaktiviert-und-die-lizenz-aendert-sich
---
Seit einiger Zeit habe ich mir Gedanken über die Kommentarfunktion gemacht. Schlussendlich bin ich zu dem Ergebnis gekommen, diese ab sofort und bis auf Weiteres bei neuen Artikeln zu deaktivieren. Das hat hauptsächlich zwei Gründe. Mich selbst. Und manche andere Nutzer.

* Ich merke, dass ich Kommentare oft sehr zeitverzögert freischalte. Zum einen, fehlt mir teilweise die Zeit dafür. Sei es nun, weil ich beruflich viel zu tun habe oder weil mir einfach manche Dinge wichtiger sind. Oftmals will ich mich derzeit auch nicht mehr mit den Kommentaren an sich beschäftigen.

* Dass ich immer weniger Lust habe, liegt teilweise an den Kommentaren an sich. Gefühlt würde ich sagen, dass ich 80 Prozent aller Kommentare gar nicht mehr freischalte. Zum Beispiel, weil sich um Spam handelt, der nur abgesetzt wird, um auf irgendwelche dubiosen Seiten zu verlinken. Oder Kommentare, in denen andere Programme empfohlen werden, obwohl ich gar keine Alternative will. Aber auch, weil immer mehr Leute Kommentare erstellen, die scheinbar Defizite im Umgang mit Dritten oder bezüglich des Textverständnisses haben.

Kurz gesagt, mich nervt die Situation. Wobei ich nicht sagen kann, was mich mehr stört. Ich würde aber fast schon sagen, der erste Grund.

Daher habe ich beschlossen, dass bis auf Weiteres bei neuen Artikeln (angefangen mit diesem) keine Kommentare mehr möglich sein werden. Kommentare, die sich aktuell noch in der Warteschleife befinden, werde ich in den nächsten Tagen bearbeiten.

Bei bereits bestehenden Artikeln wird die Kommentarfunktion erst einmal weiter aktiv bleiben, damit eventuell vorhandene Kommentare weiterhin angezeigt werden. Eine zufriedenstellende Lösung, wie ich neue Kommentare verhindern, aber bereits vorhandene Kommentare anzeigen kann, habe ich bisher noch nicht gefunden.

Ob dies eine dauerhafte Entscheidung sein wird, kann ich noch nicht sagen. Vielleicht entscheide ich mich auch dafür, die Kommentarfunktion nur für bestimmte Artikel wieder zu aktivieren. Die Zeit wird es zeigen.

Unabhängig davon habe ich mich entschieden, dass ich die Lizenz des von mir erstellten Codes zum Anzeigen der Internetseite auf die [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html) ändere. Die Artikel an sich werden weiterhin unter der [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.de) Lizenz veröffentlicht. Der von mir genutzte Code Dritter ([Isso](https://isso-comments.de), [Prism](https://prismjs.com) und [Atkinson Hyperlegible](https://brailleinstitute.org/freefont)) ist von dem Lizenzwechsel natürlich auch nicht betroffen. 