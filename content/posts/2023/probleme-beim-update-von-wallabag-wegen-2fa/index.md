---
title: Probleme beim Update von Wallabag wegen 2FA
date: 2023-06-23T14:50:14+0200
categories:
- OSBN
tags:
- Wallabag
- Update
slug: probleme-beim-update-von-wallabag-wegen-2fa
---
Ich nutze [Wallabag](https://wallabag.org) um damit bestimmte Internetseiten dauerhaft speichern zu können. Somit besteht nicht die Gefahr, dass ein Lesezeichen irgendwann nicht mehr erreichbar ist. Gestern wollte ich Wallabag auf Version 2.6.1 aktualisieren, da diese endlich mit einer aktuellen Version von Composer kompatibel ist, sodass ich auf einen Workaround verzichten kann, den ich seit einiger Zeit nutze.

Leider ist das Update immer mit der Fehlermeldung {{< mark >}}You have requested a non-existent service "scheb_two_factor.security.google_authenticator"{{< /mark >}} abgeschmiert.

Interessant daran ist, dass ich bei der betreffenden Installation gar keine 2FA ([Zwei-Faktor-Authentisierung](https://de.wikipedia.org/wiki/Zwei-Faktor-Authentisierung)) nutze.

Um Wallabag trotzdem aktualisieren zu können, muss man glücklicherweise nur eine kleine Änderung in der Datei {{< mark >}}app/config/parameters.yml{{< /mark >}} im Wallabag-Verzeichnis durchführen. In der Datei sucht man einfach nach {{< mark >}}twofactor_auth: false{{< /mark >}} und ändert false auf true. Führt man dann das Update mit {{< mark >}}make update{{< /mark >}} erneut aus, klappt es problemlos. Hinterher habe ich den Wert bei mir wieder auf false geändert, ohne dass es Probleme gegeben hat.

Das Problem wurde bereits unter https://github.com/wallabag/wallabag/issues/6649 gemeldet. Von dort habe ich auch den genannten Workaround, den man nutzen kann, bis der Bug offiziell behoben wurde.