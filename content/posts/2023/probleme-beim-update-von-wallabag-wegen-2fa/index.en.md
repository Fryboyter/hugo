---
title: Problems updating Wallabag because of 2FA
date: 2023-06-23T14:50:14+0200
categories:
- Englis(c)h
tags:
- Wallabag
- Update
slug: problems-updating-wallabag-because-of-2fa
---
I use [Wallabag](https://wallabag.org) to save specific internet pages permanently. This way there is no risk that a bookmark will be inaccessible at some point. Yesterday I wanted to update Wallabag to version 2.6.1 because it is finally compatible with a recent version of Composer, so I can do without a workaround I have been using for some time.

Unfortunately, the update always crashed with the error {{< mark >}}You have requested a non-existent service "scheb_two_factor.security.google_authenticator"{{< /mark >}}.

What's interesting about this is that I'm not using 2FA ([two-factor authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication)) at all on the installation in question.

Fortunately, to be able to update Wallabag anyway, all you have to do is make a small change in the {{< mark >}}app/config/parameters.yml{{< /mark >}} file in the Wallabag directory. In the file you simply search for {{< mark >}}twofactor_auth: false{{< /mark >}} and change false to true. If you then run the update again with {{< mark >}}make update{{< /mark >}}, it works without problems. Afterwards I changed the value back to false without any problems.

The problem was already reported at https://github.com/wallabag/wallabag/issues/6649. From there I also have the mentioned workaround, which you can use until the bug is officially fixed.