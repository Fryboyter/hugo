---
title: Gefälschte USB-Sticks erkennen
date: 2023-01-04T20:49:07+0100
categories:
- OSBN
tags:
- USB-Stick
- Fälschung
slug: gefaelschte-usb-sticks-erkennen
---
Letzte Woche hat mich ein Bekannter um Hilfe gebeten. Er hat Daten auf seinen neuen USB-Stick verschoben, was ohne Fehlermeldung funktioniert hat. Allerdings waren einige Daten danach nicht auf dem Stick vorhanden. Backups sei Dank hat er das mehrmals mit dem gleichen Ergebnis probiert.

Noch bevor ich mir den Stick überhaupt angesehen habe, habe ich schon vermutet, dass er sich einen gefälschten USB-Stick angeschafft hat, bei dem mehr vorhandener Speicherplatz angezeigt wird als tatsächlich vorhanden ist.

Um meine Vermutung belegen zu können, habe ich mir das Tool F3 (https://github.com/AltraMayor/f3) installiert und den Stick mittels {{< mark >}}f3probe --destructive --time-ops /dev/sdX{{< /mark >}} getestet. Und siehe da, der Stick hat tatsächlich deutlich weniger Speicherplatz als angegeben. Es handelt sich daher klar um eine Fälschung.

Von daher, testet neu gekaufte USB-Sticks unbedingt bevor ihr sie das erste mal nutzt. Denn auch ein Kauf bei einem bekannten Händler ist nicht unbedingt eine Garantie, dass es sich wirklich um ein Original handelt.

Neben f3probe besteht F3 noch aus weiteren Befehlen. 

Mit f3write werden mehrere 1 GB große Dateien auf den USB-Stick geschrieben bis kein Speicherplatz mehr vorhanden ist. Mit f3read werden diese anschließend gelesen. Neben dem Test, ob der Stick tatsächlich über den angegebenen Speicherplatz verfügt, wird hierbei somit noch die Lese- und Schreibgeschwindigkeit getestet. Dies dauert allerdings deutlich länger als f3probe.

Hat man einen gefälschten USB-Stick und kann oder will man diesen nicht beim Händler reklamieren, kann man den Befehl f3fix nutzen. Damit lässt sich eine Partition erstellen, die dem tatsächlich vorhanden Speicherplatz entspricht. Somit ist eine Nutzung zumindest halbwegs sicher möglich.

Wer lieber eine grafische Oberfläche will, kann noch zusätzlich https://github.com/zwpwjwtz/f3-qt installieren. 