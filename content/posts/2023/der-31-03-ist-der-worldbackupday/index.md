---
title: Der 31.03. ist der World Backup Day
date: 2023-03-31T15:13:32+0200
categories:
- OSBN 
tags:
- Backup
- Borg
slug: der-31-03-ist-der-world-backup-day
---
Der 31.03. ist jedes Jahr ein besonderer Tag. Nämlich der [World Backup Day](https://www.worldbackupday.com/de).

Hierbei handelt es sich um einen Gedenktag, bei dem es um Backups geht. Also das, was viele Nutzer nie oder nur unregelmäßig erstellen und sie sich wünschen sie hätten es, wenn Daten, aus welchen Gründen auch immer, verloren gegangen sind.

**Daher nutze ich heute diesen Tag und rufe meine Leser auf, regelmäßig Backups zu erstellen.**

Ich selbst nutze für meine Backups das Tool [Borg](https://www.borgbackup.org). Unter anderem, weil man sich Dank der Deduplikation und der Komprimierung Speicherplatz sparen kann. Und weil aufgrund der standardmäßigen Verschlüsselung es kein Problem ist, Datensicherungen auch in der sogenannten Cloud zu speichern. Aber welches Programm man für die Backups verwendet ist im Grunde egal, solange man überhaupt Backups erstellt.

Meine Backups sichere ich lokal auf externe Festplatten. Die wirklich wichtigen Daten sichere ich zusätzlich noch bei rsync.net und wenn ich dieses Wochenende endlich mal dazu komme auch noch in einer Storage Box von Hetzner. Verschlüsselt, wie bereits angemerkt. In meinem Fall mit einer [Diceware](https://theworld.com/~reinhold/diceware.html)-Passphrase und einer Schlüsseldatei die nur lokal vorhanden ist.

Was Backups betrifft, möchte ich noch zwei Dinge anmerken. 

Ein Backup muss immer auf einem anderen Datenträger gespeichert werden. Denn sonst sind nach einem Defekt des Speichermediums sowohl die Daten an sich als auch die Sicherung verloren. Daher halte ich wenig davon, dass [Timeshift](https://github.com/linuxmint/timeshift) immer öfters als Backup empfohlen bzw. bezeichnet wird. Denn in der Standardkonfiguration werden die Snapshots im Verzeichnis /timeshift auf der Root-Partition und somit in der Regel auf der gleichen Festplatte gespeichert. Timeshift ist daher aus meiner Sicht ein gutes Tool um beispielsweise nach einem fehlerhaften Update den ursprünglichen Zustand schnell wiederherstellen zu können. Aber es ist kein Backup, sondern eine Kopie der Daten.

Und mit dem Anlegen eines Backups ist es nicht getan. Man sollte regelmäßig testen, ob man Dateien aus dem Backup wiederherstellen kann. Macht man das nicht, hat man kein richtiges Backup.

Wer sich nun für Borg interessiert, aber eine grafische Oberfläche benötigt, kann sich [Vorta](https://vorta.borgbase.com) oder [Pika](https://gitlab.gnome.org/World/pika-backup) ansehen. Für die Erweiterung bzw. Verbesserung von Borg lohnt es sich meiner Meinung nach zudem sich den Wrapper [Borgmatic](https://torsion.org/borgmatic/) anzusehen.

Zugegeben, der Artikel ist etwas sehr "borg-lastig" ausgefallen. Wenn ihr andere Tools für Backups nutzt, ignoriert das einfach. Wichtig ist nur, dass ihr regelmäßig richtige Backups erstellt.