---
title: Ältere Kernel unter CentOS deinstallieren
date: 2023-08-08T00:46:59+0200
categories:
- OSBN
tags:
- CentOS
- Kernel
slug: aeltere-kernel-unter-cent-os-deinstallieren
---
CentOS ist von Haus aus so konfiguriert, dass durch Updates bis zu 5 Kernel-Versionen installiert sein können. Für mich ist das viel zu viel. Wie kann man also ausmisten? 

CentOS bietet beispielsweise den Befehl package-cleanup an, welcher Teil des Pakets yum-utils ist. Führt man {{< mark >}}package-cleanup &hyphen;&hyphen;oldkernels &hyphen;&hyphen;count=2{{< /mark >}} aus, werden alle installierten Kernel-Versionen bis auf die letzten zwei deinstalliert. Das ist somit etwas einfacher als die jeweiligen Versionen manuell mit yum zu deinstallieren.

Soweit so gut. Was aber, wenn man immer nur maximal 2 Kernel-Versionen installiert haben will? Muss man jedes Mal nach einem Kernel-Update package-cleanup ausführen? Nicht unbedingt. In der Datei /etc/yum.conf kann man auch fest einstellen wie viele Kernel-Versionen vorgehalten werden. Hierfür muss man die Zeile {{< mark >}}installonly_limit=5{{< /mark >}} einfach entsprechend ändern.