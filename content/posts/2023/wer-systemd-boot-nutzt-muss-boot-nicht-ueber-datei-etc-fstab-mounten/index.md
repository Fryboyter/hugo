---
title: Wer systemd-boot nutzt, muss /boot nicht über die Datei /etc/fstab mounten
date: 2023-08-25T19:13:29+0200
categories:
- OSBN
tags:
- Systemd-boot
- Fstab
slug: wer-systemd-boot-nutzt-muss-boot-nicht-ueber-die-datei-etc-fstab-mounten
---
Schon vor Jahren habe ich es mir angewöhnt für /boot eine extra Partition anzulegen. Somit habe ich in der Datei /etc/fstab einen extra Eintrag für /boot. Aber warum eigentlich?

Kürzlich bin ich im Internet über die Aussage gestoßen, dass ein Eintrag von /boot in der Datei /etc/fstab nicht nötig ist, wenn man systemd-boot nutzt.

Da ich systemd-boot nutze, habe ich kurzerhand einfach mal auf meinem Notebook den betreffenden Eintrag aus der Datei entfernt und neu gebootet. 

Was auch ohne Probleme funktioniert hat. Systemd-boot benötigt in der Tat keinen extra Eintrag in der Datei /etc/fstab, wenn /boot auf einer extra Partition liegt.

Welche Vorteile hat dies nun? Eigentlich nur, dass in /etc/fstab ein Eintrag weniger vorhanden ist. Da man sich aber bei diesen leicht vertippen kann, würde ich das durchaus als Vorteil werten. Aber nur als einen kleinen.

Hat das Ganze Nachteile? Bisher konnte ich noch keinen entdecken. So habe ich beispielsweise mittels {{< mark >}}mkinitcpio -P{{< /mark >}} neue initrd (initial ramdisks) erstellt, welche in /boot gespeichert werden. Das hat problemlos funktioniert.