---
title: Helix Editor
date: 2023-10-28T16:04:41+0200
categories:
- OSBN
tags:
- Helix
- Editor
slug: helix-editor
---
Wenn es um sogenannte modale Editoren geht, dürften aktuell [Vim](https://www.vim.org) und [Neovim](https://neovim.io) wohl die bekanntesten Vertreter ihrer Art sein. [Kakoune](https://kakoune.org) hingegen dürfte eher unbekannt sein.

Nach unzähligen Versuchen mit Vim in den letzten Jahrzehnten habe ich solche Editoren gemieden wie der sprichwörtliche Teufel das Weihwasser. Die Bedienung war mir einfach zu fremd. Und ich nutze einen Editor einfach zu selten, um mich daran zu gewöhnen. Ganz zu schweigen davon, dass ich nur einen Bruchteil des Funktionsumfangs benötige den Vim oder Neovim bietet.

Vor Längerem wurde ich dennoch auf den Editor [Helix](https://helix-editor.com) aufmerksam. Da es sich hierbei ebenfalls um einen modalen Editor (also um einen Editor, dessen Bedienung in unterschiedliche Modi unterteilt ist) handelt, hatte ich ihn aus genannten Gründen ignoriert. Sorry aber jeder der noch alle Tassen im Schrank hat, will doch keine neue Sprache lernen nur um einen Editor bedienen zu können.

In letzter Zeit wurde bei Diskussionen zum Thema Editoren ab und zu Helix genannt. Und das wohl auch von (ehemaligen) Nutzern von Vim bzw. Neovim die inzwischen Helix nutzen. Das hat dann doch mein Interesse geweckt. Daher habe ich mir vor einigen Tagen Helix installiert und mich damit so objektiv beschäftigt wie es mir möglich ist.

Der wohl größte Unterschied zwischen Helix und beispielsweise vim ist, dass bei der Bedienung erst ausgewählt wird und dann eine Aktion erfolgt ([https://docs.helix-editor.com/from-vim.html](https://docs.helix-editor.com/from-vim.html)). Bei vim ist es genau anders herum. Nur diese "kleine" Änderung bewirkt bei mir, dass ich mit Helix deutlich schneller und besser zurechtkomme. Ich habe wohl auch nicht alle Tassen im Schrank.

Wer sich mit Vim / Neovim auskennt und dessen Befehle mit denen von Helix vergleicht, wird schnell das Argument vorbringen, dass die Befehle von Helix oft länger sind. Und ja, diese Person hat recht. Aus meiner Sicht sind aber die Befehle von Helix oft eingängiger und somit leichter zu merken, sodass es mir egal ist, ob ein Befehl nun 3 oder 4 Zeichen lang ist. Vor allem, wenn ich für den Befehl mit 3 Zeichen länger überlegen muss als für den mit 4.

Mittels [LSP](https://en.wikipedia.org/wiki/Language_Server_Protocol) kann man zudem die Unterstützung zusätzlicher Programmiersprachen leicht nachrüsten ([https://docs.helix-editor.com/lang-support.html](https://docs.helix-editor.com/lang-support.html)). Ansonsten bietet Helix viele Funktionen bereits "out of the box". Die Entwickler bezeichnen es auch als "batteries included".

Helix hat aber auch Nachteile. So gibt es noch kein Plugin-System. An diesem wird aktuell noch gearbeitet. Und Helix bietet auch nicht den gesamten Funktionsumfang von Vim. Aber die erste Version von Helix wurde 2021 veröffentlicht. Die von Vim 1991. Dafür, dass Helix so jung ist, finde ich Helix bereits sehr ausgereift.

Abschließend zur Frage, die in solche einem Fall oft von Nutzern von Vim / Neovim gestellt wird. Warum sollte ich Helix nutzen? Ich kann nicht für die Entwickler von Helix sprechen. Aber meine persönliche Antwort wäre, dass Nutzer die mit Vim / Neovim zufrieden sind Helix gar nicht nutzen sollen. Helix will meiner Meinung nach auch gar nicht eine bessere Version von Vim / Neovim sein. Daher gibt es offiziell auch keine entsprechenden Keybindings. 

Helix will einfach ein eigenständiger modaler Editor sein. Und ausgehend von mir selbst, ist der Einstieg in einen modalen Editor mit Helix einfach einfacher als mit Vim / Neovim. Habe ich damit objektiv gesehen recht? Vielleicht. Mir geht es auch nicht darum, welcher Editor "besser" ist. Das lässt sich objektiv sowieso nie beantworten. Wie heißt es in Köln so schön? Jeder Jeck is anders. Von daher ist es für mich auch absolut in Ordnung, wenn jemand einen Editor ohne Modi, wie beispielsweise [micro](https://micro-editor.github.io) nutzt.

Ach ja, vor ein paar Tagen wurde [Helix 23.10](https://helix-editor.com/news/release-23-10-highlights/) veröffentlicht.