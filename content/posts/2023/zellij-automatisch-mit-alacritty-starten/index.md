---
title: Zellij automatisch mit Alacritty starten
date: 2023-12-22T14:15:31+0100
nocomments: true
categories:
- OSBN
tags:
- Zellij
- Alacritty
slug: zellij-automatisch-mit-alacritty-starten
---
Als ich vor einigen Wochen das erste Mal [Zellij](https://zellij.dev) ausprobiert habe, habe ich erst den Terminal Emulator an sich und dann Zellij gestartet. Das ist mir inzwischen zu umständlich, da ich derzeit fast nur noch mit Zellij arbeite.

Der Terminal Emulator [Alacritty](https://alacritty.org) bietet hierfür eine recht einfache Konfigurationsmöglichkeit, die es ermöglicht beispielsweise direkt nach dem Starten Zellij auszuführen.

In der Konfigurationsdatei ~/.config/alacritty/alacritty.yml gibt es den Bereich, der sich auf die verwendete Shell bezieht. In meinem Fall ungefähr ab Zeile 405 (da die Konfigurationsdatei auch gleichzeitig als deren Dokumentation dient, ist die Datei ziemlich lang). Diesen habe ich wie folgt geändert.

{{< highlight bash >}}
shell:
  program: /bin/zsh
  args:
    - -c
    - zellij attach -c Notebook
{{< /highlight >}}

In der zweiten Zeile gibt man die Shell an, die verwendet werden soll. In meinem Fall also die ZSH. Das erste Argument {{< mark >}}-c{{< /mark >}} bedeutet im Falle der ZSH, dass das nächste Argument als Befehl ausgeführt wird. Mit der nächsten Zeile wird Zellij gestartet und eine Verbindung zur Session mit der Bezeichnung Notebook aufgebaut. Ist diese nicht vorhanden, wird die Session erstellt. 

Der Name ist frei wählbar. In meinem Fall lautet er Notebook, da ich gerade diesen Artikel mit meinem Notebook schreibe. Man kann {{< mark >}}attach -c Notebook{{< /mark >}} auch weglassen. Dann wählt Zellij die Bezeichnung der Session allerdings automatisch, sodass Namen wie {{< mark >}}adamant-capsicum{{< /mark >}} oder schlimmere dabei herauskommen.