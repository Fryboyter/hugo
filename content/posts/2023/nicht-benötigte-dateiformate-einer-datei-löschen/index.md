---
title: Nicht benötigte Dateiformate einer Datei löschen
date: 2023-03-12T18:29:06+0100
categories:
- OSBN
tags:
- ZSH
slug: nicht-benoetigte-dateiformate-einer-datei-loeschen
---
Nehmen wir an, dass in einem Verzeichnis Dateien mit gleichem Inhalt aber in unterschiedlichen Dateiformaten vorliegen. Also beispielsweise Datei.txt, Datei.epub und Datei.pdf. Oftmals benötigt man die jeweilige Datei aber nur in einem bestimmten Format.

Natürlich kann man die nicht benötigten Dateien manuell löschen. Einfacher wäre es aber, wenn man automatisch alle Dateien bis auf die, die man behalten will, löschen kann. Will man beispielsweise nur die EPUB-Dateien behalten kann man dies, zumindest mit der [ZSH](https://www.zsh.org), mit dem Befehl {{< mark >}}rm &hyphen;&hyphen; ^*.epub{{< /mark >}} machen. Hiermit werden alle Dateien bis auf die mit der Dateiendung .epub gelöscht. Damit der Befehl funktioniert muss "extended glob" aktiviert sein. Entweder indem man vorher den Befehl {{< mark >}}setopt EXTENDED_GLOB{{< /mark >}} ausführt bzw. diesen in eine Konfigurationsdatei der ZSH einträgt.

Ob der Befehl auch mit einer anderen Shell wie der Bash funktioniert, kann ich nicht garantieren.