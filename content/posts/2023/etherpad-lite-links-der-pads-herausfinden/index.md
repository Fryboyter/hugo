---
title: Etherpad Lite - Links der Pads herausfinden
date: 2023-05-23T00:57:01+0200
categories:
- OSBN
tags:
- MySQL
- Etherpad Lite
- HedgeDoc
slug: etherpad-lite-links-der-pads-herausfinden
---
Es gibt bei einem Projekt die Überlegung, ob man von [Etherpad Lite](https://github.com/ether/etherpad-lite) auf [HedgeDoc](https://hedgedoc.org) umsteigt. Bei der Umstellung sollen ggf. alle Inhalte übernommen werden. Und die bisherigen Links auf die jeweiligen Pads sollen entsprechend auf HedgeDoc weiterleiten. 

Also habe ich mir überlegt, wie man vorgehen könnte. Die Links der Pads würde ich aus der Datenbank von Etherpad Lite exportieren. Was sich allerdings als keine allzu gute Idee herausgestellt hat. Denn es wird eine MariaDB-Datenbank verwendet, die sozusagen als [Key-Value-Store](https://de.wikipedia.org/wiki/Schl%C3%BCssel-Werte-Datenbank) genutzt wird. Was zur Folge hat, dass es nur eine Tabelle mit zwei Spalten gibt. Und in dieser Tabelle findet man beispielsweise folgende Einträge.

{{< highlight bash >}}
pad:faq
{{< /highlight >}}

{{< highlight bash >}}
{"atext":{"text":"Welcome to Etherpad!\n\nThis pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!\n\nGet involved with Etherpad at https://etherpad.org\n\n","attribs":"|6+6d"},"pool":{"numToAttrib":{},"nextNum":0},"head":0,"chatHead":-1,"publicStatus":false,"passwordHash":null,"savedRevisions":[]}
{{< /highlight >}}

Um die Namen der vorhandenen Pads aus der Datenbank auszulesen, kann man folgende Abfrage nutzen.

{{< highlight sql >}}
SELECT 
	DISTINCT SUBSTRING(store.key, 5, LOCATE(":", store.key, 5)-5) AS pads
FROM
	store
WHERE
	store.key LIKE "pad:%"
{{< /highlight >}}

Diese Abfrage zeigt aber tatsächlich nur den Namen der Pads an. Zum Beispiel {{< mark >}}faq{{< /mark >}}. Aber bessere wäre es, wenn der gesamte Link ausgegeben wird. Also um bei dem Beispiel zu bleiben {{< mark >}}https://ep.domain.de/p/faq{{< /mark >}}. Da aber die Domain nicht in der Datenbank gespeichert ist, muss man etwas kreativ werden. Was aber in diesem Fall keine große Herausforderung ist.

{{< highlight sql >}}
SELECT 
	DISTINCT CONCAT("https://ep.domain.de/p/", SUBSTRING(store.key, 5, LOCATE(":", store.key, 5)-5)) AS pad
FROM
	store
WHERE
	store.key LIKE "pad:%"
{{< /highlight >}}

Mittels CONCAT wird im Grunde nur https://ep.domain.de/p/ vor dem jeweiligen Namen der Pads eingefügt, sodass schlussendlich richtige Links angezeigt werden, die man weiterverarbeiten kann.
