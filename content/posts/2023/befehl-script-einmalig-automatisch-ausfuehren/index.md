---
title: Befehl / Script einmalig automatisch ausführen
date: 2023-07-11T11:50:49+0200
categories:
- OSBN
tags:
- Cronjob
- Timer
slug: befehl-script-einmalig-automatisch-ausfuehren
---
Ab und zu kommt es vor, dass man einen bestimmten Befehl bzw. ein Script einmalig zu einem bestimmten Zeitpunkt ausführen will. Zum Beispiel um einen bestimmten Artikel Mitten in der Nacht zu veröffentlichen. Hierfür könnte man natürlich einen Cronjob / systemd Timer anlegen und wieder löschen, wenn er ausgeführt wurde. Oder man nutzt einfach den Befehl {{< mark >}}at{{< /mark >}}.

At ist im Grunde dafür gedacht, einen Befehl bzw. ein Script zu einem späteren Zeitpunkt einmalig auszuführen. Nicht mehr und nicht weniger. Also genau das was man in diesem Fall will.

Um beispielsweise das Script ~/bin/lirpa.sh heute um 20 Uhr auszuführen, kann man folgenden Befehl nutzen.

{{< highlight bash >}}
at -t 202307112000 -f ~/bin/lirpa.sh
{{< /highlight >}}

Das in dem Beispiel verwendet Datumsformat besteht aus der Jahreszahl, dem Monat, dem Tag und der Uhrzeit in Form von Stunde und Minute. Der Befehl at versteht aber auch andere Formate wie beispielsweise 2023/07/11/20/00, 11.07.23, 20:00 PM, oder 2000. Auch einige "Platzhalter" wie noon, now oder tomorrow werden unterstützt. Somit wäre beispielsweise auch eine zeitliche Angabe wie {{< mark >}}20:00 PM next month{{< /mark >}} oder {{< mark >}}now + 30 minutes{{< /mark >}} möglich. At ist daher auch ziemlich flexibel einsetzbar.

At hat auch noch einen Vorteil. Um damit "Cronjobs" zu erstellen, benötigt man nur Benutzerrechte. Somit ist man natürlich auch auf die jeweiligen Benutzerrechte eingeschränkt.