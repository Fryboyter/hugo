---
title: Execute command / script automatically once
date: 2023-07-11T11:50:49+0200
categories:
- Englis(c)h
tags:
- Cronjob
- Timer
slug: execute-command-script-automatically-once
---
From time to time it happens that you want to execute a certain command or script once at a certain time. For example, to publish a certain article in the middle of the night. For this you could of course create a cronjob / systemd timer and delete it when it was executed. Or you can simply use the command {{< mark >}}at{{< /mark >}}.

At is basically meant to execute a command or script only once at a later point in time. No more and no less. So exactly what you want in this case.

For example, to run the script ~/bin/lirpa.sh today at 8pm, you can use the following command.

{{< highlight bash >}}
at -t 202307112000 -f ~/bin/lirpa.sh
{{< /highlight >}}

The date format used in the example consists of the year, the month, the day and the time in the form of hours and minutes. However, the at command also understands other formats such as 2023/07/11/20/00, 11/07/23, 8:00 PM, or 2000. Some "placeholders" such as noon, now, or tomorrow are also supported. Thus, for example, a temporal specification like {{< mark >}}20:00 PM next month{{< /mark >}} or {{< mark >}}now + 30 minutes{{< /mark >}} would also be possible. At is therefore also quite flexible to use.

At also has another advantage. To create "cronjobs" with it, you only need user rights. Thus one is naturally also limited to the respective user rights.