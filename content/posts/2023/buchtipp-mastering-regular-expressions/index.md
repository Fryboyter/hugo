---
title: "Buchtipp: Mastering Regular Expressions"
date: 2023-03-05T14:12:25+0100
categories:
- OSBN
tags:
- RegEx
- Buch
slug: buchtipp-mastering-regular-expressions
---
Seit Jahren waren "Regular Expressions" ([Reguläre Ausdrücke](https://de.wikipedia.org/wiki/Regul%C3%A4rer_Ausdruck)) ein Buch mit sieben Siegeln für mich. Selbst nachdem ich mehrere Anleitungen gelesen haben, wurde es nicht viel besser.

Kürzlich bin ich auf das Buch "[Mastering Regular Expressions: Understand Your Data and Be More Productiv](https://www.oreilly.com/library/view/mastering-regular-expressions/0596528124/)" des Autors Jeffrey E. F. Friedl aufmerksam geworden. 

Der Autor schafft es meiner Meinung nach reguläre Ausdrücke so zu erklären, dass ich selbst nach wenigen Seiten schon mehr gelernt habe als mir andere Anleitungen vermitteln konnten. Wer sich daher mit dem Thema beschäftigen will oder muss, sollte sich das Buch einmal ansehen. Veröffentlicht wurde das Buch über den O'Reilly Verlag.

Die dritte und somit aktuellste Edition wurde zwar bereits 2006 veröffentlicht aber da sich seit dem wenig bis gar nichts geändert haben sollte, ist das Buch aus meiner Sicht immer noch aktuell.