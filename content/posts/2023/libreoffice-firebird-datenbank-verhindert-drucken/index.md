---
title: LibreOffice - Firebird-Datenbank verhindert drucken
date: 2023-07-15T15:31:27+0200
categories:
- OSBN
tags:
- LibreOffice
- Firebird
- HSQL
slug: libreoffice-firebird-datenbank-verhindert-drucken
---
Jedes Jahr erstelle ich zwischen Juli und November 2 bis 3 Serienbriefe die immer an die gleichen Leute geschickt werden. Die letzten Jahre hat das immer wunderbar funktioniert. Dieses Jahr aber nicht.

Zum Erstellen eines Serienbriefs nutze ich LibreOffice. Somit sind auch die Daten der Empfänger in einer Datenbank gespeichert die mit LibreOffice Base erstellt wurde.

Den Serienbrief drucke ich nicht direkt, sondern exportiere ihn als PDF-Datei, die ich dann an jemanden weiterleite. Dieser überprüften diesen dann und druckt ihn dann auch aus, wenn alles stimmt.

Und genau das Erzeugen hat dieses Jahr nicht mehr funktioniert. Ich hatte, wie gewohnt, den Brief geschrieben und als .odt-Datei abgespeichert. Als ich daraus dann die einzelnen Briefe an die Empfänger in Form einer einzelnen PDF-Datei erstellen wollte, ist mir LibreOffice immer abgestürzt. Es gab weder eine aussagekräftige Fehlermeldung noch sonst einen Hinweis, was das Problem ist. Wie ich das hasse...

Irgendwann hatte ich dann gemerkt, dass es sich bei der Datenbank um eine Firebird-Datenbank handelt. Dunkel konnte ich mich daran erinnern, dass LibreOffice mich vor einiger Zeit genervt hat, dass ich die vorhandene Adress-Datenbank zu einer Firebird-Datenbank umwandeln soll. Neuer Standard. Viel besser. Oder was auch immer der Grund war. Vermutlich hatte ich irgendwann nachgegeben und die Datenbank umgewandelt. Und genau das war auch das Problem.

Als ich mit meinem Latein am Ende war, habe ich heute eine ältere Version der Datenbank aus einem Backup wiederherstellt, die noch als HSQLDB (HyperSQL DataBase) erstellt wurde. Damit konnte ich problemlos eine PDF-Datei erzeugen. 

Somit kommt eigentlich nur die Firebird-Datenbank als Grund des Problems infrage. Um sicherzugehen, habe ich unter LibreOffice Base eine neue HSQL-Datenbank erstellt und in diese den Inhalt der Firebase-Datenbank kopiert, damit ich die aktuellen Empfänger des Serienbriefs habe. Und was soll ich sagen? Das Erstellen des Serienbriefs hat wieder funktioniert.

Warum die Firebird-Datenbank Probleme gemacht hat, kann ich mangels Fehlermeldungen leider nicht sagen. In LibreOffice Base konnte ich sie ganz normal nutzen. Somit dürfte die Datenbank an sich wohl nicht defekt sein.

Wegen des Problems habe ich mir übrigens auch gestern OnlyOffice angeschaut. Die Serienbrieffunktion gibt es nur, wenn man die Online-Version nutzt ([https://helpcenter.onlyoffice.com/de/onlyoffice-editors/onlyoffice-document-editor/usageinstructions/usemailmerge.aspx](https://helpcenter.onlyoffice.com/de/onlyoffice-editors/onlyoffice-document-editor/usageinstructions/usemailmerge.aspx). Ähm, danke. Aber nein danke.