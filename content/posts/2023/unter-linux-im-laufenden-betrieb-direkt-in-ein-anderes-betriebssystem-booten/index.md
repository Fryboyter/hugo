---
title: Unter Linux im laufenden Betrieb direkt in ein anderes Betriebssystem booten
date: 2023-07-26T20:25:30+0200
categories:
- OSBN
tags:
- Systemd-boot
- Linux
slug: unter-linux-im-laufenden-betrieb-direkt-in-ein-anderes-betriebssystem-booten
---
Im Jahre 2014 hatte ich bereits einen [Artikel](/bestimmten-eintrag-des-bootloaders-automatisch-ausfuehren/) veröffentlicht, in dem ich beschrieben habe, wie man mittels syslinux bzw. extlinux den Rechner neu startet und automatisch ein bestimmtes Betriebssystem bootet. Die letzte Version von syslinux wurde allerdings ebenfalls 2014 veröffentlicht. Zudem hat syslinux einige Nachteile. Und die offizielle Internetseite ist im Moment auch nicht erreichbar. 

Da ich nun seit einigen Jahren systemd-boot als Bootloader nutze, ist besagter Artikel auch nicht mehr nützlich für mich. Aber systemd-boot bietet eine vergleichbare Möglichkeit, um direkt aus einer laufenden Linux-Distribution heraus neu zu starten und automatisch ein anderes Betriebssystem zu booten. 

Ich gehe davon aus, dass systemd-boot bereits so konfiguriert ist, dass man manuell im Bootmenü das gewünschte Betriebssystem auswählen kann. Wer sich noch nicht damit beschäftigt hat, kann sich https://wiki.archlinux.org/title/systemd-boot durchlesen.

Als Erstes führt man {{< mark >}}bootctl list{{< /mark >}} aus. Dieser Befehl sollte alle vorhandenen Einträge von systemd-boot anzeigen. Diese Einträge könnten beispielsweise wie folgt aussehen.

{{< highlight bash >}}
title: Arch Linux
id: arch.conf
source: /boot/efi/loader/entries/arch.conf
linux: /EFI/arch/grubx64.efi
options: root=UUID=12345678-1234-1234-1234-123456789123 ro loglevel=0 splash

title: Windows Boot Manager
id: auto-windows
source: /sys/firmware/efi/efivars/LoaderEntries-87654321-4321-4321-4321-987654321987
{{< /highlight >}}

Wichtig ist die hierbei die Zeile die mit "id" beginnt, da man mit dieser id systemd-boot mitteilt welches Betriebssystem gebootet werden soll. Hat man beispielsweise Arch Linux gestartet und will nun direkt Windows booten, reicht hierfür der Befehl {{< mark >}}systemctl reboot &hyphen;&hyphen;boot-loader-entry=auto-windows{{< /mark >}} aus und schon wird der Rechner neu gestartet und Windows gebootet. In meinem Fall wird allerdings 4 Sekunden lang das Bootmenü von systemd-boot angezeigt bis das Betriebssystem tatsächlich gebootet wird. Das liegt daran, dass der Befehl die allgemeine Konfiguration von systemd-boot berücksichtigt in der ich eine Wartezeit von 4 Sekunden definiert habe, damit ich beim manuellen Booten den gewünschten Eintrag des Bootloaders auswählen kann. Um diese Wartezeit zu verkürzen, kann man den genannten Befehl noch um {{< mark >}}&hyphen;&hyphen;boot-loader-menu=1{{< /mark >}} erweitern, sodass nur eine Sekunde gewartet wird. Ein kleinerer Wert ist scheinbar nicht möglich. Setzt man den Wert auf 0, wird so lange gewartet bis man den Eintrag manuell bestätigt.

Umgekehrt funktioniert es allerdings nicht. Wenn man Windows gebootet hat, gibt es meines Wissens nach keine Möglichkeit systemd-boot anzuweisen direkt ein anderes Betriebssystem zu booten. Somit ist das eine reine Linux-Lösung für Installationen mit systemd-boot.

Dem einen oder anderen wird bereits aufgefallen sein, dass sich beim Eintrag von Arch Linux auf die Konfigurationsdatei arch.conf bezogen wird aber im Falle von Windows die id nur "auto-windows" lautet. Das liegt daran, dass systemd-boot normalerweise vorhandene Windows-Installationen automatisch erkennt. Somit gibt es auch keine extra Konfigurationsdatei, die man anlegen muss. Das ist nur in besonderen Fällen nötig, wenn man von der Standardkonfiguration abweichen will.