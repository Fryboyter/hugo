---
title: Ja, die sogenannte Cloud ist für Backups geeignet
date: 2023-08-12T13:18:53+02:00
categories:
- OSBN
tags:
- Backup
- Cloud
slug: ja-die-sogenannte-cloud-ist-für-backups-geeignet
---
Zumindest, wenn man es richtig macht. Kürzlich gab es mal wieder eine Diskussion, bei der es um Backups ging. Ein Teilnehmer war felsenfest davon überzeugt, dass Backups nicht in der sogenannten Cloud gespeichert werden dürfen. Genannt wurden folgende Gründe.

**Was, wenn der Cloud-Anbieter die Daten löscht bzw. seinen Dienst ohne Vorwarnung komplett einstellt?**

Eine Datensicherung in der Cloud sollte nie die einzige Datensicherung sein. Die Regeln des sogenannten 3-2-1 Backups besagen, dass die Daten insgesamt 3 Mal vorhanden sein sollen. Also einmal die Daten die man normal nutzt. Dann einmal in Form eines lokalen Backups auf beispielsweise einer externen Festplatte. Und einmal außer Haus. 

**Die Daten sind bei einem Cloud-Anbieter nicht sicher.**

Ich nutze für die Backups das Programm [Borg](https://www.borgbackup.org). Das Programm verschlüsselt die Daten von Haus aus. Um die Daten wieder zu entschlüsseln, benötigt man in meinem Fall ein Passwort sowie eine Schlüsseldatei. Beides ist lokal gespeichert. Das also ein Cloud-Anbieter das Passwort kennt und Zugriff auf die Schlüsseldatei hat, ist relativ unwahrscheinlich. Auch halte ich es für relativ unwahrscheinlich, dass Anbieter wie rsync.net überhaupt auf die Daten zugreifen wollen. Der Chef von rsync.net hat beispielsweise schon mehrmals dazu geraten die Daten lokal zu verschlüsseln und erst dann hochzuladen.

**Externe Datenträger sind ausreichend.**

Nein sind sie nicht. Was bringt zum Beispiel eine externe Festplatte, die neben dem Computer liegen, wenn das Haus abbrennt? Vor einigen Jahren ist bei mir in Sichtweite ein Teil eines Bauernhofs abgebrannt. Bis alles so weit gelöscht war, dass nur noch die Brandwache vor Ort war, ist die ganze Nacht vergangen. Und selbst diese hat noch nach gelöscht. Das überlebt kein handelsüblicher Datenträger.

**Es gibt bessere Alternativen wie ein Bankschließfach.**

Bei der Bank, bei der ich mein Konto habe, ist kein Bankschließfach frei und die Warteliste ist laut einem Mitarbeiter sehr lang. Natürlich kann man alternativ auch eine externe Festplatte bei einem Verwandten oder einem guten Freund abgeben. Aber in beiden Fällen müsste ich regelmäßig den Datenträger austauschen, damit immer ein aktuelles Backup außer Haus vorhanden ist. Man hat also mehr Aufwand. Und auch in diesem Fall würde ich die Daten verschlüsseln. Zumal man bei Borg die Verschlüsselung nicht aktivieren, sondern bewusst deaktivieren muss.

**Mein persönliches Fazit**

Aus meiner Sicht spricht somit mehr für die Cloud, wenn es um Backups geht als dagegen. Sofern sie einen Teil der Backup-Strategie darstellt und die Daten lokal verschlüsselt und erst dann hochgeladen werden.