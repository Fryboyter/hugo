---
title: Version der Mikroarchitektur einer CPU herausfinden
date: 2023-03-12T13:48:30+0100
categories:
- OSBN
tags:
- Mikroarchitektur
- CPU
slug: version-der-mikroarchitektur-einer-cpu-herausfinden
---
Die unterschiedlichen [Versionen der Mikroarchitektur einer CPU](https://en.wikipedia.org/wiki/X86-64#Microarchitecture_levels) bieten unterschiedliche Optimierungen während der Kompilierung eines Pakets und somit ggf. Vorteile beim Ausführen des Programms. Einige Distributionen überlegen nun, ob sie auf x86-64-v3 upgraden bzw. haben dies bereits getan.

Nun gibt es einige Nutzer die immer noch alte Rechner nutzen, deren CPU beispielsweise nur x86-64-v2 unterstützt. Die Frage ist nun, wie kann man das herausfinden? Hierfür gibt es mehrere Möglichkeiten. Wie beispielsweise folgendem Script.

{{< highlight bash >}}
#!/bin/sh

flags=$(< /proc/cpuinfo grep flags | head -n 1 | cut -d: -f2)

v2='awk "/cx16/&&/lahf/&&/popcnt/&&/sse4_1/&&/sse4_2/&&/ssse3/ {found=1} END {exit !found}"'
v3='awk "/avx/&&/avx2/&&/bmi1/&&/bmi2/&&/f16c/&&/fma/&&/abm/&&/movbe/&&/xsave/ {found=1} END {exit !found}"'
v4='awk "/avx512f/&&/avx512bw/&&/avx512cd/&&/avx512dq/&&/avx512vl/ {found=1} END {exit !found}"'

echo "$flags" | eval "$v2" || exit 2 && echo "Die CPU unterstützt x86-64-v2"
echo "$flags" | eval "$v3" || exit 3 && echo "Die CPU unterstützt x86-64-v3"
echo "$flags" | eval "$v4" || exit 4 && echo "Die CPU unterstützt x86-64-v4"
{{< /highlight >}}

Führe ich das Script auf dem Rechner aus mit den ich gerade diesen Beitrag erstelle, erhalte ich folgende Ausgabe.

{{< highlight bash >}}
./cpuv3.sh
Die CPU unterst&uuml;tzt x86-64-v2
Die CPU unterst&uuml;tzt x86-64-v3
{{< /highlight >}}

Die verbaute CPU (AMD Ryzen 5 2600X) unterstützt also Version 2 und 3 aber nicht Version 4. Wobei Version 4 selten bis gar nicht von einer CPU unterstützt wird. Zumindest nicht bei gewöhnlichen Prozessoren für durchschnittliche private Nutzer soweit mit bekannt ist.
