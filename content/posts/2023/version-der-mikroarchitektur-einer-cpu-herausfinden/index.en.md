---
title: Identify the version of the microarchitecture of a CPU
date: 2023-03-12T13:48:30+0100
categories:
- Englis(c)h
tags:
- Microarchitecture
- CPU
slug: identify-the-version-of-the-microarchitecture-of-a-cpu
---
The different [versions of the microarchitecture of a CPU](https://en.wikipedia.org/wiki/X86-64#Microarchitecture_levels) offer different optimisations during the compilation of a package and thus possibly advantages when running the application. Some distributions are now considering upgrading to x86-64-v3 or have already done so.

Now there are some users who still use old computers whose CPU only supports x86-64-v2, for example. The question now is, how can one check this? There are several possibilities. For example, the following script.

{{< highlight bash >}}
#!/bin/sh

flags=$(< /proc/cpuinfo grep flags | head -n 1 | cut -d: -f2)

v2='awk "/cx16/&&/lahf/&&/popcnt/&&/sse4_1/&&/sse4_2/&&/ssse3/ {found=1} END {exit !found}"'
v3='awk "/avx/&&/avx2/&&/bmi1/&&/bmi2/&&/f16c/&&/fma/&&/abm/&&/movbe/&&/xsave/ {found=1} END {exit !found}"'
v4='awk "/avx512f/&&/avx512bw/&&/avx512cd/&&/avx512dq/&&/avx512vl/ {found=1} END {exit !found}"'

echo "$flags" | eval "$v2" || exit 2 && echo "The CPU supports x86-64-v2"
echo "$flags" | eval "$v3" || exit 3 && echo "The CPU supports x86-64-v3"
echo "$flags" | eval "$v4" || exit 4 && echo "The CPU supports x86-64-v4"
{{< /highlight >}}

If I run the script on the computer I am currently using to create this post, I get the following output.

{{< highlight bash >}}
./cpuv3.sh
The CPU supports x86-64-v2
The CPU supports x86-64-v3
{{< /highlight >}}

So the CPU installed (AMD Ryzen 5 2600X) supports version 2 and 3 but not version 4, whereby version 4 is rarely or never supported by a CPU. At least not for ordinary processors for average private users as far as I know.