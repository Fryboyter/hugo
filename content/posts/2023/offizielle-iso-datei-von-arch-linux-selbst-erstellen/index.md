---
title: Offizielle Datei von Arch Linux selbst erstellen
date: 2023-10-09T18:39:22+02:00
categories:
- OSBN
tags:
- Arch Linux
- Iso
slug: offizielle-datei-von-arch-linux-selbst-erstellen
---
Die Entwickler von Arch Linux veröffentlichen normalerweise am Anfang jeden Monats eine neue Iso-Datei. Diesen Monat verzögert sich das, weil der zuständige Entwickler scheinbar keine Zeit hat oder aus anderen Gründen verhindert ist. Und die Welt geht unter.

Zumindest wenn es nach einigen Nutzern geht, die scheinbar ohne aktuelle Iso-Datei nicht leben können. Warum auch immer. 

Dabei wäre die Lösung ganz einfach. Man erstellt sich einfach selbst eine neue Iso-Datei. Das ist keine Raketenwissenschaft und geht auch ziemlich schnell.

{{< highlight bash >}}
pacman -Syu archiso git
cd $(mktemp -d)
cp -r /usr/share/archiso/configs/releng/ archlive
mkarchiso -v
{{< /highlight >}}

Zuerst installiert man die Pakete archiso und git. Dann erzeugt man ein temporäres Verzeichnis. In dieses Verzeichnis kopiert man dann den Inhalt von /usr/share/archiso/configs/releng/ in das Unterverzeichnis archlive. Schlussendlich erzeugt man mit mkarchiso eine taufrische Iso-Datei von Arch Linux im Verzeichnis {{< mark >}}out{{< /mark >}}.

Mehr ist nicht zu machen. Der ganze Vorgang dauert nur ein paar Minuten und ist daher schneller erledigt als manche Leute zum Erstellen von "Nörgel-Beiträgen" auf Reddit oder sonst wo benötigen.