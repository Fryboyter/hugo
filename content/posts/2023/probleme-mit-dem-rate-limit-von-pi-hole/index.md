---
title: Probleme mit dem Rate-Limit von Pi-hole
date: 2023-01-18T18:52:29+0100
categories:
- OSBN
tags:
- Pi-hole
- Rate Limit
slug: probleme-mit-dem-rate-limit-von-pi-hole
---
Morgens halb zehn in Deutschland. Na ja eigentlich war es heute kurz nach 6 Uhr. Ich wollte noch kurz Dateien verschicken bevor ich zur Arbeit fahre. Was aber nicht funktioniert hat. Kein Gerät in meinem privaten Netzwerk konnte eine Internetverbindung aufbauen.

Wenn der Tag schon so beginnt, kann es nur besser werden. Oder noch viel schlechter. Die Lösung des Problems war dann aber doch relativ einfach. Aber erst einmal muss ich etwas ausholen. 

Auf einem Raspberry Pi ist, unter anderem, eine Kombination von Pi-Hole und unbound installiert. Im Router ist Pi-Hole als DNS eingetragen. Alle im Netzwerk vorhandenen Geräte sind so konfiguriert, dass der Router der einzige DNS ist.

Gestern Nacht hat nun ein Mobiltelefon versucht den F-Droid-Client über das Netzwerk zu aktualisieren. Was aber die ganze Nacht nicht funktioniert hat. Warum kann ich nicht sagen. Da hierbei so hartnäckig vorgegangen wurde, wurden im gesamten Netzwerk durchgehend mehr als 1000 Anfragen pro Minute an Pi-hole gestellt. Und das mag Pi-hole nicht. In der Standardkonfiguration ist Pi-hole so konfiguriert, dass ein Client für einen bestimmten Zeitraum geblockt wird, wenn dieser mehr als 1000 Anfragen pro Minute stellt. Stellt der betreffende Client danach weiterhin zu viele Anfragen, wird er wieder für einen bestimmten Zeitraum geblockt. Und so weiter.

Da nun alle DNS-Anfragen über den Router laufen, ist das für Pi-hole der einzige Client. Und schon ist alles lahmgelegt. DoS sozusagen.

Seit Jahren hat das Limit kein Problem gemacht. Daher gehe ich hier von einem Einzelfall aus. Da allerdings der Raspberry Pi nicht einmal ins Schwitzen gekommen ist, habe ich das Rate-Limit testweise komplett deaktiviert.

In der Datei /etc/pihole/pihole-FTL.conf habe ich die Zeile {{< mark >}}RATE_LIMIT=1000/60{{< /mark >}} auf {{< mark >}}RATE_LIMIT=0/0{{< /mark >}} geändert. Stattdessen kann man natürlich das Limit auch entsprechend erhöhen. Was in dem Fall aber nicht viel gebracht hätte.

Laut https://github.com/pi-hole/FTL/pull/1052 bin ich wohl nicht der Erste, bei dem das Limit überschritten wurde. Ich kann allerdings die Entwickler von Pi-hole durchaus verstehen, warum sie sich für das Limit entschieden haben.