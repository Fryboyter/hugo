---
title: Systemd-boot - Einträge sortieren
date: 2023-09-29T08:31:52+02:00
lastmod: 2023-09-29T09:35:38+0200
categories:
- OSBN
tags:
- Systemd-boot
slug: systemd-boot-eintraege-sortieren
---
Ich schreibe diesen Artikel mit meinem Notebook, auf dem aktuell mehrere unterschiedliche Kernel installiert sind (Linux-Zen, der normale Kernel sowie die aktuelle LTS-Version). Im Auswahlmenü beim Booten werden diese allerdings in einer Reihenfolge angezeigt, die mir nicht zusagt.

Als Erstes wird der Zen-Kernel angezeigt dann der LTS-Kernel und dann der normale Kernel. Der Zen-Kernel an erster Stelle wäre in Ordnung, da er eigentlich mein Standardkernel ist. Allerdings macht dieser in Verbindung mit meinem Notebook derzeit Probleme ([https://github.com/zen-kernel/zen-kernel/issues/313](https://github.com/zen-kernel/zen-kernel/issues/313)). Daher habe ich mir den normalen Kernel installiert und nutze diesen vorübergehend. Der LTS-Kernel ist eigentlich nur für den absoluten Notfall installiert, der bisher nicht eingetreten ist.

Um Einträge im Bootmenü von systemd-boot nach den eigenen Wünschen zu sortieren, muss man die jeweiligen Konfigurationsdateien in /boot/loader/entries/ anpassen. Beim Eintrag, der als erstes angezeigt werden soll, trägt man beispielsweise unterhalb des Titels {{< mark >}}sort-key 01{{< /mark >}} ein. Beim zweiten Eintrag dann {{< mark >}}sort-key 02{{< /mark >}}. Und so weiter. Startet man den Rechner dann neu, sollten die Einträge in der gewünschten Reihenfolge angezeigt werden. 

In meinem Fall ist das aber im Grunde nur rein kosmetischer Natur, da ich in der Datei /boot/loader/loader.conf den normalen Kernel als Standard definiert habe, der entweder nach Ablauf des Timeouts oder nach bestätigen mit Return gestartet wird.

**Edit:** Ich habe eben meinen Rechner gestartet, auf dem neben Linux auch Windows installiert ist. Systemd-boot erkennt eine Windows-Installation automatisch, sodass eine Konfigurationsdatei nicht unbedingt nötig ist. Allerdings wird der Eintrag für Windows an erster Stelle angezeigt. Muss man somit eine Konfigurationsdatei für Windows erstellen, damit man sortieren kann? Nein. Bearbeitet man die Konfigurationsdateien wie bereits beschrieben, berücksichtigt systemd-boot diese Sortierung als Erstes. Der automatisch erzeugte Eintrag für Windows im Bootmenü rutscht somit an das Ende. Eine extra Konfigurationsdatei ist daher nur nötig, wenn man Windows beispielsweise zwischen zwei Einträgen für Linux haben will.