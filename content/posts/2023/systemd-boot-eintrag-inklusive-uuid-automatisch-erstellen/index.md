---
title: Systemd-boot - Eintrag inklusive UUID erstellen
date: 2023-12-31T18:31:00+01:00
nocomments: true
categories:
- OSBN
tags:
- Systemd
- boot
- UUID
- Blkid
slug: systemd-boot-eintrag-inklusive-uuid-erstellen
---
Ein Update des Spiels [Baldurs Gate 3](https://baldursgate3.game) hat kürzlich den Speicherplatz der NVMe gesprengt auf der ich Arch Linux inkl. diverser Benutzerdaten wie die Steam-Datenbank installiert habe. Da ich mir immer Ende jeden Jahres sozusagen selbst eine Art Weihnachtsgeschenk schenke, fiel wie Wahl diesmal auf eine größere NVMe. Also ist demnächst ein Umzug der Daten angesagt.

Da meine Datenträger verschlüsselt sind und ich nicht unbedingt alle Daten umziehen will, habe ich mich für eine Neuinstallation mit anschließender Wiederherstellung von Benutzerdaten aus dem Backup entschieden. Ein Klonen inkl. anschließender Vergrößerung der Partitionen wäre zeitlich aufwändiger.

Als Bootloader verwende ich systemd-boot, da dieser von Haus aus installiert ist und dessen Konfigurationsdateien ziemlich übersichtlich sind. Ein Eintrag, mit dem man Arch Linux startet, könnte wie folgt aussehen.

{{< highlight systemd >}}
title Arch Linux
sort-key 01
linux /vmlinuz-linux-zen
initrd /amd-ucode.img
initrd /initramfs-linux-zen.img
options cryptdevice=UUID=28645e67-6626-49a7-b915-a3e3320d2ddb:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=@ rw nowatchdog nmi_watchdog=0
{{< /highlight >}}

Leider bietet systemd-boot keine Möglichkeit an, diese Einträge automatisch zu erstellen. Was kein Problem wäre, wäre nicht die UUID. So sinnvoll es ist UUID zu nutzen (weil sie sich nicht ändert), so nervig ist es diese einzutragen. Für zukünftige Installationen will ich es mir daher bequemer machen.

{{< highlight bash >}}
uuid=$(blkid -s UUID -o value /dev/nvme0n1p1)
{{< /highlight >}}

{{< highlight bash >}}
cat <<EOF >/home/laythos/arch.conf
title Arch Linux
sort-key 01
linux /vmlinuz-linux-zen
initrd /amd-ucode.img
initrd /initramfs-linux-zen.img
options cryptdevice=UUID=${uuid}:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=@ rw nowatchdog nmi_watchdog=0
EOF
{{< /highlight >}}

Der erste Befehl liest die UUID der Partition von /dev/nvme0n1p1 aus, auf der das Betriebssystem installiert ist, und speichert diese in der Variable uuid. Die Partition muss man natürlich an seine eigenen Gegebenheiten anpassen. 

Der zweite Befehl erzeugt unter /boot/loader/entries die Datei arch.conf mit entsprechendem Inhalt. Anstelle von ${uuid} wird die UUID eingetragen die der erste ausgegeben hat. Bei diesem Beispiel verweist rootflags auf das Btrfs-Subvolumen @. Dort ist in meinem Fall die Distribution installiert. Wer beispielsweise ext4 als Dateisystem nutzt, muss den Eintrag daher ebenfalls anpassen.

Das Beispiel lässt sich natürlich auch für andere Dinge nutzen. Zum Beispiel zum nachträglichen Erzeugen von Einträgen in der /etc/fstab um einen neuen Datenträger dauerhaft zu mounten.