---
title: Mehrere Dokumente gleichzeitig mit Paperless-ngx einscannen
date: 2023-07-31T20:35:09+0200
categories:
- OSBN
tags:
- Paperless-ngx
- Patch T
slug: mehrere-dokumente-gleichzeitig-mit-paperless-ngx-einscannen
---
Genau wie Jörg von [My-IT-Brain](https://www.my-it-brain.de/wordpress/mein-paperless-ngx-mini-konzept/) bin ich gerade dabei Dokumente einzuscannen um sie mit [Paperless-ngx](https://docs.paperless-ngx.com) zu verwalten. Als Scanner nutze ich den Brother ADS-1700W. Dieser kann laut Datenblatt bis zu 20 Seiten automatisch am Stück einscannen. Und genau da fängt das Problem an.

Wenn ich beispielsweise 20 einseitige Rechnungen am Stück einscanne, speichert der Scanner diese in einer PDF-Datei. Für Paperless-ngx ist dies somit auch nur ein Dokument und verarbeitet die Datei entsprechend. Jede Rechnung in eine bestimmte Kategorie einzuordnen und unterschiedliche Tags zuordnen ist somit nicht möglich. Also alle Dokumente einzeln einscannen? Danke, aber nein danke. Da bleibe ich lieber bei der Totholz-Ablage.

Aber das Problem hatten glücklicherweise schon einige Nutzer vor mir. Daher gibt es bereits eine Lösung und sie lautet Patch T. Hierbei handelt es sich um besondere Trennseiten. Diese druckt man aus und fügt sie zwischen die einzelnen Dokumente ein. Diesen Stapel scannt man dann komplett ein. Der Scanner erzeugt zwar weiterhin eine einzelne PDF-Datei aber Paperless-ngx erkennt anhand des Strichcodes, der auf der Trennseite vorhanden ist, wann ein neues Dokument beginnt und splittet die PDF-Datei entsprechend in mehrere Dokumente auf.

Damit Paperless-ngx den Strichcode nutzen kann, muss man vorher aber erst die Umgebungsvariable PAPERLESS_CONSUMER_ENABLE_BARCODES auf true setzen ([https://docs.paperless-ngx.com/configuration/#barcodes](https://docs.paperless-ngx.com/configuration/#barcodes)).

Weiterhin ist es nötig die Trennseite, welche man unter http://www.alliancegroup.co.uk/downloads/patch-code-t.pdf herunterladen kann, entsprechend der Hinweise in der PDF-Datei auszudrucken. Sonst funktioniert das Aufsplitten der PDF-Datei in mehrere Dokumente nicht. Die Ausdrucke kann man beliebig oft wiederverwenden, da der Code immer gleich ist.