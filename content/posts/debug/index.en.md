---
title: Article for testing
date: 1978-09-16T20:02:47+0100
nositemap: true
nocomments: true
nosearch: true
nofeed: false
build:
  list: never
categories:
tags:
slug: ztrbzzjytxlduozznuhizymfttitqgmw
---
One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.

He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.

His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me? " he thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.

A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather. Drops

{{< quote src="Terry Pratchett" >}}
I’d rather be a rising ape than a falling angel.
{{< /quote >}}

{{< quote src="Wau Holland" >}}
Wir müssen die Rechte der Andersdenkenden selbst dann beachten, wenn sie Idioten oder schädlich sind. Wir müssen aufpassen. Wachsamkeit ist der Preis der Freiheit - Keine Zensur!
{{< /quote >}}
