---
title: Das Testfahrzeug ist da
date: 2011-04-14T13:22:00+0100
categories:
- Handbike
tags:
- Hase
- Kettwiesel
- Testen
slug: das-testfahrzeug-ist-da
---
Gerade eben habe ich die Nachricht bekommen, dass das Handbike zum Testen beim Händler angekommen ist. Spätestens Samstag, eher Freitag werde ich es abholen. Somit ist das Wochenende wohl verplant.
