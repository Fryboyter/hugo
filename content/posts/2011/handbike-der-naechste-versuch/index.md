---
title: Handbike - Der nächste Versuch
date: 2011-01-04T23:42:00+0100
categories:
- Allgemein
tags:
- Sopur
- Hase
- Kettwiesel
- Lepus
slug: handbike-der-naechste-versuch
---
Morgen werde ich den nächsten Versuch starten und mich mal über die derzeit angebotenen Handbikes informieren.

Was ein Handbike ist? Unterm Strich kann man sagen, es hat drei Räder und wir nicht mit den Beinen, sondern mit den Armen angetrieben. Also ideal für Krüppel wie mich.

Letztes Jahr hatte ich ja bereits einen Versuch unternommen. Leider ist mir mein damaliges Auto, das in Rente wollte, dazwischen gekommen. Und da ein Handbike mit ein paar tausend Euro nicht mal eben aus der Portokasse nebenher zu zahlen ist, musste ich eben Prioritäten setzen. Aber nun ist wieder genug auf dem Konto.

Zwischenzeitlich bin ich aber von einem Speedbike wie dem [Sopur Shark](https://www.sunrisemedical.de/rollstuehle/rgk/handbike-shark-rt "Sopur Shark") weg, hin zu einem [Kettwiesel](https://hasebikes.com/de/deine-raeder/dreiraeder-fuer-erwachsene/kettwiesel-handbike/ "Kettwiesel") oder {{% deadlink %}}[Lepus](http://hasebikes.com/86-0-Liegerad-Lepus.html "Lepus"){{% /deadlink %}} von Hasebikes.

Unverschämt finde ich nur die Preise. Und zwar egal von welchem Hersteller. Aber was will man machen? Die Dinger sind nun ein Nischenprodukt. Da zahlt man eben oder hat Pech.