---
title: Television - Ein Fuzzy Finder mit Kanälen
date: 2025-03-02T12:24:43+0100
nocomments: true
categories:
- OSBN
tags:
- Fuzzy Finder
slug: television-ein-fuzzy-finder-mit-kanaelen
---
Für das Suchen von Dateien / Verzeichnissen verwende ich seit Jahren einen sogenannten [Fuzzy Finder](https://de.wikipedia.org/wiki/Unscharfe_Suche). Genauer gesagt [fzf](https://github.com/junegunn/fzf), mit dem ich auch im Grunde zufrieden bin. Kürzlich wurde ich aber auf das Tool [television](https://github.com/alexpasmantier/television) aufmerksam und wollte es daher testen.

{{< image src="television.webp" >}}

Die Suchergebnisse von television waren in meinem Fall nicht deutlich besser als die von fzf. Aber auch nicht merklich schlechter. 

Was ich bei television allerdings interessant finde, sind die sogenannten [Channels](https://github.com/alexpasmantier/television/blob/main/docs/channels.md). So gibt es diverse vorkonfigurierte Kanäle. Zum Beispiel {{< mark >}}gitrepos{{< /mark >}} mit denen man nur in Git-Repositories sucht. Oder {{< mark >}}files{{< /mark >}} mit dem man nur nach Dateien sucht. So bietet das Tool einige Möglichkeiten die Suche flexibel einzuschränken.

Weiterhin kann man auch noch konfigurieren, bei welchem Befehl television überhaupt reagieren soll und welcher Kanal dann genutzt wird. In der Standardkonfiguration ist der Editor helix beispielsweise nur für den Kanal git-repos konfiguriert.

{{< highlight bash >}}
[shell_integration.channel_triggers]
"git-repos" = ["nvim",·"code",·"hx",·"git·clone"]
{{< / highlight >}}

Gibt man also im Terminal Emulator {{< mark >}}hx{{< /mark >}} (oder helix je nach verwendeter Distribution) ein und führt dann den Shortcut {{< mark >}}STRG+t{{< /mark >}} aus, wird nur in den erkannten lokalen Git-Repositories gesucht. 

Weiterhin kann man aber auch neue Kanäle anlegen. Zum Beispiel um nach Dotfiles im Verzeichnis $HOME/.config zu suchen und hierbei für die Anzeige bat und für die Suche fd zu verwenden.

{{< highlight bash >}}
[[cable_channel]]
name = "dotfiles"
source_command = 'fd -t f . $HOME/.config'
preview_command = 'bat -n --color=always {0}'
{{< / highlight >}}

Man kann allerdings auch die Standardkanäle anpassen. Hierfür erstellt man einfach, wie auch bei neuen Kanälen, einen Eintrag in beispielsweise der Datei $HOME/.config/television/my-custom-channels.toml und verwendet bei dem betreffenden Eintrag bei name einfach die Bezeichnung des betreffenden [Standardkanals](https://github.com/alexpasmantier/television/blob/main/docs/channels.md#-built-in-channels).

Ist nun television besser als beispielsweise fzf oder [skim](https://github.com/skim-rs/skim)? Ich würde sagen, es kommt wie üblich auf den jeweiligen Nutzer und den jeweiligen Anwendungsfall an. Ich persönlich finde die Idee mit den Kanälen sehr interessant, sodass ich aktuell television deutlich öfters nutze als fzf. Ob ich allerdings beispielsweise komplett auf television umsteige oder beide Tools nutze, kann ich aktuell noch nicht sagen.

Was ich allerdings sagen kann ist, dass die Entwickler bei der Bezeichnung ihrer Tools mal etwas kreativer sein sollten. Ja, television und channels sind von der Idee her gut gewählt. Aber wenn man eine Suchmaschine nutzt, hat der Nutzer einfach mehr, meiner Meinung nach unnötigen, Aufwand. Das Gleiche gilt beispielsweise auch für Hugo (Generator von statischen Webseiten) oder für das Kommunikationsprotokoll Matrix. Ab und zu bin ich inzwischen sogar der Meinung, es wäre gut, wenn Entwickler neue Tools beispielsweise ugaZKp4Hab benennen würden, sodass man danach leichter eine Suchabfrage bei Google, DuckDuckGo oder wo auch immer stellen kann.