---
title: Television - A fuzzy finder with channels
date: 2025-03-02T12:24:43+0100
nocomments: true
categories:
- General
tags:
- Fuzzy Finder
slug: television-a-fuzzy-finder-with-channels
---
I have been using a so-called [Fuzzy Finder](https://de.wikipedia.org/wiki/Unscharfe_Suche) for years to search for files / directories. More precisely [fzf](https://github.com/junegunn/fzf), with which I am basically satisfied. Recently, however, I became aware of the tool [television](https://github.com/alexpasmantier/television) and wanted to test it.

{{< image src="television.webp" >}}

In my case, the search results from television were not significantly better than those from fzf. But not noticeably worse either. 

What I find interesting about television, however, are the so-called [channels](https://github.com/alexpasmantier/television/blob/main/docs/channels.md). There are various preconfigured channels. For example {{< mark >}}gitrepos{{< /mark >}} with which you can only search in git repositories. Or {{< mark >}}files{{< /mark >}} with which you can only search for files. The tool thus offers a number of options for flexibly restricting the search.

You can also configure which command television should respond to and which channel should be used. In the standard configuration, the helix editor is only configured for the git-repos channel, for example.

{{< highlight bash >}}
[shell_integration.channel_triggers]
"git-repos" = ["nvim",·"code",·"hx",·"git·clone"]
{{< / highlight >}}

So if you enter {{< mark >}}hx{{< /mark >}} in the terminal emulator (or helix depending on the distribution used) and then execute the shortcut {{< mark >}}STRG+t{{< /mark >}}, only the recognized local Git repositories will be searched. 

You can also create new channels. For example, to search for dotfiles in the $HOME/.config directory and use bat for the display and fd for the search.

{{< highlight bash >}}
[[cable_channel]]
name = "dotfiles"
source_command = 'fd -t f . $HOME/.config'
preview_command = 'bat -n --color=always {0}'
{{< / highlight >}}

However, you can also customize the standard channels. To do this, simply create an entry in the file $HOME/.config/television/my-custom-channels.toml, for example, as you would do for new channels, and simply use the name of the relevant [default channel](https://github.com/alexpasmantier/television/blob/main/docs/channels.md#-built-in-channels) for the entry in question.

Is television better than fzf or [skim](https://github.com/skim-rs/skim), for example? I would say that, as usual, it depends on the individual user and the respective use case. Personally, I find the idea of the channels very interesting, so I currently use television much more often than fzf. However, I can't yet say for example whether I will switch completely to television or use both tools.

What I can say, however, is that the developers should be a little more creative when naming their tools. Yes, television and channels are a clever idea. But if you use a search engine, the user simply has more, in my opinion unnecessary, effort. The same applies, for example, to Hugo (generator of static websites) or the Matrix communication protocol. Sometimes I even think it would be good if developers named new tools ugaZKp4Hab, for example, so that it would be easier to make a search query on Google, DuckDuckGo or wherever.