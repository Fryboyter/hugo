---
title: Die Version 25.01 des modalen Editors Helix wurde veröffentlicht
date: 2025-01-04T13:16:07+0100
nocomments: true
categories:
- OSBN
tags:
- Helix
- Editor
slug: die-version-25-01-des-modalen-editors-helix-wurde-veroeffentlicht
---
Gestern wurde Version 25.01 des modalen Editors [Helix](https://helix-editor.com) veröffentlicht. Da die vorherige Version im Juli 2024 veröffentlicht wurde und weil die Entwickler ziemlich fleißig waren, gibt es mit Version 25.01 viele Änderungen, Neuerungen und Verbesserungen.

Die Entwickler haben in einem [Artikel](https://helix-editor.com/news/release-25-01-highlights/) die Highlights zusammengefasst. Das wesentlich umfangreichere [Changelog](https://github.com/helix-editor/helix/blob/master/CHANGELOG.md#2501-2025-01-03) lässt sich bei Github abrufen.

Da die Entwickler von Helix relativ selten neue offizielle Versionen veröffentlichen, verwende ich aktuell selbst die Git-Version. Somit konnte ich die diversen Neuerungen usw. bereit schon seit einiger Zeit nutzen. Bei den Funktionen, die ich nutze, konnte ich keine Probleme feststellen.

Wer sich für einen modalen Editor interessiert, aber mit vim / Neovim nicht warm wird, sollte sich Helix zumindest einmal ansehen. Denn Helix funktioniert bewusst in vielen Dingen anders. Ich verweise hierzu einfach mal auf einen [Artikel](https://fryboyter.de/helix-editor/) den ich 2023 geschrieben habe.

Helix hat aber auch, verglichen mit beispielsweise Neovim, weiterhin Nachteile. Je nachdem welche Anforderungen der Nutzer hat. So gibt es weiterhin kein Plugin-System. Was mich persönlich aber nicht stört.