---
title: OpenWrt Barrier Breaker RC1 veröffentlicht
date: 2014-07-15T08:16:00+0100
categories:
- Linux
- OSBN
tags:
- OpenWrt
- Barrier Breaker
slug: openwrt-barrier-breaker-rc1-veroeffentlicht
---
[OpenWrt](https://openwrt.org "OpenWrt") kann auf vielen Routern usw. als alternatives Betriebssystem eingesetzt werden. Vor kurzem wurde nun der erste Release Candidate der kommenden Version Barrier Breaker veröffentlicht. Das Changelog, das die Highlights die seit der aktuellen, stabilen Version hinzugekommen sind kann man {{< wayback "https://lists.openwrt.org/pipermail/openwrt-devel/2014-July/026707.html" >}}hier{{< /wayback >}} nachlesen.

Für mich sind besonders folgende Punkte interessant.

- Nativer IP6-Support
- Snapshot-Funktion des Dateisystems mit möglichem Rollback
- Möglichkeit einer Testkonfiguration mit der Möglichkeit diese wieder auf die letzte funktionierende Konfiguration zurückzustellen.

Ich werde mir am Wochenende mal den RC installieren. Eventuell finde ich ja noch den einen oder anderen Bug der dann bis zur Veröffentlichung der stabilen Version behoben werden kann.
