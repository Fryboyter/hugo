---
title: "Hugo - Inhalt aus Sitemap entfernen"
date: 2019-06-11T20:00:54+0200
categories:
- Allgemein
- OSBN
tags:
- Hugo
- Sitemap
slug: hugo-inhalt-aus-sitemap-entfernen
---
Google Search Console hat herumgemault, dass auf meiner Seite zwei Links durch die robots.txt gesperrt sind, obwohl sie in der Sitemap auftauchen. Wie bekomme man diese nun aus der Sitemap heraus die Hugo automatisch erstellt?

Die Lösung ist eigentlich ganz einfach. Als erstes editiert man die betreffenden Artikel und erweitern die Metadaten um sitemap_exclude: true.

{{< highlight bash >}}
---
title: Geheimer Artikel
date: 2019-06-11T19:12:45+0200
sitemap_exclude: true
slug: geheimer-artikel
---
{{< /highlight >}}

Nun muss noch das Template angepasst werden, mit dem die Sitemap erzeugt wird. Hierfür erstellt man im Themeverzeichnis unter layouts/_default die Datei sitemap.xml und füllt diese mit folgendem Inhalt.

{{< highlight go-template >}}
{{ printf "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>" | safeHTML }}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xhtml="http://www.w3.org/1999/xhtml">
  {{ range .Data.Pages }}
  <url>
    <loc>{{ .Permalink }}</loc>{{ if not .Lastmod.IsZero }}
    <lastmod>{{ safeHTML ( .Lastmod.Format "2006-01-02T15:04:05-07:00" ) }}</lastmod>{{ end }}{{ with .Sitemap.ChangeFreq }}
    <changefreq>{{ . }}</changefreq>{{ end }}{{ if ge .Sitemap.Priority 0.0 }}
    <priority>{{ .Sitemap.Priority }}</priority>{{ end }}{{ if .IsTranslated }}{{ range .Translations }}
    <xhtml:link
                rel="alternate"
                hreflang="{{ .Lang }}"
                href="{{ .Permalink }}"
                />{{ end }}
    <xhtml:link
                rel="alternate"
                hreflang="{{ .Lang }}"
                href="{{ .Permalink }}"
                />{{ end }}
  </url>
  {{ end }}
</urlset>
{{< /highlight >}}

Dies entspricht erst einmal dem Standard-Template für die Sitemap. Nun ändert man Zeile 4 wie folgt ab.

{{< highlight go-template >}}
{{ range .Data.Pages }}{{ if ne .Params.sitemap_exclude true }}
{{< /highlight >}}

Abschließen erweitert man noch die Zeile 21 um ein weiteres {{ end }}.

{{< highlight go-template >}}
{{ end }}{{ end }}
{{< /highlight >}}

Wird nun die Seite neu erzeugt, sollten alle Inhalte bei denen sitemap_exclude: true hinterlegt wurde nicht mehr in der Sitemap erscheinen.