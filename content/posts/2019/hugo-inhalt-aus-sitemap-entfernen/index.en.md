---
title: "Hugo - Remove content from sitemap"
date: 2019-06-11T20:00:54+0200
categories:
- Englis(c)h
tags:
- Hugo
- Sitemap
slug: remove-content-from-sitemap
---
Google Search Console has complained that two links on my site are blocked by robots.txt even though they appear in the sitemap. How do I get them from the sitemap that Hugo creates automatically?

The solution is actually quite simple. First edit the related articles and add sitemap_exclude: true to the metadata.

{{< highlight bash >}}
---
title: Secret Article
date: 2019-06-11T19:12:45+0200
sitemap_exclude: true
slug: secret-article
---
{{< /highlight >}}

Now it is necessary to adapt the template with which the sitemap is created. Therefore create the file sitemap.xml in the theme directory under layouts/_default and fill it with the following content.

{{< highlight go-template >}}
{{ printf "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>" | safeHTML }}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xhtml="http://www.w3.org/1999/xhtml">
  {{ range .Data.Pages }}
  <url>
    <loc>{{ .Permalink }}</loc>{{ if not .Lastmod.IsZero }}
    <lastmod>{{ safeHTML ( .Lastmod.Format "2006-01-02T15:04:05-07:00" ) }}</lastmod>{{ end }}{{ with .Sitemap.ChangeFreq }}
    <changefreq>{{ . }}</changefreq>{{ end }}{{ if ge .Sitemap.Priority 0.0 }}
    <priority>{{ .Sitemap.Priority }}</priority>{{ end }}{{ if .IsTranslated }}{{ range .Translations }}
    <xhtml:link
                rel="alternate"
                hreflang="{{ .Lang }}"
                href="{{ .Permalink }}"
                />{{ end }}
    <xhtml:link
                rel="alternate"
                hreflang="{{ .Lang }}"
                href="{{ .Permalink }}"
                />{{ end }}
  </url>
  {{ end }}
</urlset>
{{< /highlight >}}

This corresponds initially to the standard template for the sitemap. Now change line 4 as follows.

{{< highlight go-template >}}
{{ range .Data.Pages }}{{ if ne .Params.sitemap_exclude true }}
{{< /highlight >}}

Finally add another {{ end }} to line 21.

{{< highlight go-template >}}
{{ end }}{{ end }}
{{< /highlight >}}

If the page is rebuilt now, all contents where sitemap_exclude: true is specified should no longer appear in the sitemap.