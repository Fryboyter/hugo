---
title: Dritte können nun Änderungen an meinen Artikeln vornehmen
date: 2019-09-08T20:55:14+0200
categories: 
- OSBN
- Allgemein
tags:
- Hugo
- Github
slug: dritte-koennen-nun-aenderungen-an-meinen-artikeln-vornehmen
---
{{< notice info >}}
Die Möglichkeit, die Artikel anzupassen, wurde zwischenzeitlich wieder entfernt.
{{< /notice >}}

Manchen Leuten ist scheinbar ab und zu sehr langweilig. Dieses Wochenende habe ich doch tatsächlich eine E-Mail mit Hinweisen auf den einen oder anderen Fehler in meinen Artikeln erhalten. Im Grunde waren es nur Rechtschreibfehler.

In der E-Mail wurde ich auch gefragt, ob ich für solche Sachen nicht ein öffentlich zugängliches Repository anbieten kann. Kann ich. Es ist unter [https://codeberg.org/Fryboyter/fryboyter.de](https://codeberg.org/Fryboyter/fryboyter.de) erreichbar.

Aber würde ich selbst einen Artikel lesen und dann das Repository aufrufen und die Datei suchen, wenn ich einen Fehler finden würde? Eher nicht. Daher habe ich nun einen Link unterhalb der jeweiligen Artikelüberschrift eingebaut, der direkt auf die betreffende Datei bei Github verweist.

Als Erstes habe ich hierfür in der Konfigurationsdatei von [Hugo](https://gohugo.io/) (config.toml) den Bereich [params] um einen Verweis auf das Repository eingetragen.

{{< highlight golang >}}
[params]
ghrepo = "https://codeberg.org/Fryboyter/fryboyter.de/"
{{< /highlight >}}


Als Nächstes habe ich das verwendete Theme (single.html und list.html) von fryboter.de wie folgt erweitert.

{{< highlight go-template >}}
| <a href="{{.Site.Params.ghrepo}}edit/master/content/{{.File.Path}}" >Bei Github bearbeiten</a>
{{< /highlight >}}

Hiermit wird das in der Datei conifg.toml hinterlegte Verweis auf das Respository genutzt. Dieser wird dann einfach um edit/master/content/ erweitert, da sich dieser Teil nicht ändert. Ganz am Schluss wird mit {{.File.Path}} auf die betreffende Datei verlinkt.

Im Grunde ist das mal wieder ziemlich simpel. Hugo gefällt mir immer mehr.
