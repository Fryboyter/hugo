---
title: "Auf Kompletten Feed Umgestellt"
date: 2019-04-27T18:07:22+02:00
categories:
- OSBN
- Allgemein
tags:
- Hugo
- Feed
slug: auf-kompletten-feed-umgestellt
---
Vor ein paar Tagen wurde ich gefragt, ob ich den OSBN-Feed nicht so ändern kann, dass dieser den kompletten Artikel anzeigt. Der Wunsch ist mir Befehl.

Hierzu habe ich einfach im Theme-Verzeichnis von Hugo im Unterverzeichnis layouts/_default die Datei rss.xml mit folgendem Inhalt angelegt (dieser entspricht dem Standard-Template des Feeds).

{{< highlight bash >}}
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>{{ if eq  .Title  .Site.Title }}{{ .Site.Title }}{{ else }}{{ with .Title }}{{.}} on {{ end }}{{ .Site.Title }}{{ end }}</title>
    <link>{{ .Permalink }}</link>
    <description>Recent content {{ if ne  .Title  .Site.Title }}{{ with .Title }}in {{.}} {{ end }}{{ end }}on {{ .Site.Title }}</description>
    <generator>Hugo -- gohugo.io</generator>{{ with .Site.LanguageCode }}
    <language>{{.}}</language>{{end}}{{ with .Site.Author.email }}
    <managingEditor>{{.}}{{ with $.Site.Author.name }} ({{.}}){{end}}</managingEditor>{{end}}{{ with .Site.Author.email }}
    <webMaster>{{.}}{{ with $.Site.Author.name }} ({{.}}){{end}}</webMaster>{{end}}{{ with .Site.Copyright }}
    <copyright>{{.}}</copyright>{{end}}{{ if not .Date.IsZero }}
    <lastBuildDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 -0700" | safeHTML }}</lastBuildDate>{{ end }}
    {{ with .OutputFormats.Get "RSS" }}
        {{ printf "<atom:link href=%q rel=\"self\" type=%q />" .Permalink .MediaType | safeHTML }}
    {{ end }}
    {{ range .Pages }}
    <item>
      <title>{{ .Title }}</title>
      <link>{{ .Permalink }}</link>
      <pubDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 -0700" | safeHTML }}</pubDate>
      {{ with .Site.Author.email }}<author>{{.}}{{ with $.Site.Author.name }} ({{.}}){{end}}</author>{{end}}
      <guid>{{ .Permalink }}</guid>
      <description>{{ .Summary | html }}</description>
    </item>
    {{ end }}
  </channel>
</rss>
{{< /highlight >}}

Damit nun der komplette Artikel angezeigt wird, habe ich die Zeile &lt;description&gt;{{ .Summary | html }}&lt;/description&gt; auf &lt;description&gt;{{ .Content | html }}&lt;/description&gt; geändert. Das war es schon. Die Änderung gilt für die komplette Seite und nicht nur für die Kategorie OSBN.