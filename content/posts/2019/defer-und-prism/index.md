---
title: Defer und Prism
date: 2019-01-01T11:59:00+0100
categories:
- OSBN
- Allgemein
tags:
- Prism
- Defer
slug: defer-und-prism
---
Binde Javascript mit defer ein, haben sie gesagt. Für die Syntaxhervorhebung nutze ich Prism. Aufgrund des Ratschlags habe ich daher das Script wie folgt eingebunden.

{{< highlight bash >}}
<script defer src=\"<a href="https://fryboyter.de/theme/fryboyter/js/prism.js?d46722219d" target="_blank">/theme/fryboyter/js/prism.js</a>"></script>
{{< /highlight >}}

Keine gute Idee wie ich festgestellt habe.

In einigen Fällen habe ich zum Beispiel folgenden Code hervorgehoben:

{{< highlight bash >}}
$IP_DES_RASPBERRY:$PORT_VON_CADDY
root /pfad/zum/Mirrorverzeichnis
gzip
browse
{{< /highlight >}}

Beim ersten Aufruf des Artikels wurde allerdings folgendes angezeigt:

{{< highlight bash >}}
$IP_DES_RASPBERRY:$PORT_VON_CADDY
root /pfad/zum/Mirrorverzeichnis
gzip
browse
$IP_DES_RASPBERRY:$PORT_VON_CADDY
{{< /highlight >}}

Kurz gesagt die erste Zeile wird am Ende des Blocks noch einmal angezeigt. Nicht gut. Aktualisiert man die Seite, wird es aber korrekt angezeigt. Beim jeweils ersten Aufruf ist mir dann aufgefallen, dass folgender Fehler in den Entwicklerwerkzeugen des Browsers angezeigt wird.

{{< highlight javascript >}}
Uncaught TypeError: Cannot read property &apos;children&apos; of null
    at prism.js?d46722219d:18
    at Array.forEach (&lt;anonymous&gt;)
    at n (prism.js?d46722219d:18)
    at NodeList.forEach (&lt;anonymous&gt;)
    at prism.js?d46722219d:18
{{< /highlight >}}

Ok Zeile 18 der Datei prism.js ist wohl das Problem.

{{< highlight javascript >}}
!function(){if("undefined"!=typeof self&&self.Prism&&self.document){var e="line-numbers",t=/\n(?!$)/g,n=function(e){var n=r(e),s=n["white-space"];if("pre-wrap"===s||"pre-line"===s){var l=e.querySelector("code"),i=e.querySelector(".line-numbers-rows"),a=e.querySelector(".line-numbers-sizer"),o=l.textContent.split(t);a||(a=document.createElement("span"),a.className="line-numbers-sizer",l.appendChild(a)),a.style.display="block",o.forEach(function(e,t){a.textContent=e||"\n";var n=a.getBoundingClientRect().height;i.children[t].style.height=n+"px"}),a.textContent="",a.style.display="none"}},r=function(e){return e?window.getComputedStyle?getComputedStyle(e):e.currentStyle||null:null};window.addEventListener("resize",function(){Array.prototype.forEach.call(document.querySelectorAll("pre."+e),n)}),Prism.hooks.add("complete",function(e){if(e.code){var r=e.element.parentNode,s=/\s*\bline-numbers\b\s*/;if(r&&/pre/i.test(r.nodeName)&&(s.test(r.className)||s.test(e.element.className))&&!e.element.querySelector(".line-numbers-rows")){s.test(e.element.className)&&(e.element.className=e.element.className.replace(s," ")),s.test(r.className)||(r.className+=" line-numbers");var l,i=e.code.match(t),a=i?i.length+1:1,o=new Array(a+1);o=o.join("<span></span>"),l=document.createElement("span"),l.setAttribute("aria-hidden","true"),l.className="line-numbers-rows",l.innerHTML=o,r.hasAttribute("data-start")&&(r.style.counterReset="linenumber "+(parseInt(r.getAttribute("data-start"),10)-1)),e.element.appendChild(l),n(r),Prism.hooks.run("line-numbers",e)}}}),Prism.hooks.add("line-numbers",function(e){e.plugins=e.plugins||{},e.plugins.lineNumbers=!0}),Prism.plugins.lineNumbers={getLine:function(t,n){if("PRE"===t.tagName&&t.classList.contains(e)){var r=t.querySelector(".line-numbers-rows"),s=parseInt(t.getAttribute("data-start"),10)||1,l=s+(r.children.length-1);s>n&&(n=s),n>l&&(n=l);var i=n-s;return r.children[i]}}}}}();
{{< /highlight >}}

Ok, ich verstehe so ziemlich rein gar nichts. Es hilft mir also nicht weiter. Nachdem ich etwas im Bugtracker von Prism gesucht habe, habe ich dort ein bereits behobenen Problem mit bezüglich der Einbindung per defer gefunden. Dessen Symptome waren aber nicht mit meinem Problem vergleichbar. Trotzdem habe ich mal schaut was passiert, wenn ich Prism nicht mit defer einbinde. Und schon ist mein Problem verschwunden. Da es hinsichtlich der Ladezeit gefühlt keinen Unterschied gibt, ob ich nun defer verwende oder nicht, habe ich defer einfach wieder entfernt. Und was lernen wir daraus? Nicht jeder Tipp zur Seitenoptimierung macht pauschal Sinn.