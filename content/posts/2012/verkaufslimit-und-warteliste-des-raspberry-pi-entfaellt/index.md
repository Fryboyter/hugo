---
title: Verkaufslimit und Warteliste des Raspberry Pi entfällt
date: 2012-07-17T11:59:00+0100
categories:
- Linux
- OSBN
tags:
- Raspberry Pi
- Verkaufslimit
- Warteliste
slug: verkaufslimit-und-warteliste-des-raspberry-pi-entfaellt
---
Ich sage es gleich. Die Wartezeit bleibt bei 4-6 Wochen, da die 4000 Stück, die täglich produziert werden, wohl nicht ausreichen, um den Bedarf zu decken. Dafür kann man aber nun direkt ohne Warteliste und mehr als einen Raspberry Pi bestellen. Das hat zumindest die {{< wayback "http://www.raspberrypi.org/archives/1588" >}}Raspberry Pi Foundation{{< /wayback >}} mitgeteilt und der direkte Bestellvorgang bei den beiden Vertriebspartnern scheint zu funktionieren.
