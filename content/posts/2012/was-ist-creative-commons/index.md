---
title: Was ist Creative Commons?
date: 2012-08-23T15:22:00+0100
categories:
- OSBN
- Allgemein
tags:
- Lizenz
- Creative Commons
slug: was-ist-creative-commons
---
Wie man in der rechten Spalte erkennen kann, veröffentliche ich alles von mir unter der Creative-Commons-BY-SA-Lizenz. Was muss man sich darunter vorstellen? Martin Mißfeldt hat das Konzept das hinter den Creative Commons Lizenzen steht in eine Art [Comic](http://www.bildersuche.org/creative-commons-infografik.php "Creative Commons Infografik") gepackt und gibt hierzu noch einige genauere Anmerkungen.

{{< image src="creative-commons-info.webp" alt="Creative Commons Erklärung" captionMD="Grafik welche die Creative Commons Lizenz erklärt.Von [Martin Mißfeldt](https://www.bildersuche.org/creative-commons-infografik.php) / [CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/de/)" >}}

Wer sich also für diese Lizenzen interessiert, sollte dort mal vorbeischauen.
