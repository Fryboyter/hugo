---
title: Anubis - unbekannte ausführbare Daten analysieren
date: 2012-02-02T17:29:00+0100
categories:
- Allgemein
tags:
- Anubis
- Analyse
- Dateien
- Schädlinge
slug: anubis-unbekannte-ausfuehrbare-daten-analysieren
---
Hier mal eines von vielen Möglichkeiten ausführbare Windowsdateien zu analysieren, was die Datei so treibt, wenn man Sie ausführt. Unter {{% deadlink %}}[http://anubis.iseclab.org](http://anubis.iseclab.org "Anubis"){{% /deadlink %}} kann man die verdächtige Datei hochladen und bekommt einen Report erstellt. Ich habe die schon recht bekannte Datei "WoWEmuHack.exe" (Schädling) zum Testen genommen und folgenden [Report](WoWEmuHack.exe1_.pdf) zurückbekommen. Für Leute mit Spiel- und Basteltrieb schon mal ein Anfang.